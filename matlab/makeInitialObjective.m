function [InitialObjmat] = makeInitialObjective(objstr,folder)
% folder = 'C:\Users\balto\switchdrive\PaperPlane\Experiments\0510_BO\ClusterData';
youngestFile = findYoungestFile(folder,'mat');
km_mat = load(youngestFile).km_mat;
header = km_mat(1,:);
km_mat = cell2mat(km_mat(2:end,:));
InitialObjmat = km_mat(:,find(strcmp(header,objstr)));

end


function youngestFile = findYoungestFile(folder,fileformat)

d            = dir([folder '\*' fileformat]);
[~, index]   = max([d.datenum]);
youngestFile = fullfile(d(index).folder, d(index).name);

end
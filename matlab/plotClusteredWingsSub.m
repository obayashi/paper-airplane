function plotClusteredWingsSub(plotteddesigns,design_mat,prob_strength,subdesignindex,num_cluster,clusteridx,wing_data,header,rootcluster,ynsub)
plotteddesigns = plotteddesigns{rootcluster};
figure
for i = 1:num_cluster
    designstoplot = design_mat(subdesignindex(clusteridx==i),1);
    designstoplot = unique(designstoplot);
    
    subplot(3,2,i)
    for j = 1:length(designstoplot)
        if sum(designstoplot(j)==plotteddesigns)>0
            wing_x = wing_data(design_mat(j,1),find(strcmp(header,'x1')):find(strcmp(header,'x4')));
            wing_y = wing_data(design_mat(j,1),find(strcmp(header,'y1')):find(strcmp(header,'y4')));
            plotwing([297 210],wing_x,wing_y);
            hold on
        end
    end
    if ynsub
        title(['P(Cluster' num2str(rootcluster) char(64+i) ')=[' num2str(min(prob_strength)) ' ' num2str(max(prob_strength)) ']; ' num2str(length(designstoplot)) ' of ' length(plotteddesigns) ' designs'])
        
    else
        title(['P(Cluster' num2str(i) ')=[' num2str(min(prob_strength)) ' ' num2str(max(prob_strength)) ']; ' num2str(length(designstoplot)) ' of ' length(plotteddesigns) ' designs'])
        
    end
    grid on
    axis equal
end
end
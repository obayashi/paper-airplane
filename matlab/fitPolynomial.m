function [fitX,fitY,tstat,P] = fitPolynomial(X,Y)
% fit 5th polynomial to the trajectory

[P,S,mu] = polyfit(X,Y,5);

[Yfit,DELTA] = polyval(P,X,S,mu);

COVB = (S.R'*S.R)\eye(size(S.R)) * S.normr^2/S.df;

SE = sqrt(diag(COVB));                              % Standard Errors

tstat = P./SE';

fitX = X;
fitY = Yfit;



end

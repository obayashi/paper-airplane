function plotClusteredWings(design_mat,num_cluster,clusteridx,wing_data,header,rootcluster,ynsub,totaldesigns)
figure
for i = 1:num_cluster
    designstoplot = unique(design_mat(clusteridx==i,1));
    subplot(3,2,i)
    for j = 1:length(designstoplot)
        wing_x = wing_data(designstoplot(j),find(strcmp(header,'x1')):find(strcmp(header,'x4')));
        wing_y = wing_data(designstoplot(j),find(strcmp(header,'y1')):find(strcmp(header,'y4')));
        plotwing([297 210],wing_x,wing_y);
        hold on
    end
    if ynsub
        title(['P(Cluster' num2str(rootcluster) char(64+i) ')=[]; ' num2str(length(designstoplot)) ' of ' num2str(totaldesigns) ' designs'])
        
    else
        title(['Cluster' num2str(i) '; ' num2str(length(designstoplot)) ' of ' num2str(totaldesigns) ' designs'])
        
    end
    grid on
    axis equal
end
end
function prob_cluster = findClusterProbability(idx,num_flights,num_designs,num_cluster)
idx_reshape = reshape(idx,num_flights,num_designs);
prob_cluster = zeros(num_cluster,num_designs);
for i = 1:num_cluster
    prob_cluster(i,:) = sum(idx_reshape==i)./num_flights;
end
end


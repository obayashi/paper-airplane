function [cluster_num, model] = chooseCluster(objstr,folder)
% folder = 'G:\.shortcut-targets-by-id\1wYXDAMCh59hhAOMrpcbdbmdNo1mZvuMP\CREATE Lab\Nana PhD\2022 Paper Airplane\Experiment\0510_BO\GMModels';
youngestFile = findYoungestFile(folder,'mat');
medianvec = load(youngestFile).medianvec;
modelStruct = load(youngestFile).modelStruct;

if strcmp(objstr,'maxdist')
    [temp idx] = max(medianvec);
elseif strcmp(objstr,'target3')
    [temp idx] = min(abs(medianvec-3));
elseif strcmp(objstr,'target5')
    [temp idx] = min(abs(medianvec-5));
end
% need to add here if there are other objective

cluster_num = idx;
model = modelStruct(cluster_num).model;
end

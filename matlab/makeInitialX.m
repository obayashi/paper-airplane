function [InitialXmat] = makeInitialX(folder)
% folder = 'C:\Users\balto\switchdrive\PaperPlane\Experiments\0510_BO\WingData';
youngestFile = findYoungestFile(folder,'mat');
wing_data = load(youngestFile).wing_data;
header = wing_data(1,:);
wing_data = cell2mat(wing_data(2:end,:));
num_flight = 5;
InitialXmat = [];
for i = 1:size(wing_data,1)
    for j = 1:num_flight
        newline = [wing_data(i,find(strcmp(header,'sweep'))),...
            wing_data(i,find(strcmp(header,'back_sweep'))),...
            wing_data(i,find(strcmp(header,'w/l')))];
        InitialXmat(end+1,:) = newline;
    end
end

end


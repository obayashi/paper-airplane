function getTrajectory(video_path,design_num,video_folder,current)
video_path = [video_path video_folder '\'];
out_folder = [current '\RawTrajectory'];
% num_designs = 50;
des_str = num2str(design_num);

files = dir([video_path des_str '\*.mp4']);
flinfo = {};
for l = 1:numel(files)
    vid_name = files(l).name;
    
    vidObj =VideoReader(strcat(video_path,des_str,'\',vid_name),'CurrentTime',0);
    num_frames= vidObj.NumFrames;
    frames = read(vidObj,[1 Inf]);
    frame_width = size(frames,2);
    frame_height = size(frames,1);
    
    cropx=[1 frame_height];
    cropy=[1 frame_width/2]; % left
    cropyR=[frame_width/2+1 frame_width]; %right frame
    f_start=1;
    f_end=num_frames-1;
    
    windowSize = 3; % if this is bigger, blurs more, takes more time 3-5 is often used
    kernel = ones(windowSize) / windowSize^2; % kernel of ones, normally it is gaussian convolution matrix
    
    % For the frames of interest find the difference, convert to greyscale crop
    % and store
    for i = f_start:f_end
        %t1=frames(:,:,:,1);
        %t2=frames(:,:,:,i+1);
        t1 = imfilter(frames(:,:,:,1), kernel, 'conv'); %imfilter blurs
        t2=imfilter(frames(:,:,:,i), kernel, 'conv');
        K = imabsdiff(t1,t2);
        BW(:,:,:,i) = rgb2gray(K);
        B=BW(:,:,:,i);
        temp = imbinarize(B, 0.08);
        Bin(:,:,i) = temp(cropx(1):cropx(2), cropy(1):cropy(2));
        BinR(:,:,i) = temp(cropx(1):cropx(2), cropyR(1):cropyR(2));
    end
    
    Binf = (zeros(range(cropx)+1,range(cropy)+1));   % Crop the image to just the flight region
    x=[0];
    y=[0];
    BinfR = (zeros(range(cropx)+1,range(cropyR)+1));   % Crop the image to just the flight region
    xR=[0];
    yR=[0];
    sizeR_store = [0];
    
    for j = f_start:f_end
        largestRegion = bwareafilt(Bin(:,:,j), 1);          % Find the largest white area which is the plane
        largestRegionR = bwareafilt(BinR(:,:,j), 1);
        [rows, columns] = find(largestRegion);
        [rowsR, colsR] = find(largestRegionR);
        sizeL = sum(largestRegion, 'all');
        sizeR = sum(largestRegionR, 'all');
        sizeR_store = [sizeR_store sizeR];
        if(sizeL > 30)                           % If find single pixels, ignore and use previous location - avoids detection of noise when no plane in the frame
            
            xc = mean(rows);
            yc = mean(columns);
            x=[x xc];
            y=[y yc];
        else
            x=[x x(length(x))];
            y=[y y(length(y))];
        end
        if(sizeR > 30)                           % If find single pixels, ignore and use previous location - avoids detection of noise when no plane in the frame
            
            xcR = mean(rowsR);
            ycR = mean(colsR);
            xR=[xR xcR];
            yR=[yR ycR];
        else
            xR=[xR xR(length(xR))];
            yR=[yR yR(length(yR))];
        end
        
        Binf = Binf + Bin(:,:,j);           % This is just to sum all the pixels so can graphically see
        BinfR = BinfR + BinR(:,:,j);
    end
    
    img_x = y;
    img_y = x;
    
    nonzero_idx = find(img_x~=0); % get rid of initial zeros
    landed_idx = find(img_x == img_x(end)); % get rid of same pixels after landing
    img_x = img_x(nonzero_idx(1):landed_idx(1));
    img_y = img_y(nonzero_idx(1):landed_idx(1));
    
    % trajectory from the side
    img_xR = yR;
    img_yR = xR;
    
    nonzero_idx = find(img_xR~=0); % get rid of initial zeros
    landed_idx = find(img_xR == img_xR(end)); % get rid of same pixels after landing
    img_xR = img_xR(nonzero_idx(1):landed_idx(1));
    img_yR = img_yR(nonzero_idx(1):landed_idx(1));
    
            disp(['wing' num2str(design_num)])
            disp(['flight' num2str(l)])
    
    flight_info.top_x_pixel = img_x;
    flight_info.top_y_pixel = img_y;
    flight_info.side_x_pixel = img_xR;
    flight_info.side_y_pixel = img_yR;
    
    flinfo{l} = flight_info;
    
end
out_fn = ['flinfo' des_str '.mat'];
save([out_folder '\' out_fn],'flinfo');
end


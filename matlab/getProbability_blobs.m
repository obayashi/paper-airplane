function probout = getProbability_blobs(qp,GMModel,color)

figure %temp
g = gca; %temp
num_subcluster = size(GMModel.mu,1);

prob = zeros(size(qp,1),num_subcluster);
for i = 1:num_subcluster
    mu = GMModel.mu(i,:);
    if and(ndims(GMModel.Sigma)<3,num_subcluster>1)
        Sigma = GMModel.Sigma;
    else
        Sigma = GMModel.Sigma(:,:,i);
    end
    
    min_sigma_diagonal = 0.04;
    for j = 1:3
        if Sigma(j,j) < min_sigma_diagonal
            Sigma(j,j) = min_sigma_diagonal;
        end
    end
    
    plot_gaussian_ellipsoid(mu,Sigma,2,10,g,color) %temp
    hold on %temp

    multiple = 2;
    X1bounds = [mu(1)-multiple*35 mu(1)+multiple*35];
    X2bounds = [mu(2)-multiple*50 mu(2)+multiple*50];
    X3bounds = [mu(3)-multiple*2 mu(3)+multiple*2];

    X1elem = 100;
    X2elem = 100;
    X3elem = 100;
    [X1,X2,X3] = meshgrid(linspace(X1bounds(1),X1bounds(2),X1elem)',...
        linspace(X2bounds(1),X2bounds(2),X2elem)',...
        linspace(X3bounds(1),X3bounds(2),X3elem)');
    X = [X1(:) X2(:) X3(:)];
    
    
    pdfval = mvnpdf(X, mu, Sigma);
    
    for j = 1:size(qp,1)
%         if mod(j,1000)==0
%             disp(j)
%         end
%         
        d = (qp(j,1)-X1).^2+(qp(j,2)-X2).^2+(qp(j,3)-X3).^2; % square distance
        %     ind = dsearchn(qp,X);
        [~, ind] = min(d(:)); % minimum distance
        area = ((X1bounds(2)-X1bounds(1))/X1elem)*...
            ((X2bounds(2)-X2bounds(1))/X2elem)*((X3bounds(2)-X3bounds(1))/X3elem);
        pdf_qp = pdfval(ind);
        pdf_lessthanqp = pdfval(pdfval<=pdf_qp);
        %     prob = sum(pdf_lessthanqp)*area;
        prob(j,i) = sum(pdf_lessthanqp)*area;

    end
end
%
probout = max(prob,[],2);
end


function [km_mat,design_mat] = make_kmmat(desnum,dir,addYN)
if addYN % yes, add to existing
    km_mat = load(findYoungestFile([dir '\ClusterData'],'mat')).km_mat;
    km_mat = cell2mat(km_mat(2:end,:));
    design_mat = load(findYoungestFile([dir '\ClusterData'],'mat')).design_mat;
else % create new
    km_mat = [];
    design_mat = [];
end
for i = 1:length(desnum)
    flinfo = load([dir '\FlightInfo\' 'flinfo' num2str(desnum(i)) '.mat']).flinfo;
    for j = 1:length(flinfo)
        flight = flinfo{j};
        dist_para = flight.landing_spot(2);
        centdev_para = flight.landing_spot(1);
        fltime_para = flight.fl_time;
        
        km_mat(end+1,:) = [flight.estimates(1:4) dist_para centdev_para fltime_para];
        design_mat(end+1,:) = [desnum(i) j];
    end
end
km_header = {'coeff5','coeff4','coeff3','coeff2','dist','centdev','fltime'};
km_mat = [km_header;num2cell(km_mat)];
time = datestr(now,'mmdd_HHMM');
filename = sprintf('km_mat_%s.mat',time);
prompt = "Do you want to save km_mat? y/n:";
txt = input(prompt,"s");
if strcmp(txt,'y')
    save([dir '\ClusterData\' filename],'km_mat','design_mat')
end

end
function [newX,newY] = cutBottom(X,Y,cutoff)
% after getting trajectory, function cuts off the bottom portion below a
% threshold. searches for the first point where it reaches the threshold,
% then cuts the rest
below_threshold = find(Y<cutoff);
cutoff_idx = below_threshold(1)-1;
newX = X(1:cutoff_idx);
newY = Y(1:cutoff_idx);
end


function cluster2origparaMakeEquations()
path = 'C:\Users\balto\switchdrive\PaperPlane\matlab';

%sweep_back > 90

syms sw swb wol bhalf Ahalf real

A = bhalf/tan(pi/2-sw);

% switch
B = -1*bhalf*tan(swb-pi/2);
ct = (bhalf*2)/wol-A;

A1 = 0.5*A*bhalf;
A2 = bhalf*ct;
A3 = 0.5*B*bhalf;
eqn1 = Ahalf == A1+A2+A3;
bhalf_solution = solve(eqn1,bhalf);
A = bhalf_solution/tan(pi/2-sw);

%     switch
B = -1*bhalf_solution*tan(swb-pi/2);
ct = (bhalf_solution*2)/wol-A;

cr = A+B+ct;

matlabFunction(bhalf_solution,'File',[path '\eqn_bhalf_gt90'],'Vars', {Ahalf,sw,swb,wol});
matlabFunction(ct,'File',[path '\eqn_ct_gt90'],'Vars', {Ahalf,sw,swb,wol});
matlabFunction(cr,'File',[path '\eqn_cr_gt90'],'Vars', {Ahalf,sw,swb,wol});

%sweep_back <= 90
A = bhalf/tan(pi/2-sw);

% switch
B = bhalf*tan(pi/2-swb);
ct = (bhalf*2)/wol-A-B;

A1 = 0.5*A*bhalf;
A2 = bhalf*ct;
A3 = 0.5*B*bhalf;
eqn1 = Ahalf == A1+A2+A3;
bhalf_solution = solve(eqn1,bhalf);
A = bhalf_solution/tan(pi/2-sw);

% switch
B = bhalf_solution*tan(pi/2-swb);
ct = (bhalf_solution*2)/wol-A-B;

cr = A+B+ct;

matlabFunction(bhalf_solution,'File',[path '\eqn_bhalf_lte90'],'Vars', {Ahalf,sw,swb,wol});
matlabFunction(ct,'File',[path '\eqn_ct_lte90'],'Vars', {Ahalf,sw,swb,wol});
matlabFunction(cr,'File',[path '\eqn_cr_lte90'],'Vars', {Ahalf,sw,swb,wol});

end

function palette = paletteEPFL()

    palette = {'#ff0000','#b51f1f','#00a79f','#007480','#413d3a','#cac7c7','#4A74CF'};



    % https://www.epfl.ch/about/overview/wp-content/uploads/2020/06/EPFL-brand-guidelines.pdf
end
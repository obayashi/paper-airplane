function [newX,newY] = cutZerosAndEndRepeats(X,Y)
% after getting trajectory, gets rid of the initial zeros and the trailing
% repeats after landing, if they exist

nonzero_idx = find(X~=0); % get rid of initial zeros
landed_idx = find(X == X(end)); % get rid of same pixels after landing
newX = X(nonzero_idx(1):landed_idx(1));
newY = Y(nonzero_idx(1):landed_idx(1));
end


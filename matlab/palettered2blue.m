function color = palettered2blue(min,max,qp)

    palette = {'#fc0433', '#df5500', '#b87700', '#8f8c00', '#64991d',...
        '#3b9f51', '#00a179', '#00a097', '#009bb7', '#0092de',...
        '#0082fb', '#1164fc'};


    vq = interp1([min max],[1 size(palette,2)],qp,'linear','extrap');
    
    vq = round(vq);
    if vq<1
        vq = 1;
    elseif vq>size(palette,2)
        vq = size(palette,2);
    end
    color = palette{vq};


    
end


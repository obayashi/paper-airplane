function [augX,augY] = augmentUndetected(rawX,rawY)
%after getting trajectory, some values are repeats because they didnt get
%detected by CV. for those repeated values, augment with linear
%interpolation from the values before and after the repeats
% this should give better fit and better velocity profile

temp = rawX;
diffvec = diff(temp);
idx = find(diffvec==0);
tempidx = [];
for k = 1:length(idx)
    tempidx(end+1) = idx(k)-1;
    tempidx(end+1) = idx(k)+1;
end
newidx = [];
for k = 1:length(tempidx)
    if ~any(idx==tempidx(k))
        newidx(end+1) = tempidx(k);
    end
end
newidx = newidx + 1;
augX = rawX;
augY = rawY;
for k = 1:(length(newidx)/2)
    X = [newidx(k*2-1) newidx(k*2)];
    V = rawX(X);
    Vh = rawY(X);
    Xq = X(1)+1:1:X(2)-1;
    Vq = interp1(X,V,Xq);
    Vqh = interp1(X,Vh,Xq);
    augX(Xq) = Vq;
    augY(Xq) = Vqh;
end

end


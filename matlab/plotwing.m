function plotwing(paper,wing_x,wing_y,hex)
% paper = [270 210] % for a4
% wing_x = [x1 x2 x3 x4] starting from nose
% wing_y = [y1 y2 y3 y4] starting from nose

paper_x = [0 0 max(paper) max(paper) 0];
paper_y = [0 min(paper) min(paper) 0 0];

plot(paper_x,paper_y,'k-')
hold on
wing_y = wing_y-(210/2);
wing_x = [wing_x wing_x(end-1:-1:1)];
wing_y = [wing_y wing_y(end-1:-1:1).*-1];
wing_y = wing_y + (210/2);
if nargin == 3   % if the number of inputs equals 3
%   z = 5 % then make the third value, z, equal to my default value, 5.
  plot(wing_x,wing_y,'LineWidth',2)
else
    plot(wing_x,wing_y,'LineWidth',2,'Color',hex)

end
xlim([0 297])
ylim([0 210])
axis equal
end


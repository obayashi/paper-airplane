function [wing_pts] = get_points(wing,paper)

no_cut_margin = paper.margin;
paper_size = paper.size;

x_points = [];
y_points = [];

% point 1
x_points(end+1) = no_cut_margin;
y_points(end+1) = 0; % this is always 0

% point 2
x_points(end+1) = x_points(end) + wing.b_half*tan(deg2rad(wing.sw_1));
y_points(end+1) = wing.b_half;

% point 3
x_points(end+1) = x_points(end) + wing.c_tip; %simplify by fixing sw2 at 90
y_points(end+1) = y_points(end);

% point 4
x_points(end+1) = no_cut_margin + wing.c_root;
y_points(end+1) = 0;

% half wing area
if wing.c_tip == 0 % triangle
    wing_pts.area = .5*wing.b_half*wing.c_root;
else % trapezoid
    wing_pts.area = ((wing.c_root+wing.c_tip)/2)*wing.b_half;
end

% wing points
x_wing = [x_points x_points(end-1:-1:1)];
y_wing = [y_points y_points(end-1:-1:1).*-1] + min(paper_size)/2;

wing_pts.x = x_wing;
wing_pts.y = y_wing;
wing_pts.area_poly = polyarea(x_wing,y_wing)/2;


end
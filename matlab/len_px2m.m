function len_m = len_px2m(len_px,max_m,min_m,max_px,min_px)
m = (max_m-min_m)/(max_px-min_px);
b = min_m-(m*min_px);

len_m = (m.*len_px)+b;
end
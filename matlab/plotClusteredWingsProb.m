function [plotteddesigns] = plotClusteredWingsProb(prob_strength,num_cluster,prob_cluster,wing_data,header)
plotteddesigns = {};
figure
for i = 1:num_cluster
    designstoplot = find(and(prob_cluster(i,:)>=min(prob_strength),prob_cluster(i,:)<=max(prob_strength)));
    plotteddesigns{i} = designstoplot;
    subplot(3,2,i)
    for j = 1:length(designstoplot)
        wing_x = wing_data(designstoplot(j),find(strcmp(header,'x1')):find(strcmp(header,'x4')));
        wing_y = wing_data(designstoplot(j),find(strcmp(header,'y1')):find(strcmp(header,'y4')));
        plotwing([297 210],wing_x,wing_y);
        hold on
    end
    title(['P(Cluster' num2str(i) ')=[' num2str(min(prob_strength)) ' ' num2str(max(prob_strength)) ']; ' num2str(length(designstoplot)) ' of 50 designs'])
    grid on
    axis equal
end
end
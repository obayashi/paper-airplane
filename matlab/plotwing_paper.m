function plotwing_paper(wing_x,wing_y,desnum)
% paper = [297 210] % for a4
% wing_x = [x1 x2 x3 x4] starting from nose
% wing_y = [y1 y2 y3 y4] starting from nose

% paper_x = [0 0 max(paper) max(paper) 0];
% paper_y = [0 min(paper) min(paper) 0 0];

% plot(paper_x,paper_y,'k-')
% hold on
wing_y = wing_y-(210/2);
wing_x = [wing_x wing_x(end-1:-1:1)];
wing_y = [wing_y wing_y(end-1:-1:1).*-1];
wing_y = wing_y + (210/2);
% area(wing_x,wing_y,'LineWidth',1,'FaceColor','#BDD7EE','EdgeColor','k')
fill(wing_x,wing_y,[189/255, 215/255, 238/255],'EdgeColor','k','LineWidth',2)
title(['Wing' num2str(desnum)])
hold on
axis equal
box off
% set(gcf, 'Color', 'None')
set(gca,'visible','off')

end


function wing = cluster2origpara(sweep,sweep_back,wol)

% %temp
% path = 'C:\Users\balto\switchdrive\PaperPlane\Experiments\0413_50samples\';
% fn = 'samples_50wings.mat';
% 
% data = load([path fn]).newmat;
% header = data(1,:);
% wing_data = cell2mat(data(2:end,:));
% 
% getidx = @(headerstr) find(strcmp(headerstr,header));
% sweep = wing_data(:,getidx('sweep'));
% sweep_back = wing_data(:,getidx('back_sweep'));
% wol = wing_data(:,getidx('w/l'));


idx_gt90 = find(sweep_back>90);
idx_lte90 = find(sweep_back<=90);

wing.sw_1 = sweep;

sweep = deg2rad(sweep);
sweep_back = deg2rad(sweep_back);

make_Ahalf = @(len) zeros(len,1)+10000;

bhalf_gt90 = eqn_bhalf_gt90(make_Ahalf(length(idx_gt90)),sweep(idx_gt90),sweep_back(idx_gt90),wol(idx_gt90));
cr_gt90 = eqn_cr_gt90(make_Ahalf(length(idx_gt90)),sweep(idx_gt90),sweep_back(idx_gt90),wol(idx_gt90));
ct_gt90 = eqn_ct_gt90(make_Ahalf(length(idx_gt90)),sweep(idx_gt90),sweep_back(idx_gt90),wol(idx_gt90));

bhalf_lte90 = eqn_bhalf_lte90(make_Ahalf(length(idx_lte90)),sweep(idx_lte90),sweep_back(idx_lte90),wol(idx_lte90));
cr_lte90 = eqn_cr_lte90(make_Ahalf(length(idx_lte90)),sweep(idx_lte90),sweep_back(idx_lte90),wol(idx_lte90));
ct_lte90 = eqn_ct_lte90(make_Ahalf(length(idx_lte90)),sweep(idx_lte90),sweep_back(idx_lte90),wol(idx_lte90));

temp_gt90 = [cr_gt90 ct_gt90 bhalf_gt90];
temp_lte90 = [cr_lte90 ct_lte90 bhalf_lte90];
tempmat = [temp_gt90(1:size(temp_gt90)/2,:);temp_lte90(1:size(temp_lte90)/2,:)];

allidx = [idx_gt90;idx_lte90];
tempmat = [allidx tempmat];

[sorted sortidx] = sort(allidx);
tempmat = tempmat(sortidx,:);

wing.c_root = tempmat(:,2);
wing.c_tip = tempmat(:,3);
wing.b_half = tempmat(:,4);


end


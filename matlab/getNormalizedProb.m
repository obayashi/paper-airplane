% function normProb = getNormalizedProb(points,cluster_num,modelStruct)
%     % changed to hidden function from inline function bc inline function
%     % was taking a long time in contour plotting bc the fitgmdist() was
%     % getting called several time for every point
%     clusterprobsum = zeros(size(points,1),1);
%     for j = 1:length(modelStruct)
%         % Probability that a point belongs to a cluster
%         probLowerTail = cdf(modelStruct(j).model,points);
%         % two-tailed prob
%         nonnormClusterProb = zeros(size(probLowerTail));
%         nonnormClusterProb(probLowerTail <= 0.5) = 2*probLowerTail(probLowerTail <= 0.5);
%         nonnormClusterProb(probLowerTail > 0.5) = 2*(1-probLowerTail(probLowerTail > 0.5));
%         clusterprobsum = clusterprobsum + nonnormClusterProb;
%         if j == cluster_num
%             cluster_num_prob = nonnormClusterProb;
%         end
%     end
%     
%     normProb = cluster_num_prob ./ clusterprobsum;
% end


function normProb = getNormalizedProb(points,cluster_num,modelStruct)

clusterprobsum = 0;
for j = 1:length(modelStruct)
    clusterprobsum = clusterprobsum + pdf(modelStruct(j).model,points);
end
normProb = pdf(modelStruct(cluster_num).model,points)./ clusterprobsum;
end

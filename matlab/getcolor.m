function [hex,color_pallette] = getcolor(str)
switch str
    case 'b'
        hex = '#1164fc';
    case 'o'
        hex = '#F25B21';
    case 'y'
        hex = '#F3C518';
    case 'p'
        hex = '#9B4FEA';
    case 'g'
        hex = '#0da362';
    case 'lb'
        hex = '#BDD7EE';
    case 'r'
        hex = '#fc0433';
    case 'gy' % gray
        hex = '#586F79';
    case 'pi' %pink
        hex = '#DD6EA6';
end
    color_pallette = {'#1164fc', '#F25B21', '#F3C518', '#9B4FEA', '#0da362', '#BDD7EE', '#fc0433','#586F79','#DD6EA6'};
    % order: blue, orange, yellow, purple, green, ltblue, red, gray
end
function [newX,newY] = deleteClusteredPoints(X,Y,threshold)
% after getting trajectory, especially after the plane lands, there are
% points that are clustered close to each other that may affect the
% polynomial fit of the data, so space out these points by threshold
% distance

newX = X(1);
newY = Y(1);
for k = 2:length(X)
    n = X(k-1);
    np1 = X(k);
    if abs(np1-n)>threshold
        newX(end+1) = np1;
        newY(end+1) = Y(k);
    end
end

end


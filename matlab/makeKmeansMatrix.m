function X = makeKmeansMatrix(bigmat,columns,rows)
if isequal(rows,'all')
    X = bigmat(:,columns);
else
    X = bigmat(rows,columns);
end


function [newX,newY] = cutBigJumps(X,Y,threshold)
% after obtaining raw airplane trajectory, use this code to cut out large
% "jumps" in data as a result of CV detecting something in the surrounding
% however this assumes that the "jump" occurs after the airplane has
% already landed

diffX = diff(X);
diffY = diff(Y);

if any(abs(diffX)>threshold)
    idx_jump = find(abs(diffX)>threshold);
    newX = X(1:idx_jump(1)-1);
    newY = Y(1:idx_jump(1)-1);
else
    newX = X; newY = Y;
end
% if any(abs(diffY)>threshold)
%     idx_temp = find(abs(diffY)>threshold);
%     if idx_temp<idx_jump
%         idx_jump = idx_temp;
%         newX = newX(1:idx_jump(1)-1);
%         newY = newY(1:idx_jump(1)-1);
%     end
% end


end


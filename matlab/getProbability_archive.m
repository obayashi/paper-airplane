%  Probability that a point belongs to a cluster
function prob = getProbability(points,model)

probLowerTail = cdf(model,points);
% two-tailed prob
prob = zeros(size(probLowerTail));
prob(probLowerTail<=0.5) = 2*probLowerTail(probLowerTail<=0.5);
prob(probLowerTail > 0.5) = 2*(1-probLowerTail(probLowerTail > 0.5));
% reject for GMM if prob < 0.05
end
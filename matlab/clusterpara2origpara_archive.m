function [wing] = clusterpara2origpara(wing,sweep,sweep_back,wid_len)

    sweep = deg2rad(sweep);
    sweep_back = deg2rad(sweep_back);
    Area_half = 10000;
    
    syms sw swb wol bhalf Ahalf
    
    %swb is less than or equal to 90
    A = bhalf/tan(pi/2-sw);
    
    if rad2deg(sweep_back)>90
        B = -1*bhalf*tan(swb-pi/2);
        ct = (bhalf*2)/wol-A;
    else
        B = bhalf*tan(pi/2-swb);
        ct = (bhalf*2)/wol-A-B;
    end
    
    A1 = 0.5*A*bhalf;
    A2 = bhalf*ct;
    A3 = 0.5*B*bhalf;
    eqn1 = Ahalf == A1+A2+A3;
    bhalf_solution = solve(eqn1,bhalf);
    A = bhalf_solution/tan(pi/2-sw);
    if rad2deg(sweep_back)>90
        B = -1*bhalf_solution*tan(swb-pi/2);
        ct = (bhalf_solution*2)/wol-A;
    else
        B = bhalf_solution*tan(pi/2-swb);
        ct = (bhalf_solution*2)/wol-A-B;
    end
    
    cr = A+B+ct;
    bhalf_ex1 = double(subs(bhalf_solution,[Ahalf,sw,swb,wol],[Area_half,sweep,sweep_back,wid_len]));
    ct_ex1 = double(subs(ct,[Ahalf,sw,swb,wol],[Area_half,sweep,sweep_back,wid_len]));
    cr_ex1 = double(subs(cr,[Ahalf,sw,swb,wol],[Area_half,sweep,sweep_back,wid_len]));
    
    wing.sw(end+1) = rad2deg(sweep);
    wing.cr(end+1) = max(cr_ex1);
    wing.ct(end+1) = max(ct_ex1);
    wing.bhalf(end+1) = max(bhalf_ex1);
% wing.sw = rad2deg(sweep);
%     wing.cr = max(cr_ex1);
%     wing.ct = max(ct_ex1);
%     wing.bhalf = max(bhalf_ex1);
end

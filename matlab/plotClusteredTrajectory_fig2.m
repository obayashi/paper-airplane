function plotClusteredTrajectory_fig2(num_cluster,trajtoshow,path_flinfo,idx,design_mat,origtrajidx,rootcluster,ynsub)
FS = 12;
figure
for i = 1:num_cluster
    actualtoshow = trajtoshow;
    singlecluster = find(idx==i);
    numincluster = sum(idx==i);
    if numincluster<actualtoshow
        actualtoshow = numincluster;
    end
    randomflights = randperm(numincluster,actualtoshow);
    origidx = origtrajidx(singlecluster(randomflights));
    subplot(1,3,i)
    for j = 1:actualtoshow
        flight = load([path_flinfo 'flinfo' num2str(design_mat(origidx(j),1)) '.mat']).flinfo{design_mat(origidx(j),2)};
        traj_dist = flight.filt_dist;
        traj_height = flight.filt_height;
        
        switch i
            case 1
                palette = palettered();
            case 2
                palette = palettegreen();
            case 3
                palette = paletteblue();
        end
        palnum = mod(j,10);
        if palnum == 0
            palnum = 10;
        end
        plot(traj_dist,traj_height,'LineWidth',2,'Color',palette{palnum})
        hold on
    end
    ax = gca;
    ax.FontSize = FS;
    if ynsub
        title(['random ' num2str(actualtoshow) ' of ' num2str(numincluster) ' flights from cluster ' num2str(rootcluster) char(64+i)])
        
    else
        title(['Flights from Cluster ' num2str(i)],'FontSize',FS)
    end
    xlabel('Distance [m]','FontSize',FS)
    ylabel('Height [m]','FontSize',FS)
    grid on
%     axis equal
    xlim([0 5])
    ylim([0 1.5])
    
end
end
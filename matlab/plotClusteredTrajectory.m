function plotClusteredTrajectory(num_cluster,trajtoshow,path_flinfo,idx,design_mat,origtrajidx,rootcluster,ynsub)
figure
% set(gcf,'WindowState','fullscreen')
for i = 1:num_cluster
    actualtoshow = trajtoshow;
    singlecluster = find(idx==i);
    numincluster = sum(idx==i);
    if numincluster<actualtoshow
        actualtoshow = numincluster;
    end
    randomflights = randperm(numincluster,actualtoshow);
    origidx = origtrajidx(singlecluster(randomflights));
    %     figure
    subplot(3,2,i)
    for j = 1:actualtoshow
        flight = load([path_flinfo 'flinfo' num2str(design_mat(origidx(j),1)) '.mat']).flinfo{design_mat(origidx(j),2)};
        traj_dist = flight.filt_dist;
        traj_height = flight.filt_height;
        %         traj_dist = flight.raw_dist;
        %         traj_height = flight.raw_height;
        plot(traj_dist,traj_height,'LineWidth',2)
        hold on
    end
    if ynsub
                title(['random ' num2str(actualtoshow) ' of ' num2str(numincluster) ' flights from cluster ' num2str(rootcluster) char(64+i)])

    else
        title(['random ' num2str(actualtoshow) ' of ' num2str(numincluster) ' flights from cluster ' num2str(i)])
    end
    xlabel('distance [m]')
    ylabel('height [m]')
    grid on
    axis equal
    xlim([0 5])
    ylim([0 1.5])
    
end
end
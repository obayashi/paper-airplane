function getFlightData(design_num,current)
tform_path = [current '\Calibration'];
in_folder = 'RawTrajectory';
out_folder = 'FlightInfo';
des_str = num2str(design_num);
tform = load(findYoungestFile(tform_path,'mat')).tform;
% tform = load([tform_path 'tform_floor.mat']).tform;

fps = 30;
min_dist_m = 0.19; % in meters %change this when recalibrating!
launcher_height_m = 1.4;
gnd_height_m = 0;
min_pixel = 1;


flinfo = load([current '\' in_folder '\flinfo' des_str '.mat']).flinfo;

for j = 1:length(flinfo)
    flight_info = flinfo{j};
    
    top_x_px = flight_info.top_x_pixel;
    top_y_px = flight_info.top_y_pixel;
    side_x_px = flight_info.side_x_pixel;
    side_y_px = flight_info.side_y_pixel;
    
    
    
    % check if there are no points that "jump" far for the side
    % trajectory
    [side_x_px_1,side_y_px_1] = cutBigJumps(side_x_px,side_y_px,200);
    
    % delete the zeros in the beginning and the repeats at the end for
    % both side and top
    [side_x_px_2,side_y_px_2] = cutZerosAndEndRepeats(side_x_px_1,side_y_px_1);
    [top_x_px_2,top_y_px_2] = cutZerosAndEndRepeats(top_x_px,top_y_px);
    
    % final landing spot
    %         if (i==37 && j==3) || (i==37 && j==4) || (i==37 && j==5) || (i==44 && j==4) || (i==44 && j==5)
    %             [worldPoints_X, worldPoints_Y] = transformPointsForward(tform2, top_x_px_2(end),top_y_px_2(end));
    %         else
    [worldPoints_X, worldPoints_Y] = transformPointsForward(tform, top_x_px_2(end),top_y_px_2(end));
    %         end
    flight_info.landing_spot = [worldPoints_X, worldPoints_Y];
    
    % pixel to meters for side trajectory
    dist_m = len_px2m(side_x_px_2,worldPoints_Y,min_dist_m,side_x_px_2(end),min_pixel);
    height_m = len_px2m(side_y_px_2,launcher_height_m,gnd_height_m,side_y_px_2(1),side_y_px_2(end));
    
    %if there is repeating values, it means that the plane detected was
    %too small so maybe do an interpolation between the regions?
    %augment with interpolated values? make sure the flight time doesnt
    %change
    % initially had this after cutBottom. but when it was cut first,
    % some cases ended up with repeats at the end of the vector, which
    % made it difficult to handle
    [dist_m_1,height_m_1] = augmentUndetected(dist_m,height_m);
    
    % get rid of the bottom cutoff distance
    [dist_m_2,height_m_2] = cutBottom(dist_m_1,height_m_1,0.03); % cut off bottom 3cm
    % maybe make exception for 17/3 because in 2d, it goes below the
    % landing point
    
    
    % get rid of points if there is reversal in direction by more than
    % 3cm (arbitrary) within 30 cm (arbitrary) of floor
    
    %         arb_height = 0.3;
    %         arb_dist = 0.03;
    %         below_height = find(raw_height<arb_height);
    %         idx_height = below_height(1);
    %         diff_dist = [0 diff(raw_dist)];
    %         reversal = find(and(abs(diff_dist)>(arb_dist),(diff_dist<0)));
    %         if ~isempty(reversal)
    %             idx_reversal = find(reversal>idx_height);
    %             if ~isempty(idx_reversal)
    %                 idx_reversal = reversal(idx_reversal(1));
    %                 raw_dist = raw_dist(1:idx_reversal-1);
    %                 raw_height = raw_height(1:idx_reversal-1);
    %             end
    %         end
    
    
    % find velocity
    vel_dist = diff(dist_m_2)./(1/fps);
    vel_dist = [0 vel_dist];
    vel_height = diff(height_m_2)./(1/fps);
    vel_height = [0 vel_height];
    
    % find approx time
    % check if this is correct!!!
    fl_time = (1/fps)*(length(dist_m_2)-1);
    vec_time = 0:(1/fps):fl_time;
    
    % get rid of clustered points in distance direction ~1cm?
    % (arbitrary) for better fit
    [dist_m_3,height_m_3] = deleteClusteredPoints(dist_m_2,height_m_2,0.01);
    
    % fit polynomial
    [fit_dist,fit_height,tstat,estimates] = fitPolynomial(dist_m_3,height_m_3);
    
    
    %         disp(['wing' num2str(i)])
    %         disp(['flight' num2str(j)])
    
    
    flight_info.filt_dist = dist_m_2;
    flight_info.filt_height = height_m_2;
    flight_info.raw_dist = dist_m;
    flight_info.raw_height = height_m;
    flight_info.vel_dist = vel_dist;
    flight_info.vel_height = vel_height;
    flight_info.fl_time = fl_time;
    flight_info.vec_time = vec_time;
    flight_info.fit_dist = fit_dist;
    flight_info.fit_height = fit_height;
    flight_info.tstat = tstat;
    flight_info.estimates = estimates;
    
    flinfo{j} = flight_info;
    
end
out_fn = ['flinfo' des_str '.mat'];
save([current '\' out_folder '\' out_fn],'flinfo');
% end

end

clc, clear, close all;
addpath(genpath('C:\Users\balto\switchdrive\PaperPlane\matlab'))

path_flinfo = 'C:\Users\balto\switchdrive\PaperPlane\Experiments\0413_50samples\FlightInfo\';
path_data = 'C:\Users\balto\switchdrive\PaperPlane\Experiments\0413_50samples\';

palette_epfl = paletteEPFL();

wing_data = load([path_data 'samples_50wings.mat']).newmat;
header = wing_data(1,:);
wing_data = cell2mat(wing_data(2:end,:));
[temp,pallette] = getcolor('b');

FS = 12;
MS = 10;
fig_width = 800;

num_designs = size(wing_data,1); %50
num_flights = 5;
km_mat = []; % num of coefficients
% design_mat = [];
rng(1)

%===wings to plot from 50 samples===
designs = [1 33 26 30];

%===make main data matrix and at same time print the trajectories for the selected 3 designs===
h = figure;
for i = 1:num_designs
    flinfo = load([path_flinfo 'flinfo' num2str(i) '.mat']).flinfo;
    for j = 1:length(flinfo)
        flight = flinfo{j};
        geom_para = wing_data(i,[find(strcmp(header,'sweep')) find(strcmp(header,'back_sweep')) find(strcmp(header,'w/l'))]);
        dist_para = flight.landing_spot(2);
        centdev_para = flight.landing_spot(1);
        fltime_para = flight.fl_time;
        
        km_mat(end+1,:) = [flight.tstat flight.estimates geom_para dist_para centdev_para fltime_para];
        %         design_mat(end+1,:) = [dataTrain(i) j];
        
        %plot trajectories
        if sum(designs == i) == 1
            traj_dist = flight.raw_dist;
            traj_height = flight.raw_height;
            if i == designs(1)
                traj_pal = palettered();
            elseif i == designs(2)
                traj_pal = palettegreen();
            elseif i == designs(3)
                traj_pal = paletteblue();
            end
            if i~= designs(4)
                plot(traj_dist,traj_height,'Color',traj_pal{j*2},'LineWidth',2)
                hold on
            end
            if i==designs(4)
                if j==1
                    traj_pal = palettered();
                    color = traj_pal{4};
                elseif j==2
                    traj_pal = palettegreen();
                    color = traj_pal{4};
                elseif j==3
                    traj_pal = palettered();
                    color = traj_pal{6};
                elseif j==4
                    traj_pal = palettegreen();
                    color = traj_pal{6};
                elseif j==5
                    traj_pal = palettegreen();
                    color = traj_pal{8};
                end
                plot(traj_dist,traj_height,'Color',color,'LineWidth',2)
            end
            
        end
    end
end
km_header = {'tstat5','tstat4','tstat3','tstat2','tstat1','tstat0',...
    'coeff5','coeff4','coeff3','coeff2','coeff1','coeff0',...
    'sweep','back_sweep','w/l','dist','centdev','fltime'};

distances = km_mat(:,find(strcmp('dist',km_header)));
dist_mat = reshape(distances,num_flights,num_designs);

FS = 14;
grid on
axis equal
xlim([0 5])
ylim([0 1.5])
xlabel('Distance [m]','FontSize',FS)
ylabel('Height [m]','FontSize',FS)
set(gca, 'XTick', 0:5,'FontSize',FS)
set(h,'Position',[100 100 fig_width 350])


%===make box chart===
boxidx = [ones(num_flights,1);ones(num_flights,1)+1;ones(num_flights,1)+2;ones(num_flights,1)+3];
h = figure;
    labels = [strcat("Des ",num2str(designs(1))),...
        strcat("Des ",num2str(designs(2))),...
        strcat("Des ",num2str(designs(3))),...
        strcat("Des ",num2str(designs(4)))];
    namedLabels = categorical(boxidx,1:4,labels);
    boxdist = [];
    for j = 1:length(designs)
        boxdist = [boxdist;dist_mat(:,designs(j))];
    end
    b = boxchart(namedLabels, boxdist,'BoxWidth',0.8);
    ylabel('Distance [m]','FontSize',FS)
    set(gca, 'YTick', 0:5,'FontSize',FS,'YAxisLocation','right')
    ylim([0 5])
    ytickangle(90)
grid on
box on
set(h,'Position',[100 100 250 fig_width])

%===wings===
allwings = designs;
figure
    for i = 1:length(allwings)
        subplot(size(designs,1),size(designs,2),i)
        desnum = allwings(i);
        wing_x = wing_data(desnum,find(strcmp(header,'x1')):find(strcmp(header,'x4')));
        wing_y = wing_data(desnum,find(strcmp(header,'y1')):find(strcmp(header,'y4')));
        if i == 1
            plotwing_nmipaper(wing_x,wing_y,desnum,1)
        elseif i ==2
            plotwing_nmipaper(wing_x,wing_y,desnum,2)
        elseif i==3
            plotwing_nmipaper(wing_x,wing_y,desnum,3)
        else 
            plotwing_nmipaper(wing_x,wing_y,desnum,0)
        end
        
    end


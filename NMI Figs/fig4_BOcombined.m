clc, clear, close all;
addpath(genpath('C:\Users\balto\switchdrive\PaperPlane\matlab'))

palette_epfl = paletteEPFL();
color_robot = '#c5e0b4';
color_human = '#bdd7ee';

%light palette
% color_b1 = '#fea1a0';
% color_b2 = '#98dab1';
% color_b3 = '#fea1a0';

%bright palette
color_b1 = '#fc0433';
color_b2 = '#0da362';
color_b3 = '#1164fc';

%===== human only ======
path = 'C:\Users\balto\switchdrive\PaperPlane\Experiments\GMMExperiments\';
exp_names = {'0519_target3','0521_target5'};
exp_names_ng = {'0519_t3_noGMM','0521_t5_noGMM'};
str_names = {'3m target','5m target'};
seed_num = [10 7];
patch_max = [5 5.5];
target_dist = [3 5];



% figure
for j = 1:length(exp_names)
    
    h = figure;
    
    km_mat = load(findYoungestFile([path exp_names{j} '\ClusterData'],'mat')).km_mat;
    km_header = km_mat(1,:);
    km_data = cell2mat(km_mat(2:end,:));
    dist = km_data(:,find(strcmp(km_header,'dist')));
    dist = reshape(dist,5,size(km_data,1)/5);
    avg_dist = mean(dist,1);
    error = std(dist,0,1);
    
    km_mat_ng = load(findYoungestFile([path exp_names_ng{j} '\ClusterData'],'mat')).km_mat;
    km_data_ng = cell2mat(km_mat_ng(2:end,:));
    dist_ng = km_data_ng(:,find(strcmp(km_header,'dist')));
    dist_ng = reshape(dist_ng,5,size(km_data_ng,1)/5);
    avg_dist_ng = mean(dist_ng,1);
    error_ng = std(dist_ng,0,1);
    
    % patches change depending on experiment
    FA = 0.6;
    MS = 6;
    
    v = [1 0; seed_num(j)+0.5 0; seed_num(j)+0.5 patch_max(j); 1 patch_max(j)];
    f = [1 2 3 4];
    patch('Faces',f,'Vertices',v,'EdgeColor','none','FaceColor',color_human,'FaceAlpha',FA)
    hold on
    
    plot([1 length(avg_dist)],[target_dist(j) target_dist(j)],'-','LineWidth',2,'Color',palette_epfl{2})
    
    
    errorbar(avg_dist_ng,error_ng,'LineWidth',2,'Color','#8B8D90')
    errorbar(avg_dist,error,'LineWidth',2,'Color',palette_epfl{5})
    
    % plot dots in color
    if j == 1 %target 3
        plot(11:20,avg_dist(11:end-1),'o','MarkerSize',MS,'MarkerEdgeColor',color_b2,'MarkerFaceColor',color_b2)
        plot(21,avg_dist(end),'o','MarkerSize',MS,'MarkerEdgeColor',color_b3,'MarkerFaceColor',color_b3)
    elseif j==2
        plot(8:20,avg_dist(8:end),'o','MarkerSize',MS,'MarkerEdgeColor',color_b3,'MarkerFaceColor',color_b3)
    end
    
    %     errorbar(x,y,err,'-s','MarkerSize',10,...
    %     'MarkerEdgeColor','red','MarkerFaceColor','red')
    
    
    %     plot([1 length(avg_dist)],[target_dist(j)-0.2 target_dist(j)-0.2],'--','LineWidth',2,'Color',palette_epfl{2})
    %     plot([1 length(avg_dist)],[target_dist(j)+0.2 target_dist(j)+0.2],'--','LineWidth',2,'Color',palette_epfl{2})
    
    xlim([1 21])
    ylim([0 patch_max(j)])
    FS = 9.5;
    xlabel('Iteration','FontSize',FS)
    ylabel('Distance [m]','FontSize',FS)
    %     title(str_names{j},'FontSize',FS)
    set(gca,'xtick',1:length(avg_dist),'FontSize',FS)
    set(gca,'ytick',0:5,'FontSize',FS)
    grid on
    set(h,'Position',[100 100 600 200])
end


% L1 = errorbar(nan, nan, 'LineWidth',2,'Color',palette_epfl{5});
% L2 = errorbar(nan, nan, 'LineWidth',2,'Color','#8B8D90');
% L3 = plot(nan, nan, '-','LineWidth',2,'Color',palette_epfl{2});
% L4 = plot(nan, nan, 'k.','LineWidth',2);
% legend([L1, L2, L3, L4], {'Distance + error (GMM)','Distance + error (No GMM)', 'Target distance','Seed airplanes'},'FontSize',FS,'NumColumns',4)

% % behavior legends
% L1 = plot(nan, nan, 'o','MarkerSize',MS,'MarkerEdgeColor',color_b1,'MarkerFaceColor',color_b1);
% L2 = plot(nan, nan, 'o','MarkerSize',MS,'MarkerEdgeColor',color_b2,'MarkerFaceColor',color_b2);
% L3 = plot(nan, nan, 'o','MarkerSize',MS,'MarkerEdgeColor',color_b3,'MarkerFaceColor',color_b3);
% legend([L1, L2, L3], {'Behavior 1', 'Behavior 2','Behavior 3'},'FontSize',FS,'NumColumns',3)


clc, clear, close all;
addpath(genpath('C:\Users\balto\switchdrive\PaperPlane\matlab'))

palette_epfl = paletteEPFL();
color_robot = '#c5e0b4';
color_human = '#bdd7ee';
color_b1 = '#fea1a0';
color_b2 = '#98dab1';
color_b3 = '#fea1a0';

path = 'C:\Users\balto\switchdrive\PaperPlane\NMI Figs\';

FA = 0.6;
FS = 14;

ystr = {'Sweep, \Lambda [deg]','Back sweep, \Lambda_b [deg]','Width-to-length, W/L'};
color = {palette_epfl{3} palette_epfl{6} palette_epfl{7}};
wingparastr = {'sweep','back_sweep','w/l'};

%===target 5 constraints===
t5_data = load([path 'fig4_constraints_t5.mat']).raw;
t5_num = cell2mat(t5_data(3:end,:));
t5_min = t5_num([1,1:end-1],2:4);
t5_max = t5_num([1,1:end-1],5:7);
t5_middle = t5_num([1,1:end-1],8:end);

wing_path = 'C:\Users\balto\switchdrive\PaperPlane\Experiments\GMMExperiments\0521_target5';
wing_mat = load(findYoungestFile([wing_path '\WingData'],'mat')).wing_data;
wing_header = wing_mat(1,:);
wing_data = cell2mat(wing_mat(2:end,:));

h = figure;
iternum = size(t5_num,1);
for i = 1:3
    subplot(3,1,i)
    v = [(1:iternum)' t5_max(:,i); (iternum:-1:1)' t5_min(end:-1:1,i)];
    f = 1:size(v,1);
    patch('Faces',f,'Vertices',v,'EdgeColor','none','FaceColor',color{i},'FaceAlpha',FA)
    hold on
    
    if i == 2
        v = [[10 11 11 10]',[t5_middle(10:11,5); t5_middle(11:-1:10,2)]];
        f = [1 2 3 4];
        patch('Faces',f,'Vertices',v,'EdgeColor','none','FaceColor','w','FaceAlpha',FA+0.4)
        v = [[18 19 20 20 19 18]',[t5_middle(18:20,5); t5_middle(20:-1:18,2)]];
        f = [1 2 3 4 5 6];
        patch('Faces',f,'Vertices',v,'EdgeColor','none','FaceColor','w','FaceAlpha',FA+0.4)
    end
    
    set(gca,'FontSize',FS,'XTick',1:iternum)
    ylabel(ystr{i},'FontSize',FS)
    xlim([1,iternum])
    
    % wing para
    plot(1:iternum,wing_data(:,find(strcmp(wingparastr{i},wing_header))),'kx','MarkerSize',12,'LineWidth',2);
    
end
set(h,'Position',[100 100 1100 600])
sgtitle('Target 5m')

%===target 3 constraints===
t3_data = load([path 'fig4_constraints_t3.mat']).raw;
t3_num = cell2mat(t3_data(3:end,:));
t3_min = t3_num([1,1:end-1],2:4);
t3_max = t3_num([1,1:end-1],5:7);
t3_middle = t3_num([1,1:end-1],8:end);

wing_path = 'C:\Users\balto\switchdrive\PaperPlane\Experiments\GMMExperiments\0519_target3';
wing_mat = load(findYoungestFile([wing_path '\WingData'],'mat')).wing_data;
wing_header = wing_mat(1,:);
wing_data = cell2mat(wing_mat(2:end,:));

h = figure;
iternum = size(t3_num,1);
for i = 1:3
    subplot(3,1,i)
    v = [(1:iternum)' t3_max(:,i); (iternum:-1:1)' t3_min(end:-1:1,i)];
    f = 1:size(v,1);
    patch('Faces',f,'Vertices',v,'EdgeColor','none','FaceColor',color{i},'FaceAlpha',FA)
    hold on
    
    if i == 1
        v = [[17:20 20:-1:17]',[t3_middle(17:20,4); t3_middle(20:-1:17,1)]];
        f = [1 2 3 4 5 6 7 8];
        patch('Faces',f,'Vertices',v,'EdgeColor','none','FaceColor','w','FaceAlpha',FA+0.4)
    end
    
    if i == 2
        v = [[17:20 20:-1:17]',[t3_middle(17:20,5); t3_middle(20:-1:17,2)]];
        f = [1 2 3 4 5 6 7 8];
        patch('Faces',f,'Vertices',v,'EdgeColor','none','FaceColor','w','FaceAlpha',FA+0.4)
    end
    
    set(gca,'FontSize',FS,'XTick',1:iternum)
    ylabel(ystr{i},'FontSize',FS)
    xlim([1,iternum])
    
    % wing para
    plot(1:iternum,wing_data(:,find(strcmp(wingparastr{i},wing_header))),'kx','MarkerSize',12,'LineWidth',2);
    
end
set(h,'Position',[100 100 1100 600])
sgtitle('Target 3m')

%===== no gmm 5m====
ranges = [0.0044 74.3492; %sw
    14.3756 151.6167; %bsw
    0.2553 1.7142]; %wol

for j = 1:2
wing_path = {'C:\Users\balto\switchdrive\PaperPlane\Experiments\GMMExperiments\0521_t5_noGMM',...
    'C:\Users\balto\switchdrive\PaperPlane\Experiments\GMMExperiments\0519_t3_noGMM'};
wing_mat = load(findYoungestFile([wing_path{j} '\WingData'],'mat')).wing_data;
wing_header = wing_mat(1,:);
wing_data = cell2mat(wing_mat(2:end,:));

h = figure;
iternum = size(wing_data,1);
for i = 1:3
    subplot(3,1,i)
    v = [1 max(ranges(i,:));iternum max(ranges(i,:));iternum min(ranges(i,:));1 min(ranges(i,:))];
    f = [1 2 3 4];
    patch('Faces',f,'Vertices',v,'EdgeColor','none','FaceColor',color{i},'FaceAlpha',FA)
    hold on
    
    set(gca,'FontSize',FS,'XTick',1:iternum)
    ylabel(ystr{i},'FontSize',FS)
    xlim([1,iternum])
    
    % wing para
    plot(1:iternum,wing_data(:,find(strcmp(wingparastr{i},wing_header))),'kx','MarkerSize',12,'LineWidth',2);
    
end
set(h,'Position',[100 100 1100 600])

if j == 1
sgtitle('No GMM Target 5m')
else
    sgtitle('No GMM Target 3m')
end

end



% L1 = errorbar(nan, nan, 'LineWidth',2,'Color',palette_epfl{7});
% L2 = plot(nan, nan, '-','LineWidth',2,'Color',palette_epfl{2});
% L3 = plot(nan, nan, '--','LineWidth',2,'Color',palette_epfl{2});
% legend([L1, L2, L3], {'Distance error bars', 'Target: 5 m','Target tolerance: 20 cm'},'FontSize',FS)


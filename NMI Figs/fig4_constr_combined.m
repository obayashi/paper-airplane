clc, clear, close all;
addpath(genpath('C:\Users\balto\switchdrive\PaperPlane\matlab'))

palette_epfl = paletteEPFL();
color_robot = '#c5e0b4';
color_human = '#bdd7ee';
color_b1 = '#fea1a0';
color_b2 = '#98dab1';
color_b3 = '#fea1a0';

path = 'C:\Users\balto\switchdrive\PaperPlane\NMI Figs\';

FA = 0.3;
FS = 9.5;
MS = 11;
fig_size = [100 100 600 400];

ystr = {'\Lambda [deg]','\Lambda_b [deg]','W/L'};
color = {palette_epfl{3} palette_epfl{6} palette_epfl{7}};
wingparastr = {'sweep','back_sweep','w/l'};

%===target 5 constraints===
t5_data = load([path 'fig4_constraints_t5.mat']).raw;
t5_num = cell2mat(t5_data(3:end,:));
t5_min = t5_num([1,1:end-1],2:4);
t5_max = t5_num([1,1:end-1],5:7);
t5_middle = t5_num([1,1:end-1],8:end);

wing_path = 'C:\Users\balto\switchdrive\PaperPlane\Experiments\GMMExperiments\0521_target5';
wing_mat = load(findYoungestFile([wing_path '\WingData'],'mat')).wing_data;
wing_header = wing_mat(1,:);
wing_data = cell2mat(wing_mat(2:end,:));

wing_path_ng = 'C:\Users\balto\switchdrive\PaperPlane\Experiments\GMMExperiments\0521_t5_noGMM';
wing_mat_ng = load(findYoungestFile([wing_path_ng '\WingData'],'mat')).wing_data;
wing_data_ng = cell2mat(wing_mat_ng(2:end,:));

h = figure;
iternum = size(t5_num,1);
for i = 1:3
    subplot(3,1,i)
    v = [(1:iternum)' t5_max(:,i); (iternum:-1:1)' t5_min(end:-1:1,i)];
    f = 1:size(v,1);
    patch('Faces',f,'Vertices',v,'EdgeColor','none','FaceColor',color{i},'FaceAlpha',FA)
    hold on
    
    if i == 2
        v = [[10 11 11 10]',[t5_middle(10:11,5); t5_middle(11:-1:10,2)]];
        f = [1 2 3 4];
        patch('Faces',f,'Vertices',v,'EdgeColor','none','FaceColor','w')
        v = [[18 19 20 20 19 18]',[t5_middle(18:20,5); t5_middle(20:-1:18,2)]];
        f = [1 2 3 4 5 6];
        patch('Faces',f,'Vertices',v,'EdgeColor','none','FaceColor','w')
    end
    
    set(gca,'FontSize',FS,'XTick',1:iternum)
    if i == 1
        set(gca,'FontSize',FS,'YTick',0:25:75)
        ylim([0 75])
    elseif i == 2
        set(gca,'FontSize',FS,'YTick',0:50:150)
        ylim([0 150])
    else
        set(gca,'FontSize',FS,'YTick',0:2)
        ylim([0 2])
    end
%     ylabel(ystr{i},'FontSize',FS)
    xlim([1,21])
    
    
    % non-gmm wing para
%     plot(1:size(wing_data_ng,1),wing_data_ng(:,find(strcmp(wingparastr{i},wing_header))),'o','Color','#8B8D90','MarkerSize',12,'LineWidth',2);

    % wing para
    plot(1:iternum,wing_data(:,find(strcmp(wingparastr{i},wing_header))),'x','Color',palette_epfl{5},'MarkerSize',MS,'LineWidth',2);
    
end
set(h,'Position',fig_size)
xlabel('Iteration','FontSize',FS)
% sgtitle('Target 5m')

%===target 3 constraints===
t3_data = load([path 'fig4_constraints_t3.mat']).raw;
t3_num = cell2mat(t3_data(3:end,:));
t3_min = t3_num([1,1:end-1],2:4);
t3_max = t3_num([1,1:end-1],5:7);
t3_middle = t3_num([1,1:end-1],8:end);

wing_path = 'C:\Users\balto\switchdrive\PaperPlane\Experiments\GMMExperiments\0519_target3';
wing_mat = load(findYoungestFile([wing_path '\WingData'],'mat')).wing_data;
wing_header = wing_mat(1,:);
wing_data = cell2mat(wing_mat(2:end,:));

wing_path_ng = 'C:\Users\balto\switchdrive\PaperPlane\Experiments\GMMExperiments\0519_t3_noGMM';
wing_mat_ng = load(findYoungestFile([wing_path_ng '\WingData'],'mat')).wing_data;
wing_data_ng = cell2mat(wing_mat_ng(2:end,:));

h = figure;
iternum = size(t3_num,1);
for i = 1:3
    subplot(3,1,i)
    v = [(1:iternum)' t3_max(:,i); (iternum:-1:1)' t3_min(end:-1:1,i)];
    f = 1:size(v,1);
    patch('Faces',f,'Vertices',v,'EdgeColor','none','FaceColor',color{i},'FaceAlpha',FA)
    hold on
    
    if i == 1
        v = [[17:20 20:-1:17]',[t3_middle(17:20,4); t3_middle(20:-1:17,1)]];
        f = [1 2 3 4 5 6 7 8];
        patch('Faces',f,'Vertices',v,'EdgeColor','none','FaceColor','w')
    end
    
    if i == 2
        v = [[17:20 20:-1:17]',[t3_middle(17:20,5); t3_middle(20:-1:17,2)]];
        f = [1 2 3 4 5 6 7 8];
        patch('Faces',f,'Vertices',v,'EdgeColor','none','FaceColor','w')
    end
    
    set(gca,'FontSize',FS,'XTick',1:iternum)
    if i == 1
        set(gca,'FontSize',FS,'YTick',0:25:75)
        ylim([0 75])
    elseif i == 2
        set(gca,'FontSize',FS,'YTick',0:50:150)
        ylim([0 150])
    else
        set(gca,'FontSize',FS,'YTick',0:2)
        ylim([0 2])
    end
    ylabel(ystr{i},'FontSize',FS)
    xlim([1,iternum])
    
    % non-gmm wing para
%     plot(1:size(wing_data_ng,1),wing_data_ng(:,find(strcmp(wingparastr{i},wing_header))),'o','Color',palette_epfl{6},'MarkerSize',12,'LineWidth',2);

    % wing para
    plot(1:iternum,wing_data(:,find(strcmp(wingparastr{i},wing_header))),'x','Color',palette_epfl{5},'MarkerSize',MS,'LineWidth',2);
    
end
set(h,'Position',fig_size)
xlabel('Iteration','FontSize',FS)
% sgtitle('Target 3m')


% L1 = plot(nan, nan, 'k.','LineWidth',2);
% L2 = plot(nan, nan, 'k.','LineWidth',2);
% L3 = plot(nan, nan, 'k.','LineWidth',2);
% L4 = plot(nan, nan, 'x','Color',palette_epfl{5},'MarkerSize',MS,'LineWidth',2);
% L5 = plot(nan, nan, 'k.','LineWidth',2);
% legend([L1, L2, L3, L4, L5], {'Constraint region for \Lambda',...
%     'Constraint region for \Lambda_b','Constraint region for W/L',...
%     'Sampled point','Corresponds to GMM in c)'},'FontSize',FS,'NumColumns',5)

% for fig 4 blobs
L1 = plot(nan, nan, 'k.','LineWidth',2);
L2 = plot(nan, nan, 'k.','LineWidth',2);
L3 = plot(nan, nan, 'k.','LineWidth',2);
legend([L1, L2, L3], {'Behavior 1','Behavior 2',...
    'Behavior 3'},'FontSize',FS,'NumColumns',3)


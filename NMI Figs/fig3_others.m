clc, clear, close all;
addpath(genpath('C:\Users\balto\switchdrive\PaperPlane\matlab'))


excel_path = 'C:\Users\balto\switchdrive\PaperPlane\Experiments\0413_50samples\';
excel_name = '0413_info.xlsx';
excel_sheet = 'Sheet8';

palette_epfl = paletteEPFL();

%=====actual vs. predicted (blue bar fig 3) plot with true line=====

T = readtable([excel_path excel_name],'Sheet',excel_sheet);
actual =  T.dist(~isnan(T.dist));
predict =  T.dist_1(~isnan(T.dist_1));

MS = 8;
LW = 2;
FS = 12;
h = figure;
plot([0 5],[0 5],'LineWidth',LW,'Color',palette_epfl{2})

hold on
plot(predict,actual,'o','Color',palette_epfl{3},'MarkerSize',MS,'MarkerFaceColor',palette_epfl{3})
xlabel('Predicted distance [m]','FontSize',FS)
ylabel('Average actual distance [m]','FontSize',FS)
set(gca,'FontSize',FS,'XTick',0:5,'YTick',0:5)
axis equal
xlim([0 5])
ylim([0 5])
grid on

L1 = plot(nan, nan, 'o','Color',palette_epfl{3},'MarkerSize',MS,'MarkerFaceColor',palette_epfl{3});
L2 = plot(nan, nan,'-', 'LineWidth',LW,'Color',palette_epfl{2});
legend([L1, L2], {'Withheld designs','Identity line'},'FontSize',FS,'Location','BEST')

set(h,'Position',[100 100 350 360])

% ==== error bar charts NN vs. GMM=====
excel_sheet = 'Sheet12';
T = readtable([excel_path excel_name],'Sheet',excel_sheet);

GMM_prob = [T.Var2(1:end-1) T.Var3(1:end-1) T.Var4(1:end-1)];
GMM_mean = mean(GMM_prob);
GMM_std = std(GMM_prob);

GPR_prob = abs([T.Var5(1:end-1) T.Var6(1:end-1) T.Var7(1:end-1)]);
GPR_mean = mean(GPR_prob);
GPR_std = std(GPR_prob);

figure
subplot(1,2,1)
x = 1:3;
data = GMM_mean;
errhigh = GMM_std./2;
errlow  = errhigh;
bar(x,data)                
hold on
er = errorbar(x,data,errlow,errhigh,'LineWidth',2);   
er.Color = [0 0 0];                            
er.LineStyle = 'none';
ylim([0 0.25])

subplot(1,2,2)
x = 1:3;
data = GPR_mean;
errhigh = GPR_std./2;
errlow  = errhigh;
bar(x,data)                
hold on
er = errorbar(x,data,errlow,errhigh,'LineWidth',2);  
er.Color = [0 0 0];                            
er.LineStyle = 'none';
ylim([0 0.25])



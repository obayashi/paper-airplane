clc, clear, close all;
addpath(genpath('C:\Users\balto\switchdrive\PaperPlane\matlab'))

palette_epfl = paletteEPFL();
% color_robot = '#c5e0b4';
% color_human = '#bdd7ee';
% color_bblue = ''

%===== human only ======
path = 'C:\Users\balto\switchdrive\PaperPlane\Experiments\0413_50samples\';
% exp_names = {'0526_HumanOnly','0521_t5_noGMM','0530_Human4Robot','0531_Robot4Human','0601_RoboHum'};


% for j = 1:length(exp_names)
    testidx = [2 8 9 16 17 18 21 28 34 38 39 40 42 47 50];
    wing_mat = load([path 'samples_50wings.mat']).newmat;
    header = wing_mat(1,:);
    wing_data = cell2mat(wing_mat(2:end,:));
    
    figure
    for i = 1:length(testidx)
        subplot(3,5,i)
        desnum = testidx(i);
        wing_x = wing_data(desnum,find(strcmp(header,'x1')):find(strcmp(header,'x4')));
        wing_y = wing_data(desnum,find(strcmp(header,'y1')):find(strcmp(header,'y4')));
        plotwing_nmipaper(wing_x,wing_y,desnum,0)
        
    end
% end

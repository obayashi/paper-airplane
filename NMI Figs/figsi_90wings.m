clc, clear, close all;
addpath(genpath('C:\Users\balto\switchdrive\PaperPlane\matlab'))

path_flinfo = 'C:\Users\balto\switchdrive\PaperPlane\Experiments\0413_50samples\FlightInfo_addmore\';
path_data = 'C:\Users\balto\switchdrive\PaperPlane\Experiments\0413_50samples\';

wing_data = load([path_data 'samples_50wings.mat']).newmat;
header = wing_data(1,:);
% wing_data = cell2mat(wing_data(2:end,:));
wing_data = load([path_data 'wing_data_addmore.mat']).wing_data;
wing_data = wing_data(1:90,:); % just so we only plot 90 (arbitrary)

figure
for i = 1:90
    subplot(10,9,i)
    wing_x = wing_data(i,find(strcmp(header,'x1')):find(strcmp(header,'x4')));
    wing_y = wing_data(i,find(strcmp(header,'y1')):find(strcmp(header,'y4')));
    plotwing_nmipaper(wing_x,wing_y,i,0)

end


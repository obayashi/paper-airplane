clc, clear, close all;
addpath(genpath('C:\Users\balto\switchdrive\PaperPlane\matlab'))

palette_epfl = paletteEPFL();
% color_robot = '#c5e0b4';
% color_human = '#bdd7ee';
% color_bblue = ''

%===== human only ======
path = 'C:\Users\balto\switchdrive\PaperPlane\Experiments\0524_VennDiagram\WingData\';
% exp_names = {'0526_HumanOnly','0521_t5_noGMM','0530_Human4Robot','0531_Robot4Human','0601_RoboHum'};


% for j = 1:length(exp_names)
    wing_mat = load([path 'wing_data_venn.mat']).wing_data;
    header = wing_mat(1,:);
    wing_data = cell2mat(wing_mat(2:end,:));
    
    figure
    for i = 1:size(wing_data,1)
        subplot(3,5,i)
%         desnum = testidx(i);
        wing_x = wing_data(i,find(strcmp(header,'x1')):find(strcmp(header,'x4')));
        wing_y = wing_data(i,find(strcmp(header,'y1')):find(strcmp(header,'y4')));
        desnum = wing_data(i,find(strcmp(header,'idx_orig')));
        plotwing_nmipaper(wing_x,wing_y,desnum)
        
    end
% end

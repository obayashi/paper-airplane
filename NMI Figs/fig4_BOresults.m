clc, clear, close all;
addpath(genpath('C:\Users\balto\switchdrive\PaperPlane\matlab'))

palette_epfl = paletteEPFL();
color_robot = '#c5e0b4';
color_human = '#bdd7ee';
color_b1 = '#fea1a0';
color_b2 = '#98dab1';
color_b3 = '#fea1a0';

%===== human only ======
path = 'C:\Users\balto\switchdrive\PaperPlane\Experiments\GMMExperiments\';
exp_names = {'0519_target3','0519_t3_noGMM','0521_target5','0521_t5_noGMM'};
str_names = {'3m gmm','3m no gmm','5m gmm','5m no gmm'};
seed_num = [10 10 7 7];
patch_max = [5 5 6 6];
target_dist = [3 3 5 5];


for j = 1:length(exp_names)

    wing_mat = load(findYoungestFile([path exp_names{j} '\WingData'],'mat')).wing_data;
    header = wing_mat(1,:);
    wing_data = cell2mat(wing_mat(2:end,:));
    
    switch j
        case 1 % t3
%             behavior = [1 3 2 2 1 3 2 2 3 1 2 2 2 2 2 2 2 2 2 3 2]; % chosen cluster from after seed
            behavior = [1 3 2 2 1 3 2 2 3 1 1 2 1 1 2 2 1 2 2 2 2];
        case 2 % t3 no gmm
            behavior = zeros(1,size(wing_data,1))+4;
        case 3 % t5 
%             behavior = [1 2 3 3 2 3 2 3 3 3 3 3 3 3 3 3 3 3 3 3]; % chosen cluster from after seed
            behavior = [1 2 3 3 2 3 2 2 2 1 3 1 2 1 3 3 3 3 3 3];
        case 4 % t5 no gmm
            behavior = zeros(1,size(wing_data,1))+4;
    end
         

    figure
    for i = 1:size(wing_data,1)
        subplot(4,6,i)
        wing_x = wing_data(i,find(strcmp(header,'x1')):find(strcmp(header,'x4')));
        wing_y = wing_data(i,find(strcmp(header,'y1')):find(strcmp(header,'y4')));
        desnum = wing_data(i,find(strcmp(header,'idx_orig')));
        plotwing_nmipaper(wing_x,wing_y,desnum,behavior(i))
        sgtitle(exp_names{j})

    end
end

% figure
for j = 1:length(exp_names)
    
    h = figure;
    
    km_mat = load(findYoungestFile([path exp_names{j} '\ClusterData'],'mat')).km_mat;
    km_header = km_mat(1,:);
    km_data = cell2mat(km_mat(2:end,:));
    dist = km_data(:,find(strcmp(km_header,'dist')));
    dist = reshape(dist,5,size(km_data,1)/5);
    avg_dist = mean(dist,1);
    error = std(dist,0,1);
    %     min_error = abs(min(dist)-avg_dist);
    %     max_error = abs(max(dist)-avg_dist);
    %     error = mean([min_error;max_error]);
    
    % patches change depending on experiment
    FA = 0.6;
    
    v = [1 0; seed_num(j)+0.5 0; seed_num(j)+0.5 patch_max(j); 1 patch_max(j)];
    f = [1 2 3 4];
    patch('Faces',f,'Vertices',v,'EdgeColor','none','FaceColor',color_human,'FaceAlpha',FA)
    hold on

    
    
    errorbar(avg_dist,error,'LineWidth',2,'Color',palette_epfl{5})
    plot([1 length(avg_dist)],[target_dist(j) target_dist(j)],'-','LineWidth',2,'Color',palette_epfl{2})
%     plot([1 length(avg_dist)],[target_dist(j)-0.2 target_dist(j)-0.2],'--','LineWidth',2,'Color',palette_epfl{2})
%     plot([1 length(avg_dist)],[target_dist(j)+0.2 target_dist(j)+0.2],'--','LineWidth',2,'Color',palette_epfl{2})
    
        xlim([1 21])
        ylim([0 patch_max(j)])
    FS = 14;
    xlabel('Iteration','FontSize',FS)
    ylabel('Distance [m]','FontSize',FS)
%     title(str_names{j},'FontSize',FS)
    set(gca,'xtick',1:length(avg_dist),'FontSize',FS)
    grid on
    set(h,'Position',[100 100 1100 250])
end
% L1 = errorbar(nan, nan, 'LineWidth',2,'Color',palette_epfl{5});
% L2 = plot(nan, nan, '-','LineWidth',2,'Color',palette_epfl{2});
% L3 = plot(nan, nan, '.','LineWidth',2);
% legend([L1, L2, L3], {'Distance + error', 'Target distance','Seed airplanes'},'FontSize',FS,'NumColumns',3)
L1 = plot(nan, nan, '.','LineWidth',2);
L2 = plot(nan, nan, '.','LineWidth',2);
L3 = plot(nan, nan, '.','LineWidth',2);
legend([L1, L2, L3], {'Behavior 1','Behavior 2','Behavior 3'},'FontSize',FS,'NumColumns',3)

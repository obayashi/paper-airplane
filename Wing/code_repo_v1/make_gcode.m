function gcode_cell = make_gcode(wist_pts,laser,public)

wist_x = round(wist_pts.x,1);
wist_y = round(wist_pts.y,1);

gcode_cell = {};
gcode_cell{end+1} = "G90";
gcode_cell{end+1} = "M3 S0";
gcode_cell{end+1} = "S0";
gcode_cell{end+1} = "G0X0Y0Z0";
gcode_cell{end+1} = string(['G0X' num2str(wist_x(1)) 'Y' num2str(wist_y(1))]);
gcode_cell{end+1} = string(['S' num2str(laser.intensity)]);
for i = 1:laser.passes
    for j = 1:length(wist_x)
        gcode_cell{end+1} = string(['G1X' num2str(wist_x(j)) 'Y' num2str(wist_y(j)) 'F' num2str(laser.rate)]);
    end
end
gcode_cell{end+1} = string(['G1X' num2str(wist_x(2)) 'Y' num2str(wist_y(2))  'F' num2str(laser.rate)]);
gcode_cell{end+1} = "S0";
gcode_cell{end+1} = "M5 S0";
gcode_cell{end+1} = "G0X0Y0Z0";


% save gcode
current = pwd;
folder = strcat(current,'/out/gcode');
if not(isfolder(folder))
    mkdir(folder)
end

fileID = fopen(strcat(folder,'/',public.name,'.gcode'),'w');
fprintf(fileID, '%s\n', gcode_cell{:});


end


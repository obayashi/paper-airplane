function [wing_pts] = get_points(wing_para,paper_size)

x_points = [];
y_points = [];


% point 1
x_points(end+1) = wing_para.a;
y_points(end+1) = 0; % this is always 0

% point 2
x_points(end+1) = x_points(end) + wing_para.b_half*tan(deg2rad(wing_para.sw_1));
y_points(end+1) = wing_para.b_half;

% point 3
x_points(end+1) = x_points(end) + wing_para.c_tip; %simplify by fixing sw2 at 90
y_points(end+1) = y_points(end);
% x_points(end+1) = x_points(end) + wing_para.c_tip*sin(deg2rad(wing_para.sw_2));
% y_points(end+1) = y_points(end) + wing_para.c_tip*cos(deg2rad(wing_para.sw_2));


% point 4
x_points(end+1) = wing_para.a + wing_para.c_root;
y_points(end+1) = 0;




% half wing area
if wing_para.c_tip == 0 % triangle
    wing_pts.area = .5*wing_para.b_half*wing_para.c_root;
else % trapezoid
    wing_pts.area = ((wing_para.c_root+wing_para.c_tip)/2)*wing_para.b_half;
end


% wing points
x_wing = [x_points x_points(end-1:-1:1)];
y_wing = [y_points y_points(end-1:-1:1).*-1] + min(paper_size)/2;

% paper points
% x_paper = [0 max(paper_size) max(paper_size) 0 0];
% y_paper = [-1*min(paper_size)/2 -1*min(paper_size)/2 min(paper_size)/2 min(paper_size)/2 -1*min(paper_size)/2] + min(paper_size)/2;

wing_pts.x = x_wing;
wing_pts.y = y_wing;
wing_pts.area_poly = polyarea(x_wing,y_wing)/2;

% paper_pts.x = x_paper;
% paper_pts.y = y_paper;


end
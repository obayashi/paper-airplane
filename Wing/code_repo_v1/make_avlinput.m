function make_avlinput(wing_pts,paper,wing,public)

% necessary parameters
NACA = "0003";
Sref = round(wing_pts.area_poly*2,1);
taper = wing.c_tip/wing.c_root;
Cref = round(wing.c_root*(2/3)*((1+taper+(taper^2))/(1+taper)),1);
Bref = round(wing.b_half*2,1);
Xref = round(paper.cg*max(paper.size),1);
Xle1 = round(wing.a,1);
Yle1 = 0.0;
Zle1 = 0.0;
Chord1 = round(wing.c_root,1);
Xle2 = round(wing_pts.x(2),1);
Yle2 = round(wing.b_half,1);
Zle2 = 0.0;
Chord2 = round(wing.c_tip,1);

%write stuff
avl_cell = {};
avl_cell{end+1} = public.name;
avl_cell{end+1} = "#Mach";
avl_cell{end+1} = "0.0";
avl_cell{end+1} = "#IYsym IZsym Zsym";
avl_cell{end+1} = "0 0 0.0";
avl_cell{end+1} = "#Sref Cref Bref";
avl_cell{end+1} = string(strcat(num2str(Sref),{' '},num2str(Cref),{' '},num2str(Bref)));
avl_cell{end+1} = "#Xref Yref Zref";
avl_cell{end+1} = string(strcat(num2str(Xref),' 0.0 0.0'));
avl_cell{end+1} = "#--------------------------------------------------";
avl_cell{end+1} = "SURFACE";
avl_cell{end+1} = "Wing";
avl_cell{end+1} = "#Nchordwise Cspace Nspanwise Sspace";
avl_cell{end+1} = "14 1.0";
avl_cell{end+1} = "YDUPLICATE";
avl_cell{end+1} = "0.0";
avl_cell{end+1} = "ANGLE";
avl_cell{end+1} = "0.0";
avl_cell{end+1} = "SCALE";
avl_cell{end+1} = "1.0 1.0 1.0";
avl_cell{end+1} = "TRANSLATE";
avl_cell{end+1} = "0.0 0.0 0.0";
avl_cell{end+1} = "SECTION";
avl_cell{end+1} = "#Xle Yle Zle Chord Ainc Nspanwise Sspace";
avl_cell{end+1} = string(strcat(num2str(Xle1),{' '},num2str(Yle1),{' '},num2str(Zle1),{' '},num2str(Chord1),' 0.0 20 1.0'));
avl_cell{end+1} = "NACA";
avl_cell{end+1} = NACA;
avl_cell{end+1} = "SECTION";
avl_cell{end+1} = "#Xle Yle Zle Chord Ainc Nspanwise Sspace";
avl_cell{end+1} = string(strcat(num2str(Xle2),{' '},num2str(Yle2),{' '},num2str(Zle2),{' '},num2str(Chord2),' 0.0 0 1.0'));
avl_cell{end+1} = "NACA";
avl_cell{end+1} = NACA;

% save avl input
current = pwd;
folder = strcat(current,'/out/avl');
if not(isfolder(folder))
    mkdir(folder)
end

fileID = fopen(strcat(folder,'/',public.name,'.avl'),'w');
fprintf(fileID, '%s\n', avl_cell{:});

run_avl(public);

end
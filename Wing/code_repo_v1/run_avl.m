function run_avl(public)
%% INPUT variables
filename = strcat(pwd,'\out\avl\',public.name);

%% Directory Preparation -----> NOT WORKING
%Purge Directory of interfering files
[status,result] =dos(string(strcat('del',{' '},filename,'.ft')));
[status,result] =dos(string(strcat('del',{' '},filename,'.run')));

%% Create run file
%Open the file with write permission
fid = fopen(strcat(filename,'.run'), 'w');

%Load the AVL definition of the aircraft
fprintf(fid, 'LOAD %s%s\n',string(filename),'.avl');

%Disable Graphics
fprintf(fid, 'PLOP\ng\n\n'); 

%Open the OPER menu
fprintf(fid, '%s\n',   'OPER');   

%Define the run case
fprintf(fid, 'a a %d\n',public.alpha);       

%Run the Case
fprintf(fid, '%s\n',   'x'); 

%Save the ft data
fprintf(fid, '%s\n',   'ft'); 
fprintf(fid, '%s%s\n',filename,'.ft');   

%Drop out of OPER menu
fprintf(fid, '%s\n',   '');

%Quit Program
fprintf(fid, 'Quit\n'); 

%Close File
fclose(fid);

%% Execute Run
%Run AVL using 
[status,result] = dos(string(strcat(pwd,'\code_repo\avl.exe <',{' '},filename,'.run')));
end

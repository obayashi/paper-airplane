function fig = make_visual(paper,margin,wing,wist,public)

paper_x = [0 0 max(paper) max(paper) 0];
paper_y = [0 min(paper) min(paper) 0 0];

margin_x = [margin margin max(paper)-margin max(paper)-margin margin];
margin_y = [margin min(paper)-margin min(paper)-margin margin margin];



fig = figure;
subplot(1,2,1)
plot(paper_x,paper_y,'k','LineWidth',1.5)
hold on 
plot(margin_x,margin_y,'k--')
plot(wing.x,wing.y)
legend('Paper','Margin','Wing','Location','BEST')
xlabel('x [mm]')
ylabel('y [mm]')
axis equal
grid on

subplot(1,2,2)
plot(paper_x,paper_y,'k','LineWidth',1.5)
hold on 
plot(margin_x,margin_y,'k--')
plot(wist.x,wist.y)
legend('Paper','Margin','Wing/Stick','Location','BEST')
xlabel('x [mm]')
ylabel('y [mm]')
axis equal
grid on

% save figure and points
current = pwd;
folder = strcat(current,'/out/wing');
if not(isfolder(folder))
    mkdir(folder)
end

saveas(fig,strcat(folder,'/',public.name,'.png'))
close(fig)

% wing.x wing.y
writematrix([round(wing.x,1)',round(wing.y,1)'],...
    strcat(folder,'/',public.name,'_wing.txt'),'Delimiter',';')
writematrix([round(wist.x,1)',round(wist.y,1)'],...
    strcat(folder,'/',public.name,'_wist.txt'),'Delimiter',';')



end
function [wing_pts, wist_pts, wing_para] = make_generic(paper)

paper_size = paper.size;
no_cut_margin = paper.margin;
base_width = paper.base;

stick_size = [base_width max(paper_size)]; %1cm width

    
% generic wing
wing.a = 10;
wing.c_root = 240;
wing.c_tip = 60;
wing.b_half = 70;
wing.sw_1 = 70;

wing_pts = get_points(wing,paper_size);
wing_para = wing;


[gng, wist_pts] = wist_intersection(paper_size,stick_size,wing_pts,no_cut_margin);
            

end
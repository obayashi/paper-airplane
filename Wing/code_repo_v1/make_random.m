function [wing_pts, wist_pts, wing_para] = make_random(paper, num_designs)

paper_size = paper.size;
no_cut_margin = paper.margin;
base_width = paper.base_width;
base_length = paper.base_length;

stick_size = [base_width base_length]; %1cm width
area_fixed = 8000; %mm2 this is half only

count = 1;

while count <= num_designs
    
    % random parameters
    a = (max(paper_size)-0).*rand(1,1) + 0;
    c_root = (max(paper_size)-0).*rand(1,1) + 0;
    b_half = (min(paper_size)/2-0).*rand(1,1) + 0;
    sw_1 = (90-0).*rand(1,1) + 0;
    
    
    wing.a = a;
    wing.c_root = c_root;
    wing.b_half = b_half;
    wing.sw_1 = sw_1;

    wing.c_tip = (2/wing.b_half)*area_fixed - wing.c_root;

    wing_pts = get_points(wing, paper_size);
    
    if any(wing_pts.x < 0+no_cut_margin) || any(wing_pts.x > max(paper_size)-no_cut_margin) || any(wing_pts.y<0+no_cut_margin) || any(wing_pts.y>min(paper_size)-no_cut_margin)
        note_str = 99; % means OOB
    elseif wing.c_root <= wing.c_tip 
        note_str = 98; % tip must be smaller or equal to root chord
    elseif wing.c_tip < 0
        note_str = 97; % solved tip chord is negative
    else
        [gng, wist_pts] = wist_intersection(paper_size,stick_size,wing_pts,no_cut_margin);
        if gng == 0
            note_str = 97; % stick intersection doesnt have 2 points
        else
  
            note_str = 1; % means FIG exists
            wing_para = wing;
            
            count = count + 1;
        end
            
            
            
    end

end


end
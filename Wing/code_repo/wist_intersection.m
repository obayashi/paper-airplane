function [gng, wist_pts] = wist_intersection(paper_size,stick_size,wing_pts,no_cut_margin)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

% move wing point back to middle and just get half
wing_x = wing_pts.x(1:length(wing_pts.x)/2+1);
wing_y = wing_pts.y(1:length(wing_pts.y)/2+1) - min(paper_size)/2;

% stick points top half
stick_x_start = max(paper_size)/2-max(stick_size)/2;
stick_x_end = stick_x_start+max(stick_size);
x_stick = [stick_x_start stick_x_start stick_x_end stick_x_end];
y_stick = [0 min(stick_size)/2 min(stick_size)/2 0];

% intersection front
[xi,yi] = polyxpoly(wing_x, wing_y, x_stick, y_stick);

% for checking
paper_x = [0 0 max(paper_size) max(paper_size)];
paper_y = [0 min(paper_size)/2 min(paper_size)/2 0];
margin_x = [no_cut_margin no_cut_margin max(paper_size)-no_cut_margin max(paper_size)-no_cut_margin];
margin_y = [0 min(paper_size)/2-no_cut_margin min(paper_size)/2-no_cut_margin 0];
figure 
plot(wing_x, wing_y, x_stick, y_stick,paper_x,paper_y,margin_x,margin_y)
hold on 
plot(xi,yi,'*')
axis equal

% find if there are intersections
% also think of cases where there are no intersections ( wing is either
% tiny which is unlikely, or the wing is sufficiently big)
% if there are intersections, there could be one on each side, or there
% could possibly be more than one on each side
% find if the intersections occur in the front or back or both
% sort the combined (wing and intersection) x locations
% if there are intersections, that mean
% i think we can generalize, if there is only one intersection on each
% side, the front of the base should be added
% also generalize if there are two intersection on each side, only the
% corner should be included
% and finally, try to reduce the number of cuts 

if length(xi) == 2

    gng = 1;

    % combined wing and stick
    x_wist = [x_stick(1:2) xi(1) wing_x(2:end-1) xi(end) x_stick(end-1:end)];
    y_wist = [y_stick(1:2) yi(1) wing_y(2:end-1) yi(end) y_stick(end-1:end)];
    x_wist = [x_wist x_wist(end-1:-1:1)];
    y_wist = [y_wist y_wist(end-1:-1:1).*-1]+ min(paper_size)/2;

    wist_pts.x = x_wist;
    wist_pts.y = y_wist;
    
%     figure
%     plot(x_wist,y_wist)
%     axis equal
else 
    gng = 0;
    wist_pts = [];
end


end
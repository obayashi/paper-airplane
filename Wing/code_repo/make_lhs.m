function wing_para_mat = make_lhs(paper,area,num_design)

paper_size = paper.size;
no_cut_margin = paper.margin;

area_fixed = area; %mm2 this is half only

% min and max of parameters from 600 random test
min_bhalf = 30; 
max_bhalf = (min(paper_size)-(2*no_cut_margin))/2;
min_croot = 75; 
max_croot = 265; 
min_ctip = 0;
max_ctip = area_fixed/2.5/min_bhalf; % how to get this
min_A = 0;
max_A = max(paper_size)-80; 

% matrix to store wing parameters for lhs
wing_para_mat = zeros(num_design,4);
% matrix to store wing points 
wing_pts_x_mat = zeros(num_design,7);
wing_pts_y_mat = zeros(num_design,7);


success = 0;
iter = 0;

while not(success)

    lhs_para = lhsdesign(num_design,3); % p = 3 for croot, ctip, A
    % min+(max-min).*xcol
    lhs_para = [min_croot+(max_croot-min_croot).*lhs_para(:,1)...
        min_ctip+(max_ctip-min_ctip).*lhs_para(:,2)...
        min_A+(max_A-min_A).*lhs_para(:,3)];

    count = 1;
    iter = iter + 1;
    if isequal(mod(iter,1000),0)
        disp(strcat('iteration: ',num2str(iter)))
    end

    while count <= num_design
        wing.c_root = lhs_para(count,1);
        wing.c_tip = lhs_para(count,2);
        wing.A = lhs_para(count,3);


        % calculate implied parameters
        wing.b_half       = area_fixed ./ (wing.A./2 + wing.c_tip + (wing.c_root-wing.c_tip-wing.A)./2);
        wing.sw_1      = rad2deg(pi/2 - atan(wing.b_half./wing.A));
        
        wing_pts = get_points(wing,paper);
        
        wing_para_mat(count,:) = [wing.c_root,wing.c_tip,wing.b_half,wing.sw_1];
        wing_pts_x_mat(count,:) = wing_pts.x;
        wing_pts_y_mat(count,:) = wing_pts.y;

        if any(wing_pts.x < 0+no_cut_margin) || any(wing_pts.x > max(paper_size)-no_cut_margin) || any(wing_pts.y<0+no_cut_margin) || any(wing_pts.y>min(paper_size)-no_cut_margin)
%             note_str = 99; % means OOB
%             disp('out of bounds')
            count = num_design+1;
        elseif wing.c_root <= wing.c_tip 
%             note_str = 98; % tip must be smaller or equal to root chord
%             disp('ctip>croot')
            count = num_design+1;
        elseif wing.c_tip < 0
%             note_str = 97; % solved ctip is negative
%             disp('negative ctip')
            count = num_design+1;
        elseif (wing.b_half<min_bhalf) || (wing.b_half>max_bhalf)
%             note_str = 96; % solved half span is not within min/max
%             disp('half span OOB')
            count = num_design+1;
        else
%             note_str = 1; % means FIG exists
%             disp(num2str(count))

            count = count + 1;
            if isequal(count,num_design+1)
                success = 1;
            end

        end

    end
end


% test figure
paper_x = [0 0 max(paper_size) max(paper_size) 0];
paper_y = [0 min(paper_size) min(paper_size) 0 0];
margin_x = [no_cut_margin no_cut_margin max(paper_size)-no_cut_margin max(paper_size)-no_cut_margin no_cut_margin];
margin_y = [no_cut_margin min(paper_size)-no_cut_margin min(paper_size)-no_cut_margin no_cut_margin no_cut_margin];
figure
plot(paper_x,paper_y,'k-',margin_x,margin_y,'k--')
hold on
for i = 1:num_design
    plot(wing_pts_x_mat(i,:),wing_pts_y_mat(i,:))
end

disp(num2str(iter))

end

clear rtde_r; clear rtde_c; clear a

import py.rtde_receive.RTDEReceiveInterface
import py.rtde_control.RTDEControlInterface
% Arduino Setup
a = arduino('COM7', 'UNO');
%Robot setup
rtde_r = RTDEReceiveInterface("192.168.1.21");
rtde_c = RTDEControlInterface("192.168.1.21");

% aero_center = aero.ac_x;
aero_center = aero.ac_x-3*(aero.ac_x/10); % 10% forward

clear_cutter = [82.96,-98.01,-105.03,-67.67,-88.66,-78.03];
holder_near_paper = [91.90,-102.03,-115.74,-52.86,90.62,87.57];
holder_at_centerline_old = [87.09,-125.37,-91.66,-53.57,90.78,85.3];
place_on_launcher = [64.72,-110.45,-92.98,-67.52,-88.88,-61.93];
paper_corner = [89.97,-112.71,-120.17,-38.01,-88.75,2.64];
place_paper = [79.72,-123.75,-102.33,-42.93,-89.98,-149.5];
paper_stack = [59.51,-81.71,-157.45,-29.79,-89.79,-129.45];
holder_pos = [13.56,-104.06,-131.2,-34.44,90.16,-256.09];
tapestart = [78.9,-120.22,-99.35,-139.45,-104.09,5.62];
holder_at_centerline = [82.25,-120.37,-100.53,-49.5,90.73,80.5];
pre_holder = [13.33,-101.38,-129.76,-38.56,90.18,-256.34];

%speeds
J_speed = 0.6;
L_speed = 0.11;

% pick and move paper
tackOn(a);
rtde_c.moveJ(deg2rad(paper_stack), J_speed+.2);
forceMove(50,0.11,rtde_r,rtde_c,1,0.06,0.01);

% temp1=rtde_r.getActualTCPPose();
% temp1_f=[temp1{1}, temp1{2}, temp1{3}+0.15, temp1{4}, temp1{5}, temp1{6}];
% rtde_c.moveL(temp1_f, L_speed)

temp1=rtde_r.getActualTCPPose();
temp1_f=[temp1{1}, temp1{2}+0.4, temp1{3}, temp1{4}, temp1{5}, temp1{6}];
rtde_c.moveL(temp1_f, L_speed+0.01);
rtde_c.moveJ(deg2rad(place_paper), J_speed-0.1);
tackOff(a);
pause(2);

% move to holder
temp1=rtde_r.getActualTCPPose();
temp1_f=[temp1{1}, temp1{2}-0.27782, temp1{3}+0.15, temp1{4}, temp1{5}, temp1{6}];
rtde_c.moveL(temp1_f, L_speed+.02);
openG(a);
rtde_c.moveJ(deg2rad(pre_holder), J_speed);
rtde_c.moveJ(deg2rad(holder_pos), J_speed);
closeG(a);
pause(1);
temp1=rtde_r.getActualTCPPose();
temp1_f=[temp1{1}, temp1{2}, temp1{3}+0.16759, temp1{4}, temp1{5}, temp1{6}];
rtde_c.moveL(temp1_f, L_speed);

% put tape
rtde_c.moveJ(deg2rad(tapestart), J_speed+0.1);

tape_translate = (297/2-(aero_center-40))/1000; %the number gives approximately start of holder
temp1=rtde_r.getActualTCPPose();
temp1_f=[temp1{1}+tape_translate, temp1{2}+0.003, temp1{3}, temp1{4}, temp1{5}, temp1{6}];
rtde_c.moveL(temp1_f, L_speed);
forceMoveTape(23,0.02,rtde_r,rtde_c,1,0.06,0.01,0.065);
rtde_c.moveJ(deg2rad(tapestart), J_speed);
temp1=rtde_r.getActualTCPPose();
temp1_f=[temp1{1}+tape_translate, temp1{2}-0.006, temp1{3}, temp1{4}, temp1{5}, temp1{6}];
rtde_c.moveL(temp1_f, L_speed);
forceMoveTape(23,0.05,rtde_r,rtde_c,1,0.06,0.01,0.065);

% temp1=rtde_r.getActualTCPPose();
% temp1_f=[temp1{1}, temp1{2}, temp1{3}+0.05, temp1{4}, temp1{5}, temp1{6}];
% rtde_c.moveL(temp1_f, L_speed);

% move arm out of laser area
temp1=rtde_r.getActualTCPPose();
temp1_f=[temp1{1}, temp1{2}-0.264, temp1{3}+0.05, temp1{4}, temp1{5}, temp1{6}];
rtde_c.moveL(temp1_f, L_speed);

% cut




%%
function openG(a)
writePWMDutyCycle(a,'D3',0.47);
end

function closeG(a)
writePWMDutyCycle(a,'D3',0.52);
end

function tackOn(a)
writePWMDutyCycle(a,'D11',0.35)
end

function tackOff(a)
writePWMDutyCycle(a,'D11',0.55)
end

function forceMove(threshold,up_dist,rtde_r,rtde_c,pause_time,up_speed,down_speed)
f= rtde_r.getActualTCPForce();
f0 =  cell2mat(f(3));
pause(0.5);

f= rtde_r.getActualTCPForce();
fi =  cell2mat(f(3));

% Force move down upto 0.1 m
temp_J=rtde_r.getActualTCPPose();
temp_Jm=[temp_J{1}, temp_J{2}, temp_J{3}-0.05, temp_J{4}, temp_J{5}, temp_J{6}];
rtde_c.moveL(temp_Jm, down_speed,0.3, 1);
    
    while(fi-f0)<threshold
%         disp('loop')
            f= rtde_r.getActualTCPForce();
            fi =  cell2mat(f(3));
%         disp(fi-f0)
    end
%     disp('stop')
    rtde_c.stopL(0.3);
    
    pause(pause_time);

temp_J=rtde_r.getActualTCPPose();
temp_Jm=[temp_J{1}, temp_J{2}, temp_J{3}+up_dist, temp_J{4}, temp_J{5}, temp_J{6}];
rtde_c.moveL(temp_Jm, up_speed,0.1, 0);

end


function forceMoveTape(threshold,up_dist,rtde_r,rtde_c,pause_time,up_speed,down_speed,tapeDist)
f= rtde_r.getActualTCPForce();
f0 =  cell2mat(f(3));
pause(0.5);

f= rtde_r.getActualTCPForce();
fi =  cell2mat(f(3));

% Force move down upto 0.1 m
temp_J=rtde_r.getActualTCPPose();
temp_Jm=[temp_J{1}, temp_J{2}, temp_J{3}-0.05, temp_J{4}, temp_J{5}, temp_J{6}];
rtde_c.moveL(temp_Jm, down_speed,0.3, 1);
    
    while(fi-f0)<threshold
%         disp('loop')
            f= rtde_r.getActualTCPForce();
            fi =  cell2mat(f(3));
%         disp(fi-f0)
    end
%     disp('stop')
    rtde_c.stopL(0.3);
    
    pause(pause_time);
    
    temp_J=rtde_r.getActualTCPPose();
temp_Jm=[temp_J{1}-tapeDist, temp_J{2}, temp_J{3}, temp_J{4}, temp_J{5}, temp_J{6}];
rtde_c.moveL(temp_Jm, up_speed,0.1, 0);

temp_J=rtde_r.getActualTCPPose();
temp_Jm=[temp_J{1}, temp_J{2}, temp_J{3}+up_dist, temp_J{4}, temp_J{5}, temp_J{6}];
rtde_c.moveL(temp_Jm, up_speed,0.1, 0);

end

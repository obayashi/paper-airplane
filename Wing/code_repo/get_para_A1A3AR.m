function [wing_pts, wing, aero] = get_para_A1A3AR(x,Shalf,paper,public,holder,num_ext)
A1 = x(1);
A3 = x(2);
AR = x(3);

A2 = Shalf - A1 - A3;
bhalf = (sqrt(AR.*(2.*Shalf)))/2;
ct = A2/bhalf;
A = A1/(0.5*bhalf);
B = A3/(0.5*bhalf);
cr = ct+A+B;
sw = rad2deg(pi/2 - atan(bhalf/A));
wing.c_root = cr;
wing.c_tip = ct;
wing.b_half = bhalf;
wing.sw_1 = sw;


wing_pts = get_points(wing,paper);
aero = get_mac(wing_pts,wing,paper);
if isequal(num_ext,99)
    % generic design for testing
    wing.name_ext = '_generic';
else
    wing.name_ext = strcat('_',num2str(num_ext));
end

make_visual(paper,wing_pts,public,aero,wing,holder);


end


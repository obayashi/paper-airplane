function [aero] = get_mac(wing_pts,wing,paper)
%get mean aerodynamic chord

%wing_pts are full wing as it would be cut on the paper

%move wing so that it lies at 0
paper_size = paper.size;
no_cut_margin = paper.margin;
taper = wing.c_tip/wing.c_root;

half_wing_x = wing_pts.x(1:4);
half_wing_y = wing_pts.y(1:4) - min(paper_size)/2;

half_chord_x = [(wing.c_root/2+no_cut_margin) (wing.c_tip/2+no_cut_margin+half_wing_x(2))];
half_chord_y = [0 wing.b_half];
diagonal_x = [(wing.c_root+wing.c_tip+no_cut_margin) (half_wing_x(2)-wing.c_root)];
diagonal_y = [0 wing.b_half];
[temp_x, mac_spanloc] = polyxpoly(half_chord_x,half_chord_y,diagonal_x,diagonal_y);
mac_ext_x = [0 max(paper_size)];
mac_ext_y = [mac_spanloc mac_spanloc];
[mac_x, temp_y] = polyxpoly(mac_ext_x,mac_ext_y,half_wing_x,half_wing_y);
mac = mac_x(2)-mac_x(1);
mac2 = wing.c_root*(2/3)*((1+taper+(taper^2))/(1+taper));
mac_25 = mac_x(1) + mac*0.25;

aero.mac = mac;
aero.ac_x = mac_25;
aero.ac_y = min(paper_size)/2;

% temp for checking
% paper_x = [0 0 max(paper_size) max(paper_size)];
% paper_y = [0 min(paper_size)/2 min(paper_size)/2 0];
% margin_x = [no_cut_margin no_cut_margin max(paper_size)-no_cut_margin max(paper_size)-no_cut_margin];
% margin_y = [0 min(paper_size)/2-no_cut_margin min(paper_size)/2-no_cut_margin 0];
% figure 
% plot(half_wing_x,half_wing_y,paper_x,paper_y,margin_x,margin_y)
% hold on 
% plot(half_chord_x,half_chord_y,diagonal_x,diagonal_y,mac_ext_x,mac_ext_y)
% plot(mac_x,temp_y,'r*')
% plot(mac_25,temp_y,'r*')
% plot(mac_25,0,'r*')
% axis equal


end


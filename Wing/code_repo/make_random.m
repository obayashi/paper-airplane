function [wing_pts, wing, aero] = make_random(paper,num_ext,public,holder,area)

paper_size = paper.size;
no_cut_margin = paper.margin;

area_fixed = area; %mm2 this is half only

count = 1;

while count <= 1
    
    % random parameters
    c_root = (max(paper_size)-0).*rand(1,1) + 0;
    b_half = (min(paper_size)/2-0).*rand(1,1) + 0;
    sw_1 = (90-0).*rand(1,1) + 0;
    
    wing.c_root = c_root;
    wing.b_half = b_half;
    wing.sw_1 = sw_1;

    wing.c_tip = (2/wing.b_half)*area_fixed - wing.c_root;

    wing_pts = get_points(wing,paper);
    
    if any(wing_pts.x < 0+no_cut_margin) || any(wing_pts.x > max(paper_size)-no_cut_margin) || any(wing_pts.y<0+no_cut_margin) || any(wing_pts.y>min(paper_size)-no_cut_margin)
        note_str = 99; % means OOB
    elseif wing.c_root <= wing.c_tip 
        note_str = 98; % tip must be smaller or equal to root chord
    elseif wing.c_tip < 0
        note_str = 97; % solved tip chord is negative
    else
        note_str = 1; % means FIG exists
        
        count = count + 1;            
              
    end

end

aero = get_mac(wing_pts,wing,paper);
wing.name_ext = strcat('_',num2str(num_ext));
make_visual(paper,wing_pts,public,aero,wing,holder);

end
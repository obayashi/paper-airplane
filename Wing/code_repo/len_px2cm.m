function len_cm = len_px2cm(len_px,max_cm,min_cm,max_px,min_px)
    m = (max_cm-min_cm)/(max_px-min_px);
    b = min_cm-(m*min_px);

    len_cm = (m.*len_px)+b;
end

function make_output_data(public,count)

% flight distance
% variation from center line?

current = pwd;
folder = strcat(current,'/out/data');
filename = strcat(folder,'/',public.name,'_output.txt');

% save data with all design para and aero (if applicable)
if isequal(count,1) % to make the first line
    if not(isfolder(folder))
        mkdir(folder);
    end
    fid = fopen(filename,'w');
end

prompt = 'Distance flown? [cm]';
out_dist = input(prompt);
prompt = 'Distance from center? [cm]';
out_side = input(prompt);

output_vec = [out_dist out_side];


fid = fopen(filename,'a');
fprintf(fid, '%f ',output_vec);
fprintf(fid, '\n');

fclose(fid);


end
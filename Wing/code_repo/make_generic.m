function [wing_pts, wing, aero] = make_generic(paper,public,holder,defined_para,num_ext)
    
% generic wing
wing.c_root = defined_para(1);
wing.c_tip = defined_para(2);
wing.b_half = defined_para(3);
wing.sw_1 = defined_para(4);

wing_pts = get_points(wing,paper);
aero = get_mac(wing_pts,wing,paper);
if isequal(num_ext,99)
    % generic design for testing
    wing.name_ext = '_generic';
else
    wing.name_ext = strcat('_',num2str(num_ext));
end

make_visual(paper,wing_pts,public,aero,wing,holder);

end
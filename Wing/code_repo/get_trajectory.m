function [max_dist, vel_avg, glide_ratio] = get_trajectory(vid_name,num_ext,public)

path = 'C:\Users\balto\Videos\Logitech\LogiCapture\';

fps = 60;
px_width = 1280;
px_height = 720;
video_left = 33; %cm
video_right = 424; %cm
video_top = 164; %cm

% vid_name = 'r1d1.mp4';
vidObj =VideoReader(strcat(path,vid_name),'CurrentTime',1);
l= vidObj.NumFrames;
frames = read(vidObj,[20 l]);
cropx=[1 720]; 
cropy=[1 1280];
f_start=10;
f_end=l-20;

% For the frames of interest find the difference, convert to greyscale crop
% and store
 for i = f_start:f_end
     t1=frames(:,:,:,i);
     t2=frames(:,:,:,i+1);
     K = imabsdiff(t1,t2);   
     BW(:,:,:,i) = rgb2gray(K);
     B=BW(:,:,:,i); 
     temp = imbinarize(B, 0.15);
     Bin(:,:,i) = temp(cropx(1):cropx(2), cropy(1):cropy(2));
 end

Binf = (zeros(range(cropx)+1,range(cropy)+1));   % Crop the image to just the flight region
x=[0];
y=[0];

for j = f_start:f_end
    largestRegion = bwareafilt(Bin(:,:,j), 1);          % Find the largest white area which is the plane
    [rows, columns] = find(largestRegion);
    size = sum(largestRegion, 'all');
    if(size > 10)                       % If find single pixels, ignore and use previous location - avoids detection of noise when no plane in the frame
        xc = mean(rows);
        yc = mean(columns);
        x=[x xc];
        y=[y yc];
    else
        x=[x x(length(x))];
        y=[y y(length(y))];        
    end 
    Binf = Binf + Bin(:,:,j);           % This is just to sum all the pixels so can graphically see
end

dist_px = y;
height_px = x;

dist_cm = len_px2cm(dist_px,video_right,video_left,px_width,0);
height_cm = len_px2cm(height_px,video_top,0,0,max(height_px));

% cut off initial part where height in px is 150
idx_need = find(height_cm<video_top);
height_cm = height_cm(idx_need);
dist_cm = dist_cm(idx_need);

max_dist = max(dist_cm);

% find first time it hits ground
ground_idx = find(height_cm<=0);
frame_ground = ground_idx(1);
dist_ground = dist_cm(ground_idx(1));
start_idx = find(height_cm<video_top);
height_start = height_cm(start_idx(1));
frame_start = start_idx(1);
dist_start = dist_cm(start_idx(1));

time_flight = (frame_ground-frame_start)/fps;
dist_flight = dist_ground-dist_start; % cm
vel_avg = dist_flight/time_flight; %cm/s
glide_ratio = dist_flight/height_start;

trajectory.height = height_cm;
trajectory.dist = dist_cm;

current = pwd;
folder = strcat(current,'/out/trajectory');
filename = strcat(folder,'/',public.name,'_',num2str(num_ext),'.mat');

% save data with all design para and aero (if applicable)
if not(isfolder(folder))
    mkdir(folder);
end

save(filename,'trajectory');

end


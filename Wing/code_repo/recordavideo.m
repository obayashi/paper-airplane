function recordavideo(public,num_ext)
%% Construct a video input object
vobj = videoinput('winvideo', 2);

%% Set Properties for Videoinput Object
vobj.TimeOut = Inf;
vobj.FrameGrabInterval = 1;
vobj.LoggingMode = 'disk&memory';
vobj.FramesPerTrigger = 1;
vobj.TriggerRepeat = Inf;

%% make folder and name
current = pwd;
folder = strcat(current,'/out/video');
filename = strcat(folder,'/',public.name,'_',num2str(num_ext),'.avi');

if not(isfolder(folder))
    mkdir(folder);
end

%% Construct VideoWriter object and set Disk Logger Property
timenow = datestr(now,'hhMMss_ddmmyy');
experimentid = 1;
v = VideoWriter(filename);
% v = VideoWriter(['plane_', timenow,'.avi']);
v.Quality = 50;
v.FrameRate = 60;
vobj.DiskLogger = v;

% Select the source to use for acquisition.
vobj.SelectedSourceName = 'input1';

% Create an image object for previewing.
vidRes = vobj.VideoResolution;
nBands = vobj.NumberOfBands;
hImage = image( zeros(vidRes(2), vidRes(1), nBands) );
preview(vobj, hImage);
imaqmex('feature','-limitPhysicalMemoryUsage',false);

disp('here 1')
% pause(5)

% Records 1 second video
tic
disp('here 2')
start(vobj)
disp('here 3')
pause(3);
disp('here 4')
stop(vobj)
disp('here 5')

% Compute actual Frame Rate
elapsedTime = toc;
disp('here 6')
framesaq = vobj.FramesAcquired;
disp('here 7')
ActualFR = framesaq/elapsedTime;
disp('here 8')

% Delete Object
delete(vobj);
clear vobj;
disp('here 9')

%% Change the Frame Rate to Orinial Value and save it to new video file
% with the same timestamp
%ChangeFrameRate(['plane_', timenow,'.avi'], timenow, ActualFR)

end

%% This function changes the Frame Rate that the Saved Video is as long as the recording
function ChangeFrameRate(Video, timestr, ActualFR)

% Construct Video Objects
vidObj = VideoReader(Video);
writerObj = VideoWriter(['ActualFR_', timestr, '.avi']);
writerObj.FrameRate = ActualFR;

% Open Video Writer Object
open(writerObj);

% Read video frames until the end of the file is reached
while hasFrame(vidObj)
    vidFrame = readFrame(vidObj);
    writeVideo(writerObj, vidFrame)
    pause(1/vidObj.FrameRate);
end

% Close Video Writer Object
close(writerObj);
end
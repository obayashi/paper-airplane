function fig = make_visual(paper,wing_pts,public,aero,wing,holder)
% outputs visual of wing, datapoints of wing, and holder location

paper_size = paper.size;
no_cut_margin = paper.margin;

paper_x = [0 0 max(paper_size) max(paper_size) 0];
paper_y = [0 min(paper_size) min(paper_size) 0 0];

margin_x = [no_cut_margin no_cut_margin max(paper_size)-no_cut_margin max(paper_size)-no_cut_margin no_cut_margin];
margin_y = [no_cut_margin min(paper_size)-no_cut_margin min(paper_size)-no_cut_margin no_cut_margin no_cut_margin];

fig = figure;
% subplot(1,2,1)
plot(paper_x,paper_y,'k','LineWidth',1.5)
hold on 
plot(margin_x,margin_y,'k--')
plot(wing_pts.x,wing_pts.y)
plot(aero.ac_x,aero.ac_y,'r*')
legend('Paper','Margin','Wing','AC','Location','BEST')
xlabel('x [mm]')
ylabel('y [mm]')
axis equal
grid on

% save figure and points for wing
current = pwd;
folder = strcat(current,'/out/wing');
if not(isfolder(folder))
    mkdir(folder)
end

saveas(fig,strcat(folder,'/',public.name,wing.name_ext,'.png'))
close(fig)

% wing.x wing.y
writematrix([round(wing_pts.x,1)',round(wing_pts.y,1)'],...
    strcat(folder,'/',public.name,wing.name_ext,'.txt'),'Delimiter',',')

% save holder location
% also move holder point so that robot can always grasp the midpoint
holder_cm_loc = holder.length*holder.cm;
holder_mid_loc = holder.length*0.5;
holder_translate = holder_mid_loc - holder_cm_loc;
new_holder_x = aero.ac_x + holder_translate;

folder2 = strcat(current,'/out/holder');
if not(isfolder(folder2))
    mkdir(folder2)
end

% % holder.x, holder.y (center of specific holder)
% writematrix([new_holder_x,aero.ac_y],...
%     strcat(folder2,'/',public.name,wing.name_ext,'.txt'),'Delimiter',',')

% % holder.x, holder.y (aero center)
% writematrix([aero.ac_x,aero.ac_y],...
%     strcat(folder2,'/',public.name,wing.name_ext,'_ac.txt'),'Delimiter',',')

% holder.x, holder.y (aero center 10% forward)
writematrix([aero.ac_x-(aero.ac_x/10),aero.ac_y],...
    strcat(folder2,'/',public.name,wing.name_ext,'_ac_10fwd.txt'),'Delimiter',',')

% holder.x, holder.y (aero center 20% forward)
writematrix([aero.ac_x-2*(aero.ac_x/10),aero.ac_y],...
    strcat(folder2,'/',public.name,wing.name_ext,'_ac_20fwd.txt'),'Delimiter',',')




end
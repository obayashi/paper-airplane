function aero = find_aero(public,wing,aero)

% target strings
cl_str = 'CLtot[ ]+=';
cd_str = 'CDff[ ]+=';
cd_trunc = '|[ ]+Trefftz';


%  slurp the entire file into a cell array
filename = strcat(pwd,'\out\avl\',public.name,wing.name_ext);
fid = fopen(strcat(filename,'.ft'), 'r');
tline = fgetl(fid);
while ischar(tline)
%     disp(tline)
    cl_start = regexp(tline,cl_str);
    cd_start = regexp(tline,cd_str);
%     cd_end = regexp(tline,cd_trunc);
    if not(isempty(cl_start))
        temp_line = tline(cl_start+5:end);
        temp_idx = regexp(temp_line,'=');
        format long
        cl = str2num(temp_line(temp_idx+1:end));
    end
    if not(isempty(cd_start))
        temp_line = tline(cd_start+4:end);
        temp_idx = regexp(temp_line,'=');
        end_idx = regexp(temp_line,cd_trunc);
        format long
        cd = str2num(temp_line(temp_idx+1:end_idx-2));
    end
    tline = fgetl(fid);
    
end
fclose(fid);
aero.cl = cl;
aero.cd = cd;
end


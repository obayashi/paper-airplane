function make_mb_data(public,count,wing,aero_analysis,aero)

% design para
% aero

current = pwd;
folder = strcat(current,'/out/data');
filename = strcat(folder,'/',public.name,'_data.txt');

% save data with all design para and aero (if applicable)
if isequal(count,1) % to make the first line
    if not(isfolder(folder))
        mkdir(folder);
    end
    fid = fopen(filename,'w');
end

output_vec = [wing.c_root,wing.c_tip,wing.b_half,wing.sw_1];

if aero_analysis
    output_vec = [output_vec, aero.cl, aero.cd, (aero.cl/aero.cd), aero.mac, aero.ac_x];
end

fid = fopen(filename,'a');
fprintf(fid, '%f ',output_vec);
fprintf(fid, '\n');

fclose(fid);


end
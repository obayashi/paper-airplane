function cut_wing(gcode_cell,laser)

clear s
s = serialport(string(strcat('COM',round(num2str(laser.COM)))),115200,"Timeout",80);

configureTerminator(s,"CR")
quote = "";
for i = 1:length(gcode_cell)
    tosend = strcat(quote, gcode_cell{i}, quote);
    writeline(s,tosend);
    idn = readline(s);
end
end



function [wing_pts, wing, aero, outvec] = make_random2(paper,num_ext,public,holder,harea)

paper_size = paper.size;
no_cut_margin = paper.margin;

%hardcode not good 
ac_fwd = 30; % percentage
holder_front_offset = 32; %mm

count = 1;

while count <= 1
    
    % random parameters
    c_root = (max(paper_size)-0).*rand(1,1) + 0;
    b_half = (min(paper_size)/2-0).*rand(1,1) + 0;
    sweep = (90-0).*rand(1,1) + 0;
    
    wing.c_root = c_root;
    wing.b_half = b_half;
    wing.sw_1 = sweep;
    
    wing.c_tip = (2/wing.b_half)*harea - wing.c_root;
    
    wing_pts = get_points(wing,paper);
    
    if any(wing_pts.x < 0+no_cut_margin) || any(wing_pts.x > max(paper_size)-no_cut_margin) || any(wing_pts.y<0+no_cut_margin) || any(wing_pts.y>min(paper_size)-no_cut_margin)
        note_str = 99; % means OOB
    elseif wing.c_root <= wing.c_tip
        note_str = 98; % tip must be smaller or equal to root chord
    elseif wing.c_tip < 0
        note_str = 97; % solved tip chord is negative
    else
        note_str = 1; % means FIG exists
        count = count + 1;
    end
    
    
end

% get more parameters
x1 = wing_pts.x(3);
y1 = wing_pts.y(3);
x2 = x1 + 30;
y2 = y1;
x3 = wing_pts.x(4);
y3 = wing_pts.y(4);
v_1 = [x2,y2,0] - [x1,y1,0];
v_2 = [x3,y3,0] - [x1,y1,0];
wing.bsweep = rad2deg(atan2(norm(cross(v_1, v_2)), dot(v_1, v_2)));

wing.width = wing.b_half*2;
wing.length = max(wing_pts.x)-min(wing_pts.x);
wing.wol = wing.width/wing.length;

aero = get_mac(wing_pts,wing,paper);
ac_x_fwd = aero.ac_x-((ac_fwd/10)*(aero.ac_x/10));
holder_front_x = ac_x_fwd-holder_front_offset;
holder_back_x = holder_front_x+holder.length;

outvec =  [wing_pts.x(1:4) wing_pts.y(1:4) wing_pts.area...
    wing.c_root wing.c_tip wing.b_half wing.sw_1 wing.bsweep...
    wing.width wing.length wing.wol aero.mac...
    aero.ac_x holder_front_x holder_back_x];


wing.name_ext = strcat('_',num2str(num_ext));
make_visual(paper,wing_pts,public,aero,wing,holder);

end
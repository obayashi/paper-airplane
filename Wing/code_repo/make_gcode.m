function gcode_cell = make_gcode(wing_pts,laser,public,wing)

wing_x = round(wing_pts.x,1);
wing_y = round(wing_pts.y,1)+35; % this offset is because of how laser is positioned at home in relation to paper

gcode_cell = {};
gcode_cell{end+1} = "G90";
gcode_cell{end+1} = "M3 S0";
% gcode_cell{end+1} = "S0";
gcode_cell{end+1} = "G0X0Y0Z0";
gcode_cell{end+1} = string(['G0X' num2str(wing_x(1)) 'Y' num2str(wing_y(1))]);
gcode_cell{end+1} = string(['S' num2str(laser.intensity)]);
for i = 1:laser.passes
    for j = 1:length(wing_x)
        gcode_cell{end+1} = string(['G1X' num2str(wing_x(j)) 'Y' num2str(wing_y(j)) 'F' num2str(laser.rate)]);
    end
end
% gcode_cell{end+1} = string(['G1X' num2str(wing_x(2)) 'Y' num2str(wing_y(2))  'F' num2str(laser.rate)]);
gcode_cell{end+1} = "S100";
gcode_cell{end+1} = "G1X0Y0F900";
gcode_cell{end+1} = "S0 M5";
gcode_cell{end+1} = "G0X0Y0Z0";


% save gcode
current = pwd;
folder = strcat(current,'/out/gcode');
if not(isfolder(folder))
    mkdir(folder)
end

fileID = fopen(strcat(folder,'/',public.name,wing.name_ext,'.gcode'),'w');
fprintf(fileID, '%s\n', gcode_cell{:});


end


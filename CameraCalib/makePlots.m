clc, clear, close all;

% far glide 4 6 35 38
% recovery 3 17 29 41 33

designs = [4 6 35 38 3 17 29 33 41];
wing_far = [4 6 35 38];
wing_recover = [3 17 29 33 41];
clusters = {'far','recovery'};
% note: these classifications are from previous BO results. they may not be
% exactly correct

% for plotting
blue = '#0072BD'; orange = '#D95319'; yellow = '#EDB120'; purple = 	'#7E2F8E';
green = '#77AC30'; ltblue = '#4DBEEE'; red = '#A2142F'; gray = '#808080';
color_pallette = {blue, orange, yellow, green, ltblue, red, purple,gray};
LW = 2;
MS = 5;

figure
for k = 1:length(clusters)
    
    which_cluster = clusters{k};
    if isequal(which_cluster,'far')
        wing_nums = wing_far;
    elseif isequal(which_cluster,'recovery')
        wing_nums = wing_recover;
    end
    
    avg_dist = [];
    var_dist = []; % variance normalized by N. why is sometimes N-1 used???
    avg_dev = []; % deviation from centerline
    var_dev = [];
    % first try for one cluster
    for i = 1:length(wing_nums) % for each design
        mat_name = ['flinfo' num2str(wing_nums(i)) '.mat'];
        flight_info = load(mat_name).flinfo;
        each_dist = [];
        each_dev = [];
        for j = 1:length(flight_info) % for each of the runs
            each_run = flight_info{j};
            each_dist(j) = each_run.landing_spot(2);
            each_dev(j) = abs(each_run.landing_spot(1)); % abs because we want to see deviation from centerline for now, not the left and right
        end
        avg_dist(i) = mean(each_dist);
        var_dist(i) = var(each_dist,1);
        avg_dev(i) = mean(each_dev);
        var_dev(i) = var(each_dev,1);
    end
    
    subplot(1,2,1)
    hold on 
    plot(avg_dist,zeros(1,length(avg_dist)),'LineWidth',LW,'Color',color_pallette{k})
    plot([min(avg_dist) max(avg_dist)],[0 0],'o','Color',color_pallette{k},'MarkerFaceColor',color_pallette{k})
    [sorted idx] = sort(avg_dist);
    plot(avg_dist(idx),var_dist(idx),'LineWidth',LW-1,'Color',color_pallette{k})
    
    subplot(1,2,2)
    hold on
    plot(avg_dev,zeros(1,length(avg_dev)),'LineWidth',LW,'Color',color_pallette{k})
    plot([min(avg_dev) max(avg_dev)],[0 0],'o','Color',color_pallette{k},'MarkerFaceColor',color_pallette{k})
    [sorted idx2] = sort(avg_dev);
    plot(avg_dev(idx2),var_dev(idx2),'LineWidth',LW-1,'Color',color_pallette{k})
    
end


xlabel('distance [m]')
ylabel('\sigma^2')
hold off

% figure for trajectory
figure
for k = 1:length(clusters)
    
    which_cluster = clusters{k};
    if isequal(which_cluster,'far')
        wing_nums = wing_far;
    elseif isequal(which_cluster,'recovery')
        wing_nums = wing_recover;
    end
    

    % first try for one cluster
    for i = 1:length(wing_nums) % for each design
        mat_name = ['flinfo' num2str(wing_nums(i)) '.mat'];
        flight_info = load(mat_name).flinfo;
%         each_dist = [];
%         each_dev = [];
        for j = 1:length(flight_info) % for each of the runs
            each_run = flight_info{j};
            plot(each_run.traj_dist,each_run.traj_height,'Color',color_pallette{i})
            hold on
%             each_dist(j) = each_run.landing_spot(2);
%             each_dev(j) = abs(each_run.landing_spot(1)); % abs because we want to see deviation from centerline for now, not the left and right
        end

    end
    
    
    
    
end


xlabel('distance [m]')
ylabel('\sigma^2')



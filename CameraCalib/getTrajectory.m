clc,clear,close all;

path = 'C:\Users\balto\Videos\0402_variation\0402_variation\';

% far glide 4 6 35 38
% recovery 3 17 29 41 33
% 
designs = [4 6 35 38 3 17 29 33 41];


tform = load('tform_floor.mat').tform;

flight_info = []; % output mat

fps = 30;
min_dist_m = 0.1; %10 cm kurai?
launcher_height_m = 1.4;
gnd_height_m = 0;
min_pixel = 1;


for k = 1:length(designs)
    files = dir([path num2str(designs(k)) '\*.mp4']);
    flinfo = {};
    for l = 1:numel(files)
        vid_name = files(l).name;

        vidObj =VideoReader(strcat(path,num2str(designs(k)),'\',vid_name),'CurrentTime',0);
        num_frames= vidObj.NumFrames;
        frames = read(vidObj,[1 Inf]);
        frame_width = size(frames,2);
        frame_height = size(frames,1);

        cropx=[1 frame_height];
        cropy=[1 frame_width/2]; % left
        cropyR=[frame_width/2+1 frame_width]; %right frame
        f_start=1;
        f_end=num_frames-15;

        % For the frames of interest find the difference, convert to greyscale crop
        % and store
        for i = f_start:f_end
            t1=frames(:,:,:,i);
            t2=frames(:,:,:,i+1);
            K = imabsdiff(t1,t2);
            BW(:,:,:,i) = rgb2gray(K);
            B=BW(:,:,:,i);
            temp = imbinarize(B, 0.15);
            Bin(:,:,i) = temp(cropx(1):cropx(2), cropy(1):cropy(2));
            BinR(:,:,i) = temp(cropx(1):cropx(2), cropyR(1):cropyR(2));
        end

        Binf = (zeros(range(cropx)+1,range(cropy)+1));   % Crop the image to just the flight region
        x=[0];
        y=[0];
        BinfR = (zeros(range(cropx)+1,range(cropyR)+1));   % Crop the image to just the flight region
        xR=[0];
        yR=[0];


        for j = f_start:f_end
            largestRegion = bwareafilt(Bin(:,:,j), 1);          % Find the largest white area which is the plane
            largestRegionR = bwareafilt(BinR(:,:,j), 1); 
            [rows, columns] = find(largestRegion);
            [rowsR, colsR] = find(largestRegionR);
            sizeL = sum(largestRegion, 'all');
            sizeR = sum(largestRegionR, 'all');
            if(sizeL > 50)                           % If find single pixels, ignore and use previous location - avoids detection of noise when no plane in the frame

                xc = mean(rows);
                yc = mean(columns);
                x=[x xc];
                y=[y yc];
            else
                x=[x x(length(x))];
                y=[y y(length(y))];
            end
            if(sizeR > 10)                           % If find single pixels, ignore and use previous location - avoids detection of noise when no plane in the frame

                xcR = mean(rowsR);
                ycR = mean(colsR);
                xR=[xR xcR];
                yR=[yR ycR];
            else
                xR=[xR xR(length(xR))];
                yR=[yR yR(length(yR))];
            end

            Binf = Binf + Bin(:,:,j);           % This is just to sum all the pixels so can graphically see
            BinfR = BinfR + BinR(:,:,j);    
        end

        img_x = y;
        img_y = x;

        nonzero_idx = find(img_x~=0); % get rid of initial zeros
        landed_idx = find(img_x == img_x(end)); % get rid of same pixels after landing
        img_x = img_x(nonzero_idx(1):landed_idx(1));
        img_y = img_y(nonzero_idx(1):landed_idx(1));

        % final landing spot
        [worldPoints_X, worldPoints_Y] = transformPointsForward(tform, img_x(end),img_y(end));
        flight_info.landing_spot = [worldPoints_X, worldPoints_Y];


        % trajectory from the side
        img_xR = yR;
        img_yR = xR;

        nonzero_idx = find(img_xR~=0); % get rid of initial zeros
        landed_idx = find(img_xR == img_xR(end)); % get rid of same pixels after landing
        img_xR = img_xR(nonzero_idx(1):landed_idx(1));
        img_yR = img_yR(nonzero_idx(1):landed_idx(1));

        dist_m = len_px2m(img_xR,worldPoints_Y,min_dist_m,img_xR(end),min_pixel);
        height_m = len_px2m(img_yR,launcher_height_m,gnd_height_m,img_yR(1),img_yR(end));
        flight_info.traj_dist = dist_m;
        flight_info.traj_height = height_m;
        
        flinfo{l} = flight_info;

    end
    out_fn = ['flinfo' num2str(designs(k)) '.mat'];
    save(out_fn,'flinfo');
end



%% function repo

function len_m = len_px2m(len_px,max_m,min_m,max_px,min_px)
    m = (max_m-min_m)/(max_px-min_px);
    b = min_m-(m*min_px);

    len_m = (m.*len_px)+b;
end
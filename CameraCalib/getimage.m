clc, clear, close all;

%% 1) change here
path = 'C:\Users\balto\Videos\0418_50samples\0420_redo\';
vid_name = 'reddotsafter.mp4';

vidObj =VideoReader(strcat(path,vid_name),'CurrentTime',0);
l= vidObj.NumFrames;
frames = read(vidObj,[1 Inf]);
frame_width = size(frames,2);
frame_height = size(frames,1);
t1 = frames(:,:,:,1);

% work on top view camera
I1 = imcrop(t1,[0 0 frame_width/2 frame_height]); % left of screen
figure
imshow(I1)
imwrite(I1,['red_floor.jpg'])

%% open color thresholder app and save createMask function

%% work with createMask function and check if okay
num_dots = 10; % may need to change
[BW,maskedRGBImage] = createMask_floor2(I1);
largestRegion = bwareafilt(BW, num_dots);       
figure
imshow(largestRegion)
hold on
s = regionprops(largestRegion,'centroid'); %Calculate centroids for connected components in the image using regionprops. The regionprops function returns the centroids in a structure array.
centroids_top = cat(1,s.Centroid); %Store the x- and y-coordinates of the centroids into a two-column matrix.
plot(centroids_top(:,1),centroids_top(:,2),'r*')
hold off

%% save pixel points of red dots
% centroids_top([1,3],:) = []; % if there are ones i want to exclude
campts.x = centroids_top(:,1);
campts.y = centroids_top(:,2);
save('campts.mat','campts');

%% check that campts and wldpts are in the same order and run transform.m

%% side view if needed

% work on side view camera
crop_vec = [frame_width/2+1 0 frame_width/2 frame_height];
I2 = imcrop(t1,crop_vec); % right of screen
figure
imshow(I2)
%crop further
left = 200;
right = 800;
top = 200;
bottom = 700;
crop_new = crop_vec + [left top right*-1 bottom*-1];
I3 = imcrop(t1,crop_new); % right of screen
figure
imshow(I3)
% imwrite(I3,['red_side.jpg'])
[BW,maskedRGBImage] = createMask_side(I3);
largestRegion = bwareafilt(BW, 10);          % there are 10 red dots in image
figure
imshow(largestRegion)
figure 
imshow(I2)
hold on
s = regionprops(largestRegion,'centroid'); %Calculate centroids for connected components in the image using regionprops. The regionprops function returns the centroids in a structure array.
centroids_side = cat(1,s.Centroid); %Store the x- and y-coordinates of the centroids into a two-column matrix.
plot(centroids_side(:,1)+left,centroids_side(:,2)+top,'b*')
hold off

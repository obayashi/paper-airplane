clc, clear, close all;
path = 'C:\Users\balto\Videos\drive-download-20220401T191454Z-001\';

for k = 1:37
vid_name = [num2str(k) '.mp4'];

frame_width = 3840;
frame_height = 1080;

vidObj =VideoReader(strcat(path,vid_name),'CurrentTime',0);
l= vidObj.NumFrames;
frames = read(vidObj,[1 Inf]);

i = 1;
t1 = frames(:,:,:,i);
imshow(t1)
I1 = imcrop(t1,[1 1 frame_width/2-1 frame_height]); % left of screen
imshow(I1)
imwrite(I1,['other/other' num2str(k) '.jpg'])


I2 = imcrop(t1,[frame_width/2+1 1 frame_width frame_height]); %right of screen
imshow(I2)
imwrite(I2,['brio/brio' num2str(k) '.jpg'])
end
clc, clear, close all

%227
camLx = load('brio_x.mat').x;
camLy = load('brio_y.mat').y;
camRx = load('other_x.mat').x;
camRy = load('other_y.mat').y;
stereoParams = load('stereoParams3.mat').stereoParams;

idx_st = 227;
idx_ed = length(camLx);

matchedPoints1 = [camLx(idx_st:idx_ed)' camLy(idx_st:idx_ed)'];
matchedPoints2 = [camRx(idx_st:idx_ed)' camRy(idx_st:idx_ed)'];

worldPoints = triangulate(matchedPoints1,matchedPoints2,stereoParams);

figure
subplot(1,2,1)
plot(camLy(idx_st:idx_ed)', camLx(idx_st:idx_ed).*-1')
title('left/brio')
subplot(1,2,2)
plot(camRy(idx_st:idx_ed)', camRx(idx_st:idx_ed).*-1')
title('right/other')



figure
plot3(worldPoints(:,1),worldPoints(:,2),worldPoints(:,3),'o-')
xlabel('x')
ylabel('y')
zlabel('z')
grid on
axis equal

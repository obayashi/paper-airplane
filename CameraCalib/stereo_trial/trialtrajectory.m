clc,clear,close all
path = 'C:\Users\balto\switchdrive\PaperPlane\CameraCalib\stereo_trial\';
% public.name = 'BO_0226';

%18-9
vid_name = 'flight2.mp4';

fps = 60;
px_width = 1280;
px_height = 720;
video_left = 33; %cm 39
video_right = 424; %cm 431
video_top = 164; %cm

max_dist_vec = zeros(60,1);


% for num_ext = 1:60
%     vid_name = [num2str(num_ext) '.mp4'];
    

    vidObj =VideoReader(strcat(path,vid_name),'CurrentTime',0);
    l= vidObj.NumFrames;
    frames = read(vidObj,[20 l]);
    cropx=[1 1080]; 
%     cropy=[1 1920]; % left/other
    cropy=[1921 3840]; %right/brio
    f_start=10;
    f_end=l-20;

    % For the frames of interest find the difference, convert to greyscale crop
    % and store
     for i = f_start:f_end
         t1=frames(:,:,:,i);
         t2=frames(:,:,:,i+1);
         K = imabsdiff(t1,t2);   
         BW(:,:,:,i) = rgb2gray(K);
         B=BW(:,:,:,i); 
         temp = imbinarize(B, 0.15);
         Bin(:,:,i) = temp(cropx(1):cropx(2), cropy(1):cropy(2));
     end

    Binf = (zeros(range(cropx)+1,range(cropy)+1));   % Crop the image to just the flight region
    x=[0];
    y=[0];

    for j = f_start:f_end
        largestRegion = bwareafilt(Bin(:,:,j), 1);          % Find the largest white area which is the plane
        [rows, columns] = find(largestRegion);
        size = sum(largestRegion, 'all');
        if(size > 80)                           % If find single pixels, ignore and use previous location - avoids detection of noise when no plane in the frame
            
            xc = mean(rows);
            yc = mean(columns);
            x=[x xc];
            y=[y yc];
        else
            x=[x x(length(x))];
            y=[y y(length(y))];        
        end 
        Binf = Binf + Bin(:,:,j);           % This is just to sum all the pixels so can graphically see
%        imshow(Binf) 
       
    end
    
    figure
    plot(y,x)
%     figure
%     plot(y(165:242),x(165:242).*-1)

    dist_px = y;
    height_px = x;

    dist_cm = len_px2cm(dist_px,video_right,video_left,px_width,0);
    height_cm = len_px2cm(height_px,video_top,0,0,max(height_px));

    % cut off initial part where height in px is 164cm
    idx_need = find(height_cm<video_top);
    height_cm = height_cm(idx_need);
    dist_cm = dist_cm(idx_need);
    
    % cut off last 5 cm
    idx_5cm = find(height_cm<=4);
    idx_5cm = idx_5cm(1);%first time its less than 5cm
    height_cm = height_cm(1:idx_5cm);
    dist_cm = dist_cm(1:idx_5cm);

    max_dist = max(dist_cm);
    max_dist_vec(num_ext) = max_dist;

    % find first time it hits ground
%     ground_idx = find(height_cm<=0);
    frame_ground = idx_5cm;
    dist_ground = dist_cm(frame_ground);
    start_idx = find(height_cm<video_top);
    height_start = height_cm(start_idx(1));
    frame_start = start_idx(1);
    dist_start = dist_cm(start_idx(1));

    time_flight = (frame_ground-frame_start)/fps;
    dist_flight = dist_ground-dist_start; % cm
    vel_avg = dist_flight/time_flight; %cm/s
    glide_ratio = dist_flight/height_start;

    trajectory.height = height_cm;
    trajectory.dist = dist_cm;

    current = pwd;
    folder = strcat(current,'/out/trajectory');
    filename = strcat(folder,'/',public.name,'_',num2str(num_ext),'.mat');

    % save data with all design para and aero (if applicable)
    if not(isfolder(folder))
        mkdir(folder);
    end

    save(filename,'trajectory');
    disp('vid')
    disp(num_ext)
% end

max_dist_vec;
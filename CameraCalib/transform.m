clc, clear, close all;

imgpts = load('campts.mat').campts;
wldpts = load('wldpts.mat').wldpts;

movingPoints = [imgpts.x imgpts.y];
fixedPoints = [wldpts.x wldpts.y];
tform = fitgeotrans(movingPoints,fixedPoints,'projective');
save('tform_floor.mat','tform');

%test -- seems correct
[worldPoints_X, worldPoints_Y] = transformPointsForward(tform, 960,540);



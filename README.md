# Paper Airplane

The files in this repository accompany the paper Robotic Automation and Unsupervised Cluster Assisted Modeling for Solving the Forward and Reverse Design Problem of Paper Airplanes. 

The code was written using MATLAB 2021a. 

This README highlights key codes/data used for this work. More details are available upon request. Please contact nana.obayashi@epfl.ch.



## CameraCalib folder

**getimage.m** contains script and instructions for transformation from video to world points for the room used for experiments. Uses Matlab's color thresholder app.

## CompVis folder

**trajectoryTracking.m** obtains 2D trajectory of the paper airplane flights by binary image differencing.

## Experiments folder

**0413_50samples\classifyTrajectory2.m** generates mixture models for given dataset.

**GMMExperiments** contains script and results for all optimization experiments.

## NMI Figs folder

Contains scripts used to generate figures presented in the work.

## Wing
**paper_airplane_run.m** performs the robotic experiment for this work. This involves the wing geometry generation, gcode generation for laser cutter, and robot arm manipulation. 

## matlab folder

Contains Matlab functions called within the key scripts. 

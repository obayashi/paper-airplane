clear all
vidObj =VideoReader('2022-02-15_14-37-34.mp4','CurrentTime',1.2);
l= vidObj.NumFrames
frames = read(vidObj,[1 l]);

% For the frames of interest find the difference, convert to greyscale crop
% and store
 for i = 1:l
     t1=frames(:,:,:,i);
     t2=frames(:,:,:,i+1);
     K = imabsdiff(t1,t2);   
     BW(:,:,:,i) = rgb2gray(K);
     B=BW(:,:,:,i); 
     temp = imbinarize(B, 0.15);
     Bin(:,:,i) = temp(50:1000, 600:1800);
 end

Binf = (zeros(951,1201));   % Crop the image to just the flight region
x=[0];
y=[0];

for j = 1:l
    largestRegion = bwareafilt(Bin(:,:,j), 1);          % Find the largest white area which is the plane
    [rows, columns] = find(largestRegion);
    size = sum(largestRegion, 'all')
    if(size > 10)                       % If find single pixels, ignore and use previous location - avoids detection of noise when no plane in the frame
        xc = mean(rows);
        yc = mean(columns);
        x=[x xc];
        y=[y yc];
    else
        x=[x x(length(x))];
        y=[y y(length(y))];        
    end 
    Binf = Binf + Bin(:,:,j);           % This is just to sum all the pixels so can graphically see
end

figure
imshow(Binf);
hold on
plot(y,x, '-rx', 'LineWidth', 2)

maxdist = max(y)


clc, clear, close all

% to interactively crop:
% [J,rect] = imcrop(img);
% rect has coordinates, J has cropped image

% to show image
% imshow(img);

%BW
% BW = im2bw(img_cropped,.6); % may need to play with threshold number
%     BW_logical = imshow(BW);

video=VideoReader("Logi CR920 2.mp4");
vidHeight=video.Height; 
vidWidth=video.Width; 

% Specify that reading should begin # second from the beginning of the file
video.CurrentTime = 0.6;

C1 = zeros(1,2);    % using two corners, otherwise the tracking is unprecise
C2 = zeros(1,2);

k=1;
while hasFrame(video)
    img = readFrame(video);
    img_cropped = imcrop(img,[360.5100  204.5100  399.9800  323.9800]);       % [XMIN YMIN WIDTH HEIGHT]
    BW = im2bw(img_cropped,.5); % may need to play with threshold number
    BW_logical = imshow(BW);
    
    %     img_gray = rgb2gray(img_cropped);
%     imshow(img_gray);
    C = corner(img_gray,2);                           
    corner_logical = imshow(img_gray);
    hold on
    plot(C(:,1),C(:,2),'r*')
    hold off
    [m,n] = size(C);
    C1(k,:) = C(1,:);
    if m == 2
        C2(k,:) = C(2,:);
    else
        C2(k,:) = C1(k,:);
    end
    k = k+1;
end

% save('C:\Users\carlo\Documents\Tentacles\Videos\2\Freq1\points.mat','C1','C2')
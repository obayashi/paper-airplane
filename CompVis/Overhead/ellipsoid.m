figure
[X,Y, Z] = ellipsoid(0,-0.5,0,6,3.25,3.25);
hSurface = surf(X,Y,Z);
hold on
set(hSurface,'FaceColor','r', ...
      'FaceAlpha',0.5,'FaceLighting','gouraud','EdgeColor','none')
% alpha(0.5)
camlight


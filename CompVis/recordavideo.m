function RecordVideo()
%% Construct a video input object
vobj = videoinput('winvideo', 1);

%% Set Properties for Videoinput Object
vobj.TimeOut = Inf;
vobj.FrameGrabInterval = 1;
vobj.LoggingMode = 'disk&memory';
vobj.FramesPerTrigger = 1;
vobj.TriggerRepeat = Inf;

%% Construct VideoWriter object and set Disk Logger Property
timenow = datestr(now,'hhMMss_ddmmyy');
experimentid = 1;
v = VideoWriter(['plane_', timenow,'.avi']);
v.Quality = 50;
v.FrameRate = 60;
vobj.DiskLogger = v;

% Select the source to use for acquisition.
vobj.SelectedSourceName = 'input1';

% Create an image object for previewing.
vidRes = vobj.VideoResolution;
nBands = vobj.NumberOfBands;
hImage = image( zeros(vidRes(2), vidRes(1), nBands) );
preview(vobj, hImage);
imaqmex('feature','-limitPhysicalMemoryUsage',false);


% Records 1 second video
tic
start(vobj)
pause(1);
stop(vobj)

% Compute actual Frame Rate
elapsedTime = toc;
framesaq = vobj.FramesAcquired;
ActualFR = framesaq/elapsedTime;

% Delete Object
delete(vobj);

%% Change the Frame Rate to Orinial Value and save it to new video file
% with the same timestamp
%ChangeFrameRate(['plane_', timenow,'.avi'], timenow, ActualFR)

end

%% This function changes the Frame Rate that the Saved Video is as long as the recording
function ChangeFrameRate(Video, timestr, ActualFR)

% Construct Video Objects
vidObj = VideoReader(Video);
writerObj = VideoWriter(['ActualFR_', timestr, '.avi']);
writerObj.FrameRate = ActualFR;

% Open Video Writer Object
open(writerObj);

% Read video frames until the end of the file is reached
while hasFrame(vidObj)
    vidFrame = readFrame(vidObj);
    writeVideo(writerObj, vidFrame)
    pause(1/vidObj.FrameRate);
end

% Close Video Writer Object
close(writerObj);
end
function get_trajectory('r1d1.mp4')

height = {};
dist = {};


% vid_name = 'r1d1.mp4';
vidObj =VideoReader(vid_name,'CurrentTime',1.2);
l= vidObj.NumFrames;
frames = read(vidObj,[20 l]);
cropx=[1 720]; 
cropy=[1 1280];
f_start=10;
f_end=l-20;

% For the frames of interest find the difference, convert to greyscale crop
% and store
 for i = f_start:f_end
     t1=frames(:,:,:,i);
     t2=frames(:,:,:,i+1);
     K = imabsdiff(t1,t2);   
     BW(:,:,:,i) = rgb2gray(K);
     B=BW(:,:,:,i); 
     temp = imbinarize(B, 0.15);
     %Bin(:,:,i) = temp(50:1000, 600:1800);
     Bin(:,:,i) = temp(cropx(1):cropx(2), cropy(1):cropy(2));
 end

Binf = (zeros(range(cropx)+1,range(cropy)+1));   % Crop the image to just the flight region
x=[0];
y=[0];

for j = f_start:f_end
    largestRegion = bwareafilt(Bin(:,:,j), 1);          % Find the largest white area which is the plane
    [rows, columns] = find(largestRegion);
    size = sum(largestRegion, 'all');
    if(size > 10)                       % If find single pixels, ignore and use previous location - avoids detection of noise when no plane in the frame
        xc = mean(rows);
        yc = mean(columns);
        x=[x xc];
        y=[y yc];
    else
        x=[x x(length(x))];
        y=[y y(length(y))];        
    end 
    Binf = Binf + Bin(:,:,j);           % This is just to sum all the pixels so can graphically see
end

figure
imshow(Binf);
hold on
plot(y,x, '-rx', 'LineWidth', 2)

maxdist = max(y);

dist{k} = y;
height{k} = x;

disp([num2str(k) ' done'])
end

clc, clear, close all;
addpath(genpath('C:\Users\balto\switchdrive\PaperPlane\matlab'))

wing_data = load('samples_50wings.mat').newmat;
header = wing_data(1,:);
wing_data = cell2mat(wing_data(2:end,:));
paper = [297 210]; %a4
path_flinfo = 'C:\Users\balto\switchdrive\PaperPlane\Experiments\0413_50samples\FlightInfo\';
subploc_disp = [3 7 11 15 19];
subploc_vel = [4 8 12 16 20];
[temp,pallette] = getcolor('b');

num_designs = 50;

avg_dist = [];
avg_dev = [];
avg_time = [];
var_dist = [];
var_dev = [];
var_time = [];

for i = 1:num_designs
    wing_x = wing_data(i,find(strcmp(header,'x1')):find(strcmp(header,'x4')));
    wing_y = wing_data(i,find(strcmp(header,'y1')):find(strcmp(header,'y4')));
    flinfo = load([path_flinfo 'flinfo' num2str(i) '.mat']).flinfo;
    each_dist = [];
    each_dev = [];
    each_time = [];
    
    
    figure
    set(gcf,'WindowState','fullscreen')
    
    % wing geometry
    subplot(5,4,2)
    plotwing(paper,wing_x,wing_y);
    
    % trajectory with fit
    subplot(5,4,[5 6 9 10])
    
    for j = 1:length(flinfo)
        flight = flinfo{j};
        
        each_dist(j) = flight.landing_spot(2);
        each_dev(j) = flight.landing_spot(1);
        each_time(j) = flight.fl_time;
        
        plot(flight.filt_dist,flight.filt_height,'*','Color',getcolor('b'))
        hold on
        plot(flight.fit_dist,flight.fit_height,'-','Color',getcolor('o'),'LineWidth',1)
        
    end
    grid on
    axis equal
    xlim([0 4])
    ylim([0 1.5])
    xlabel('Dist [m]')
    ylabel('Height [m]')
    L1 = plot(nan, nan, '*','Color',getcolor('b'));
    L2 = plot(nan, nan, '-','Color',getcolor('o'),'LineWidth',1);
    legend([L1, L2], {'raw filtered', 'fit'})
    
    % raw trajectory before all the filters
    subplot(5,4,[13 14 17 18])
    
    for j = 1:length(flinfo)
        flight = flinfo{j};
        
        plot(flight.raw_dist,flight.raw_height,'-','Color',pallette{j},'LineWidth',1)
        hold on
        
    end
    grid on
    axis equal
    xlim([0 4])
    ylim([0 1.5])
    xlabel('Dist [m]')
    ylabel('Height [m]')
    L1 = plot(nan, nan, '-','Color',pallette{1},'LineWidth',1);
    L2 = plot(nan, nan, '-','Color',pallette{2},'LineWidth',1);
    L3 = plot(nan, nan, '-','Color',pallette{3},'LineWidth',1);
    L4 = plot(nan, nan, '-','Color',pallette{4},'LineWidth',1);
    L5 = plot(nan, nan, '-','Color',pallette{5},'LineWidth',1);
    legend([L1 L2 L3 L4 L5], {'fl1','fl2','fl3','fl4','fl5'})
    
    % displacement and velocity
    for j = 1:length(flinfo)
        flight = flinfo{j};
        
        subplot(5,4,subploc_disp(j))
        plot(flight.vec_time,flight.filt_dist,'-',flight.vec_time,flight.filt_height,'-',...
            flight.vec_time,hypot(flight.filt_dist,flight.filt_height),'-','LineWidth',1)
        grid on
        xlabel('Time [s]')
        ylabel('Displacement [m]')
        legend('Hori','Vert','Tot','orientation','horizontal','location','south','color','none','box','off')
        xlim([0 2])
        
        subplot(5,4,subploc_vel(j))
        plot(flight.vec_time,flight.vel_dist,'-',flight.vec_time,flight.vel_height,'-',...
            flight.vec_time,hypot(flight.vel_dist,flight.vel_height),'-','LineWidth',1)
        grid on
        xlabel('Time [s]')
        ylabel('Velocity [m/s]')
        legend('Hori','Vert','Tot','orientation','horizontal','location','south','color','none','box','off')
        xlim([0 2])
    end
    
    % calculate averages and make updated data matrix
    avg_dist(i) = mean(each_dist);
    avg_dev(i) = mean(abs(each_dev));
    avg_time(i) = mean(each_time);
    var_dist(i) = var(each_dist);
    var_dev(i) = var(abs(each_dev));
    var_time(i) = var(each_time);
    
    % text info
    subplot(5,4,1)
    set(gca,'XTick',[],'YTick',[])
    message = sprintf(['Design #' num2str(i)...
        '\n\nDistance avg: ' sprintf('%.2f',avg_dist(i)) ' [m] / var: ' sprintf('%.3f',var_dist(i)) ' [m^2]'...
        '\nCenterdev avg: ' sprintf('%.2f',avg_dev(i)) ' [m] / var: ' sprintf('%.3f',var_dev(i)) ' [m^2]'...
        '\nTime avg: ' sprintf('%.2f',avg_time(i)) ' [s] / var: ' sprintf('%.3f',var_time(i)) ' [s^2]']);
    text(0.1,0.5, message, 'FontSize', 10.5, 'Color', [.6 .2 .6]);
    
    saveas(gcf,['Datasheet\wing' num2str(i) '.png'])
end

newdata = [wing_data avg_dist' var_dist' avg_dev' var_dev' avg_time' var_time'];
newheader = [header {'avg_dist'} {'var_dist'} {'avg_dev'} {'var_dev'} {'avg_time'} {'var_time'}];
updatedmat = [newheader;num2cell(newdata)];

close all;



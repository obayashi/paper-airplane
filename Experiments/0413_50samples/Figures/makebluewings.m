clc, clear, close all;
addpath(genpath('C:\Users\balto\switchdrive\PaperPlane\matlab'))

path_data = 'G:\.shortcut-targets-by-id\1wYXDAMCh59hhAOMrpcbdbmdNo1mZvuMP\CREATE Lab\Nana PhD\2022 Paper Airplane\Experiment\0510_BO\0519_target3\WingData\';
wing_data_name = 'wing_data_0520_1132';

wing_data = load([path_data wing_data_name]).wing_data;
header = wing_data(1,:);
wing_data = cell2mat(wing_data(2:end,:));

figure
for i = 1:size(wing_data,1)
    subplot(5,6,i)
    wing_x = wing_data(i,find(strcmp(header,'x1')):find(strcmp(header,'x4')));
    wing_y = wing_data(i,find(strcmp(header,'y1')):find(strcmp(header,'y4')));
    desnum = wing_data(i,find(strcmp(header,'idx_orig')));
    plotwing_paper(wing_x,wing_y,desnum)
    
end


clc, clear, close all;
addpath(genpath('C:\Users\balto\switchdrive\PaperPlane\matlab'))

path_flinfo = 'C:\Users\balto\switchdrive\PaperPlane\Experiments\0413_50samples\FlightInfo\';
path_data = 'C:\Users\balto\switchdrive\PaperPlane\Experiments\0413_50samples\';

wing_data = load([path_data 'samples_50wings.mat']).newmat;
header = wing_data(1,:);
wing_data = cell2mat(wing_data(2:end,:));
[temp,pallette] = getcolor('b');
FS = 12;
MS = 10;


num_designs = 50;
num_flights = 5;
km_mat = []; % num of coefficients
design_mat = [];
rng(1)

%===partition training and test===
data = 1:num_designs;
holdout = 0.0;

if holdout == 0 % no partitioning
    
    dataTest = 1:num_designs;
    dataTrain = 1:num_designs;
else
    cv = cvpartition(length(data),'HoldOut',holdout);
    idx_test = cv.test;
    dataTrain = data(~idx_test);
    dataTest  = data(idx_test)
end



%===make main data matrix===
for i = 1:length(dataTrain)
    flinfo = load([path_flinfo 'flinfo' num2str(dataTrain(i)) '.mat']).flinfo;
    for j = 1:length(flinfo)
        flight = flinfo{j};
        geom_para = wing_data(dataTrain(i),[find(strcmp(header,'sweep')) find(strcmp(header,'back_sweep')) find(strcmp(header,'w/l'))]);
        dist_para = flight.landing_spot(2);
        centdev_para = flight.landing_spot(1);
        fltime_para = flight.fl_time;
        
        km_mat(end+1,:) = [flight.tstat flight.estimates geom_para dist_para centdev_para fltime_para];
        design_mat(end+1,:) = [dataTrain(i) j];
    end
end
km_header = {'tstat5','tstat4','tstat3','tstat2','tstat1','tstat0',...
    'coeff5','coeff4','coeff3','coeff2','coeff1','coeff0',...
    'sweep','back_sweep','w/l','dist','centdev','fltime'};


% === add distance to columns  ===
choose_cols = [7 8 9 10 16];

%===make matrix for kmeans again===
X = makeKmeansMatrix(km_mat,choose_cols,'all');

%===FInd number of clusters using evalclusters, which supports, the Calinski-Harabasz criterion
clust = zeros(size(X,1),6);
best_totsumd = zeros(6,1);
for i=1:6
    %     [clust(:,i),C,sumd] = kmeans(X,i,'emptyaction','singleton',...
    %         'replicate',10);
    [clust(:,i),C,sumd] = kmeans(X,i,'Distance','cityblock','replicate',10);
    
    best_totsumd(i) = sum(sumd);
end
eva = evalclusters(X,clust,'CalinskiHarabasz');
if  holdout>0 % for partitioned data, force optimal k to be 3
    optimalk = 3;
else
    optimalk = eva.OptimalK;
end

% temp force optimalk to 3====
optimalk = 3;

idx = clust(:,optimalk);
fprintf('Optimal K: %1.0f',optimalk)
figure
plot(1:6,best_totsumd,'o-')
hold on
plot([optimalk optimalk],[min(best_totsumd) max(best_totsumd)],'r--')
xlabel('number of clusters')
ylabel('best total sum of distances of centroid to each point')
grid on

%=====reorder the cluster based on median distance: shortest cluster should
%be cluster 1, etc ====
get_flightdata = @(flidx,headerstr) km_mat(flidx,find(strcmp(km_header,headerstr)));
medianvec = [];
for i = 1:optimalk
    medianvec(end+1) = quantile(get_flightdata(idx==i,'dist'),.5);
end
[temp sortedidx] = sort(medianvec);
for i = 1:optimalk
    idx(idx==sortedidx(i)) = optimalk+i;
end
idx = idx-optimalk;

%=====plot clustered trajectories=====
figure
for i = 1:optimalk
    actualtoshow = 80;
    singlecluster = find(idx==i);
    numincluster = sum(idx==i);
    if numincluster<actualtoshow
        actualtoshow = numincluster;
    end
    randomflights = randperm(numincluster,actualtoshow);
    origtrajidx = 1:(length(dataTrain)*num_flights);
    origidx = origtrajidx(singlecluster(randomflights));
    subplot(3,1,i)
    for j = 1:actualtoshow
        flight = load([path_flinfo 'flinfo' num2str(design_mat(origidx(j),1)) '.mat']).flinfo{design_mat(origidx(j),2)};
        traj_dist = flight.filt_dist;
        traj_height = flight.filt_height;
        
        switch i
            case 1
                palette = palettered();
            case 2
                palette = palettegreen();
            case 3
                palette = paletteblue();
        end
        palnum = mod(j,10);
        if palnum == 0
            palnum = 10;
        end
        plot(traj_dist,traj_height,'LineWidth',2,'Color',palette{palnum})
        hold on
    end
    ax = gca;
    ax.FontSize = FS;
    
    %     title(['Flights from Cluster ' num2str(i)],'FontSize',FS)
    
    xlabel('Distance [m]','FontSize',FS)
    ylabel('Height [m]','FontSize',FS)
    grid on
    %     axis equal
    xlim([0 5])
    ylim([0 1.5])
    
end

%====find optimal k for each cluster====
get_wing_geom = @(cluster_num,headerstr) wing_data(design_mat(idx==cluster_num,1),[find(strcmp(header,headerstr))]); % gets specific column data based on cluster
subidx = {};
suboptimalk = [];
max_subgroups = 3;
trajincluster = hist(idx,1:max(idx));
[val,clusterwithmany] = max(trajincluster);
for i = 1:optimalk
    X = [get_wing_geom(i,'sweep') get_wing_geom(i,'back_sweep') get_wing_geom(i,'w/l')];
    clust = zeros(size(X,1),max_subgroups);
    %     if i == clusterwithmany
    %         max_subgroups = 3;
    %     end
    for j=1:max_subgroups
        [clust(:,j),C,sumd] = kmeans(X,j,'emptyaction','singleton',...
            'replicate',10);
    end
    if max_subgroups>1
        eva = evalclusters(X,clust,'silhouette');
        %     eva = evalclusters(X,clust,'CalinskiHarabasz');
        %     if i == clusterwithmany
        %         suboptimalk(i) = 4;
        %     else
        
        suboptimalk(i) = eva.OptimalK;
    else
        suboptimalk(i) = 1;
    end
    %     end
    subidx{i} = clust(:,suboptimalk(i));
end
suboptimalk


%====geom fig====
figure
for i = 1:optimalk
    mainclusterwings = design_mat(idx==i,1);
    subclusteridx = subidx{i};
    switch i
        case 1
            palette = palettered();
        case 2
            palette = palettegreen();
        case 3
            palette = paletteblue();
    end
    
    for j = 1:suboptimalk(i)
        designstoplot = unique(mainclusterwings(subclusteridx==j));
        panelnum = (i-1)*3+j;
        subplot(optimalk,max(suboptimalk),panelnum)
        for k = 1:length(designstoplot)
            %     for j = 1:length(designstoplot)
            palnum = mod(k,10);
            if palnum == 0
                palnum = 10;
            end
            wing_x = wing_data(designstoplot(k),find(strcmp(header,'x1')):find(strcmp(header,'x4')));
            wing_y = wing_data(designstoplot(k),find(strcmp(header,'y1')):find(strcmp(header,'y4')));
            plotwing([297 210],wing_x,wing_y,palette{palnum});
            set(gca,'XTick',[],'YTick',[],'Visible','off')
%             box off
            %             title(['Cluster ' num2str(i) ' Subcluster ' num2str(j)])
            hold on
        end
        %     end
    end
    
    grid on
    axis equal
end


%===make GMM===
getGMModel = @(cluster_num,subcluster_num) fitgmdist([get_wing_geom(cluster_num,'sweep'),get_wing_geom(cluster_num,'back_sweep'),get_wing_geom(cluster_num,'w/l')],subcluster_num,'Start',subidx{cluster_num}); % gets 3D GMM for cluster
getGMModel_error = @(cluster_num,subcluster_num) fitgmdist([get_wing_geom(cluster_num,'sweep'),get_wing_geom(cluster_num,'back_sweep'),get_wing_geom(cluster_num,'w/l')],subcluster_num,'SharedCovariance',true,'Start',subidx{cluster_num}); % gets 3D GMM for cluster
get_wing_geom_direct = @(des_num,headerstr) wing_data(des_num,[find(strcmp(header,headerstr))]); % gets specific column data based on cluster
% first get GMM model for all clusters
for j = 1:optimalk
    try
        
        
        
        modelStruct(j).model = getGMModel(j,suboptimalk(j));
    catch
        modelStruct(j).model = getGMModel_error(j,suboptimalk(j));
    end
end

%=====get probabilities=====
getdesnum = @(flnum,cluster_num,subcluster_num) design_mat(flnum(subidx{cluster_num}==subcluster_num),1);
getGMModel_indiv = @(desnum) fitgmdist([get_wing_geom_direct(desnum,'sweep'),...
    get_wing_geom_direct(desnum,'back_sweep'),get_wing_geom_direct(desnum,'w/l')],1);
probmat = zeros(length(dataTest),optimalk);
for i = 1:optimalk
    for j = 1:length(dataTest)
        qp = [get_wing_geom_direct(dataTest(j),'sweep'),...
            get_wing_geom_direct(dataTest(j),'back_sweep'),...
            get_wing_geom_direct(dataTest(j),'w/l')];
        %         qp = [55.3203 115.6595 0.6852];
        %         flnum = find(idx==i);
        %         prob = [];
        %         for k = 1:suboptimalk(i)
        %             desnum = getdesnum(flnum,i,k);
        %             model_indiv = getGMModel_indiv(desnum);
        probmat(j,i) = getProbability(qp,modelStruct(i).model);
        %         end
        %         probmat(j,i) = max(prob);
    end
end

%=====normalize probabilities=====
probsum = sum(probmat,2);
probmat = bsxfun(@rdivide,probmat,probsum(:)); % divide each row by corresponding element in vector
probmat = [dataTest' probmat];




% =====design space and histogram=====
temp = probmat(:,2:end);
[M, I] = max(temp,[],2);


% FS = 12;
figure
% subplot(1,3,[1 2])

plsw = wing_data(I==1,find(strcmp(header,'sweep')));
plswb = wing_data(I==1,find(strcmp(header,'back_sweep')));
plwol = wing_data(I==1,find(strcmp(header,'w/l')));
plot3(plsw,plswb,plwol,'o','Color',getcolor('r'),'MarkerSize',MS,'MarkerFaceColor',getcolor('r'));
hold on

plsw = wing_data(I==2,find(strcmp(header,'sweep')));
plswb = wing_data(I==2,find(strcmp(header,'back_sweep')));
plwol = wing_data(I==2,find(strcmp(header,'w/l')));
plot3(plsw,plswb,plwol,'o','Color',getcolor('g'),'MarkerSize',MS,'MarkerFaceColor',getcolor('g'));

plsw = wing_data(I==3,find(strcmp(header,'sweep')));
plswb = wing_data(I==3,find(strcmp(header,'back_sweep')));
plwol = wing_data(I==3,find(strcmp(header,'w/l')));
plot3(plsw,plswb,plwol,'o','Color',getcolor('b'),'MarkerSize',MS,'MarkerFaceColor',getcolor('b'));
xlim([0 80])
ylim([0 150])
zlim([0 2])

grid on
legend('Behavior 1','Behavior 2','Behavior 3','FontSize',FS,'Location','BEST')
xlabel('Sweep, \Lambda [deg]','FontSize',FS)
ylabel('Back sweep, \Lambda_b [deg]','FontSize',FS)
zlabel('Width-to-length, W/L','FontSize',FS)
ax = gca;
ax.FontSize = FS;

figure
% subplot(1,3,3)
forhist = km_mat(:,find(strcmp(km_header,'dist')));
h = histfit(forhist,20,'kernel');
h(1).FaceColor = getcolor('lb');
xlabel('Distance [m]','FontSize',FS)
ax = gca;
ax.FontSize = FS;

%=========
geom_str = {'\Lambda [deg]','\Lambda_b [deg]','W/L'};
figure
for i = 1:optimalk
    switch i
        case 1
            palette = palettered();
        case 2
            palette = palettegreen();
        case 3
            palette = paletteblue();
    end
    
    subplot(3,1,i)
    %     wing_geom = [get_wing_geom(i,'sweep') get_wing_geom(i,'back_sweep') get_wing_geom(i,'w/l')];
    %     gscatter3(wing_geom(:,1),wing_geom(:,2),wing_geom(:,3),subidx{j})
    plot3(0,0,0)
    g = gca;
    isolatedGMModel = modelStruct(i).model;
    
    %     sgtitle(['Cluster ' num2str(i)])
    sigdims = ndims(isolatedGMModel.Sigma);
    for j = 1:suboptimalk(i)
        if or(sigdims<3,suboptimalk(i)==1)
            sigplot = isolatedGMModel.Sigma;
        else
            sigplot = isolatedGMModel.Sigma(:,:,j);
        end
        plot_gaussian_ellipsoid(isolatedGMModel.mu(j,:),sigplot,2,20,g,palette{j*2-1})
        xlim([-10 80])
        ylim([0 150])
        zlim([0 2])
    end
    xlabel(geom_str{1},'FontSize',FS)
    ylabel(geom_str{2},'FontSize',FS)
    zlabel(geom_str{3},'FontSize',FS)
    grid on
    %     title(['Cluster ' num2str(i)],'FontSize',FS)
    ax = gca;
    ax.FontSize = FS;
end





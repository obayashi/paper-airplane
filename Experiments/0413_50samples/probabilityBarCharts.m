clc, clear, close all;

excel_path = 'C:\Users\balto\switchdrive\PaperPlane\Experiments\0413_50samples\';
excel_name = '0413_info.xlsx';
excel_sheet = 'Sheet3addmore';

palette_epfl = paletteEPFL();

T = readtable([excel_path excel_name],'Sheet',excel_sheet);

holdout = [0.1 0.15 0.2 0.25 0.3];

gt = table2array(T(:,1:4));
predTest = T(:,5:end);

for i = 1:length(holdout)
    pred = predTest(:,(i-1)*4+1:(i-1)*4+4);
    pred = table2array(pred(1:floor(holdout(i)*50),:));
    figure
    y = [];
    for j = 1:size(pred,1)
        subplot(3,5,j)
        X = categorical({'B1','B2','B3'});
        X = reordercats(X,{'B1','B2','B3'});
        y = [gt(pred(j,1),2:end)' pred(j,2:end)'];
        b = bar(X,y);
        b(1).FaceColor = palette_epfl{7};
        b(2).FaceColor = palette_epfl{6};
        set(gca,'FontSize',12)
        ylim([0 1])
        title(['Wing design ' num2str(pred(j,1))])
        
        
        
    end
   
%     legend('Ground truth','Predicted','FontSize',12)
    sgtitle(['Holdout: ' num2str(holdout(i))])
end





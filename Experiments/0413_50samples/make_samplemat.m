clc, clear, close all;

idx = load('idx_50samples.mat').uq_vals;

path = 'C:\Users\balto\switchdrive\PaperPlane\Wing\out_0413\';

mat_name = 'data\0413_random2.mat';

outmat = load([path mat_name]).outmat;
header = outmat(1,:);
header = [{'idx_orig'} header];
data = cell2mat(outmat(2:end,:));
data = data(idx,:);
data = [idx' data];

newmat = [header;num2cell(data)];
% save('samples_50wings.mat','newmat');

paper_x = [0 0 297 297 0];
paper_y = [0 210 210 0 0];

figure
plot(paper_x,paper_y,'k-')
hold on
for i = 1:length(idx)
    wing_x = data(i,2:5);
    wing_y = data(i,6:9)-(210/2);
    wing_x = [wing_x wing_x(end-1:-1:1)];
    wing_y = [wing_y wing_y(end-1:-1:1).*-1];
    wing_y = wing_y + (210/2);
    plot(wing_x,wing_y)
end
axis equal



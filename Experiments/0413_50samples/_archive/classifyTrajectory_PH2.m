clc, clear, close all;
addpath(genpath('C:\Users\balto\switchdrive\PaperPlane\matlab'))

path_flinfo = 'C:\Users\balto\switchdrive\PaperPlane\Experiments\0413_50samples\FlightInfo\';
path_data = 'C:\Users\balto\switchdrive\PaperPlane\Experiments\0413_50samples\';

% addpath(genpath('/Users/huwilerepfl/switchdrive/NanaPhD/PaperPlane/matlab'))
% 
% path_flinfo = '/Users/huwilerepfl/switchdrive/NanaPhD/PaperPlane/Experiments/0413_50samples/FlightInfo/';
% path_data = '/Users/huwilerepfl/switchdrive/NanaPhD/PaperPlane/Experiments/0413_50samples/';

wing_data = load([path_data 'samples_50wings.mat']).newmat;
header = wing_data(1,:);
wing_data = cell2mat(wing_data(2:end,:));
[temp,pallette] = getcolor('b');

num_designs = 50;
num_flights = 5;
km_mat = []; % num of coefficients
design_mat = [];

for i = 1:num_designs
    
    flinfo = load([path_flinfo 'flinfo' num2str(i) '.mat']).flinfo;
    
    
    for j = 1:length(flinfo)
        flight = flinfo{j};
        geom_para = wing_data(i,[find(strcmp(header,'sweep')) find(strcmp(header,'back_sweep')) find(strcmp(header,'w/l'))]);
        dist_para = flight.landing_spot(2);
        km_mat(end+1,:) = [flight.tstat flight.estimates geom_para dist_para];
        design_mat(end+1,:) = [i j];
        
    end
    
    
    
end

% make all combinations of the tstats
num_coeffs = 6;
combcell = {};
for i = 2:num_coeffs
    C = nchoosek(1:6,i);
    [rows cols] = size(C);
    for j = 1:rows
        combcell{end+1} = C(j,:);
    end
end

%make matrix for kmeans
tstat_idx = [1:4];
coeff_idx = [7:10];
geom_idx = [13:15];
choose_cols = [coeff_idx];
X = makeKmeansMatrix(km_mat,choose_cols,'all');

% % first try the elbow method to find out number of clusters
% % this was tried with the params p5, p4, p3, p2
% best_totsumd = [];
% max_cluster = 10;
% for i = 1:max_cluster
%     opts = statset('Display','final');
%     [idx,C,sumd] = kmeans(X,i,'Distance','cityblock',...
%         'Replicates',100,'Options',opts);
%     best_totsumd(i) = sum(sumd);
% end
% figure
% plot(1:max_cluster,best_totsumd,'o-')
% xlabel('number of clusters')
% ylabel('best total sum of distances of centroid to each point')
% grid on


%===choose no of cluster and run kmeans=====
num_cluster = 3;
opts = statset('Display','final');
[idx,C,sumd] = kmeans(X,num_cluster,'Distance','cityblock',...
    'Replicates',100,'Options',opts);

% %silhouette histogram
% % since sil score is from -1 to 1
% %add 1 to every element, divide element by 2, then as total score, sum
% %everything and divide by N
% silscore = silhouette(X,idx);
% newscore = (silscore + ones(length(silscore),1))./2;
% figure
% for i = 1:num_cluster
%     score = sum(newscore(idx==i))/length(newscore(idx==i));
%     message = sprintf(['composite score: ' sprintf('%.3f',score)]);
%     subplot(3,2,i)
%     hist(silscore(idx==i),10)
%     hold on
%     text(0,3, message, 'FontSize', 12, 'Color', 'm');
%     title(['cluster ' num2str(i) ' silhouette score'])
% end
% score = sum(newscore)/length(newscore);
% message = sprintf(['composite score: ' sprintf('%.3f',score)]);
% subplot(3,2,[5 6])
% hist(silscore,10)
% hold on
% text(0,3, message, 'FontSize', 12, 'Color', getcolor('o'));



% %distance of points to the centroids
% str = {'p5','p4','p3','p2'};
% combs = [1 2;1 3;1 4;2 3;2 4;3 4];
% % combs = [choose_cols(1) choose_cols(2);choose_cols(1) choose_cols(3);...
% %     choose_cols(1) choose_cols(4);choose_cols(2) choose_cols(3);...
% %     choose_cols(2) choose_cols(4);choose_cols(3) choose_cols(4)];
% figure
% for i = 1:length(combs)
%     subplot(3,2,i)
%     plot(X(idx==1,combs(i,1)),X(idx==1,combs(i,2)),'r.','MarkerSize',12)
%     hold on
%     plot(X(idx==2,combs(i,1)),X(idx==2,combs(i,2)),'g.','MarkerSize',12)
%     plot(X(idx==3,combs(i,1)),X(idx==3,combs(i,2)),'b.','MarkerSize',12)
%     plot(X(idx==4,combs(i,1)),X(idx==4,combs(i,2)),'c.','MarkerSize',12)
%     plot(C(:,combs(i,1)),C(:,combs(i,2)),'kx',...
%         'MarkerSize',15,'LineWidth',3)
%     legend('Cluster 1','Cluster 2','Cluster 3','Cluster 4','Centroids',...
%         'Location','NW')
%     xlabel(['tstat of ' str{combs(i,1)}])
%     ylabel(['tstat of ' str{combs(i,2)}])
%     hold off
% end

%=====plot clustered trajectories=====
plotClusteredTrajectory(num_cluster,20,path_flinfo,idx,design_mat,1:(num_designs*num_flights),NaN,0)

%=====find probability of cluster for each design=====
prob_cluster = findClusterProbability(idx,num_flights,num_designs,num_cluster);

%=====plot clustered geometries for a specified range of probability=====
prob_strength = [0.8 1];
[plotteddesigns] = plotClusteredWingsProb(prob_strength,num_cluster,prob_cluster,wing_data,header);

%====if kmeans is run using coefficient or tstat at k=3, usually one
%cluster is very big. so divide that into several clusters. run kmeans with
%geometry
if and(isequal(choose_cols,coeff_idx),num_cluster==3)
    trajincluster = hist(idx,1:max(idx));
    [val,clusterwithmany] = max(trajincluster);
    subdesignindex = find(idx==clusterwithmany);
    subplotteddesigns = plotteddesigns{clusterwithmany};
    newtrajidx = []; % exclude trajectories that belong to wings not in the biggest cluster
    newwingnums = [];
    for i = 1:length(subdesignindex)
        wingdesignnum = design_mat(subdesignindex(i));
        if sum(subplotteddesigns==wingdesignnum)>0
            newtrajidx(end+1) = subdesignindex(i);
            newwingnums(end+1) = wingdesignnum;
        end
    end
    uqnewwing = unique(newwingnums);
    numuqwing = [];
    for i = 1:length(uqnewwing)
        numuqwing(end+1) = sum(newwingnums==uqnewwing(i));
    end
    subnum_cluster = 3;
    choose_subcols = geom_idx;
    X = makeKmeansMatrix(km_mat,choose_subcols,newtrajidx);
    opts = statset('Display','final');
    [subidx,subC,subsumd] = kmeans(X,subnum_cluster,'Distance','cityblock',...
        'Replicates',100,'Options',opts);
    plotClusteredTrajectory(subnum_cluster,20,path_flinfo,subidx,design_mat,newtrajidx,clusterwithmany,1)
    %plot the one with highest probability
    subprob = zeros(subnum_cluster,length(numuqwing));
    for i = 1:length(numuqwing)
        if i==1
            wingclusteridx = [subidx(1:sum(numuqwing(1:i)))];
        else
            wingclusteridx = [subidx(sum(numuqwing(1:i-1))+1:sum(numuqwing(1:i)))];
        end
        for j = 1:subnum_cluster
            subprob(j,i) = sum(wingclusteridx==j)/numuqwing(i);
        end
    end
    subplotteddesigns = {};
    figure
    for i = 1:subnum_cluster
        subplot(3,2,i)
        %         count = 0;
        temp_vec = [];
        for j = 1:length(numuqwing)
            val_idx = find(max(subprob(:,j))==subprob(:,j));
            if sum(val_idx==i)>0
                temp_vec(end+1) = uqnewwing(j);
                wing_x = wing_data(uqnewwing(j),find(strcmp(header,'x1')):find(strcmp(header,'x4')));
                wing_y = wing_data(uqnewwing(j),find(strcmp(header,'y1')):find(strcmp(header,'y4')));
                plotwing([297 210],wing_x,wing_y);
                hold on
                %                 count = count+1;
            end
        end
        subplotteddesigns{i} = temp_vec;
        title(['P(Cluster' num2str(clusterwithmany) char(64+i) ')=[' num2str(min(prob_strength)) ' ' num2str(max(prob_strength)) ']; ' num2str(length(temp_vec)) ' of 50 designs'])
        grid on
        axis equal
    end
end


% % plot clustered geometries corresponding to the trajectories. get
% % probability then choose the cluster with highest probability. if same
% % probability for multiple clusters, plot in multiple clusters
% figure
% for i = 1:num_cluster
%     subplot(3,2,i)
%     for j = 1:num_designs
%         val_idx = find(max(prob_cluster(:,j))==prob_cluster(:,j));
%         if sum(val_idx==i)>0
%             wing_x = wing_data(j,find(strcmp(header,'x1')):find(strcmp(header,'x4')));
%             wing_y = wing_data(j,find(strcmp(header,'y1')):find(strcmp(header,'y4')));
%             plotwing([297 210],wing_x,wing_y);
%             hold on
%         end
%     end
%     title(['geometry of cluster ' num2str(i)])
%     grid on
%     axis equal
% end

% distance vs. geom para plots
dist_idx = 16; % in km_mat
num_geompara = 3;
geom_str = {'sweep [deg]','back sweep [deg]','width/length'};
figure
for i = 1:num_geompara
    for j = 1:num_cluster
        subplot(num_geompara,num_cluster,(i-1)*num_cluster+j)
        plot(km_mat(idx==j,geom_idx(i)),km_mat(idx==j,dist_idx),'.','Color',pallette{j},'MarkerSize',12)
        xlabel(geom_str{i})
        ylabel('distance [m]')
        title(['cluster ' num2str(j)])
        grid on
        ylim([0 5])
    end
end

%====3d plot of geom and cluster=====
figure
count = 0;
legstr = {};
for i = 1:num_cluster
    if isequal(choose_cols,coeff_idx) & num_cluster==3
        if i == clusterwithmany
            for j = 1:subnum_cluster
                wingidxvec = subplotteddesigns{j};
                count = count + 1;
                legstr{end+1} = ['Cluster ' num2str(i) char(64+j)];
                wing_geom = wing_data(wingidxvec,[find(strcmp(header,'sweep')) find(strcmp(header,'back_sweep')) find(strcmp(header,'w/l'))]);
                plot3(wing_geom(:,1),wing_geom(:,2),wing_geom(:,3),'.','Color',pallette{count},'MarkerSize',12)
                hold on
            end
        else
            wingidxvec = plotteddesigns{i};
            count = count + 1;
            legstr{end+1} = ['Cluster ' num2str(i)];
            wing_geom = wing_data(wingidxvec,[find(strcmp(header,'sweep')) find(strcmp(header,'back_sweep')) find(strcmp(header,'w/l'))]);
            plot3(wing_geom(:,1),wing_geom(:,2),wing_geom(:,3),'.','Color',pallette{count},'MarkerSize',12)
            hold on
        end
    else
        wingidxvec = plotteddesigns{i};
        count = count + 1;
        legstr{end+1} = ['Cluster ' num2str(i)];
        wing_geom = wing_data(wingidxvec,[find(strcmp(header,'sweep')) find(strcmp(header,'back_sweep')) find(strcmp(header,'w/l'))]);
        plot3(wing_geom(:,1),wing_geom(:,2),wing_geom(:,3),'.','Color',pallette{count},'MarkerSize',12)
        hold on
    end
end
grid on
xlabel('sweep')
ylabel('back sweep')
zlabel('width/length')
legend(legstr,'Location','Best')


% % regression model for geometric para and distance
% for i = 1:num_cluster
%     disp(['regression model for cluster' num2str(i)])
%     mdl = fitlm(km_mat(idx==i,geom_idx),km_mat(idx==i,dist_idx))
% end

%====make scatter plot for each cluster with geometrical axes colored by
%probability===== first main cluster only
threshold = 0.5;
figure
for i = 1:num_cluster
    wing_geom = wing_data(:,[find(strcmp(header,'sweep')) find(strcmp(header,'back_sweep')) find(strcmp(header,'w/l'))]);
    prob_vec = prob_cluster(i,:);
    prob_vec(prob_vec==0) = NaN;
    subplot(3,2,i)
    for j = 1:2
        if j == 1
            gmmidx = find(prob_vec<0.6);
        else
            gmmidx = find(prob_vec>=0.6);
        end
        
        if ~isempty(gmmidx)
            scatter3(wing_geom(gmmidx,1),wing_geom(gmmidx,2),wing_geom(gmmidx,3),prob_vec(gmmidx).*100,'filled','MarkerFaceColor',pallette{j})
            hold on
            if length(gmmidx)>3
                isolatedGMModel = fitgmdist([wing_geom(gmmidx,1),wing_geom(gmmidx,2),wing_geom(gmmidx,3)],1);
                get2DIsoProb = @(x,y,z) arrayfun(@(x0,y0,z0) pdf(isolatedGMModel,[x0 y0 z0]),x,y,z);
                g = gca;
                plot_gaussian_ellipsoid(isolatedGMModel.mu,isolatedGMModel.Sigma,1.8,20,g,pallette{j})
            end
        end
    end
    
    L1 = plot(nan, nan, 'o','Color',pallette{1});
    L2 = plot(nan, nan, 'o','Color',pallette{2});
    legend([L1, L2], {['<' num2str(threshold)], ['>=' num2str(threshold)]})
    
    title(['GMM Cluster' num2str(i)])
    xlabel('sweep')
    ylabel('back sweep')
    zlabel('width/length')
    
    
end


%====make GMM plot with blobs for all 250 flights, clustered into flight behavior. Do
%sub kmeans on the largest cluster for 250 flights, not geometry=====
count = 0;
figure
legstr = {};
for i = 1:num_cluster
    if isequal(choose_cols,coeff_idx) & num_cluster==3
        designidxmany = design_mat(idx==clusterwithmany);
        X = makeKmeansMatrix(km_mat,choose_subcols,find(idx==clusterwithmany));
        opts = statset('Display','final');
        [subidx,subC,subsumd] = kmeans(X,subnum_cluster,'Distance','cityblock',...
            'Replicates',100,'Options',opts);
        if i == clusterwithmany
            for j = 1:subnum_cluster
                count = count + 1;
                designidx = designidxmany(subidx==j);
                wing_geom = [];
                for k = 1:length(designidx)
                    wing_geom(end+1,:) = wing_data(designidx(k),[find(strcmp(header,'sweep')) find(strcmp(header,'back_sweep')) find(strcmp(header,'w/l'))]);
                end
                scatter3(wing_geom(:,1),wing_geom(:,2),wing_geom(:,3),'filled','MarkerFaceColor',pallette{count})
                hold on
                isolatedGMModel = fitgmdist([wing_geom(:,1),wing_geom(:,2),wing_geom(:,3)],1);
                get2DIsoProb = @(x,y,z) arrayfun(@(x0,y0,z0) pdf(isolatedGMModel,[x0 y0 z0]),x,y,z);
                g = gca;
                plot_gaussian_ellipsoid(isolatedGMModel.mu,isolatedGMModel.Sigma,1.8,20,g,pallette{count})
                legstr{end+1} = ['Cluster ' num2str(i) char(64+j)];
            end
        else
            designidx = design_mat(idx==i,1);
            count = count + 1;
            
            wing_geom = [];
            for j = 1:length(designidx)
                wing_geom(end+1,:) = wing_data(designidx(j),[find(strcmp(header,'sweep')) find(strcmp(header,'back_sweep')) find(strcmp(header,'w/l'))]);
            end
            scatter3(wing_geom(:,1),wing_geom(:,2),wing_geom(:,3),'filled','MarkerFaceColor',pallette{count})
            hold on
            isolatedGMModel = fitgmdist([wing_geom(:,1),wing_geom(:,2),wing_geom(:,3)],1);
            get2DIsoProb = @(x,y,z) arrayfun(@(x0,y0,z0) pdf(isolatedGMModel,[x0 y0 z0]),x,y,z);
            g = gca;
            plot_gaussian_ellipsoid(isolatedGMModel.mu,isolatedGMModel.Sigma,1.8,20,g,pallette{count})
            legstr{end+1} = ['Cluster ' num2str(i)];
        end
    else
        designidx = design_mat(idx==i,1);
        count = count + 1;
        
        wing_geom = [];
        for j = 1:length(designidx)
            wing_geom(end+1,:) = wing_data(designidx(j),[find(strcmp(header,'sweep')) find(strcmp(header,'back_sweep')) find(strcmp(header,'w/l'))]);
        end
        scatter3(wing_geom(:,1),wing_geom(:,2),wing_geom(:,3),'filled','MarkerFaceColor',pallette{count})
        hold on
        isolatedGMModel = fitgmdist([wing_geom(:,1),wing_geom(:,2),wing_geom(:,3)],1);
        get2DIsoProb = @(x,y,z) arrayfun(@(x0,y0,z0) pdf(isolatedGMModel,[x0 y0 z0]),x,y,z);
        g = gca;
        plot_gaussian_ellipsoid(isolatedGMModel.mu,isolatedGMModel.Sigma,1.8,20,g,pallette{count})
        legstr{end+1} = ['Cluster ' num2str(i)];
    end
    
end
templeg=[];
for i = 1:count
    templeg(end+1) = plot(nan, nan, 'o','Color',pallette{i},'MarkerFaceColor',pallette{i});
end
legend(templeg, legstr)
xlabel('sweep')
ylabel('back sweep')
zlabel('width/length')
title('GMM of all 250 flights in clusters')

% %=====design space=======
% wing_geom = wing_data(designidx(k),[find(strcmp(header,'sweep')) find(strcmp(header,'back_sweep')) find(strcmp(header,'w/l'))]);
% figure
% scatter3(wing_geom(:),)

% =======pdf of clusters====================
% general inline functions to get data filterd to cluster and associated
% GMM model
get_wing_geom = @(cluster_num,headerstr) wing_data(design_mat(idx==cluster_num,1),[find(strcmp(header,headerstr))]); % gets specific column data based on cluster
getGMModel = @(cluster_num) fitgmdist([get_wing_geom(cluster_num,'sweep'),get_wing_geom(cluster_num,'back_sweep'),get_wing_geom(cluster_num,'w/l')],1); % gets 3D GMM for cluster

% now plot
% first get GMM model for all clusters
for j = 1:num_cluster
    modelStruct(j).model = getGMModel(j);
end

geomcomb = [1 2 3; 1 3 2; 2 3 1]; %last colum is what is excluded
for j = 1:num_cluster
    figure
    wing_geom = [get_wing_geom(j,'sweep') get_wing_geom(j,'back_sweep') get_wing_geom(j,'w/l')];
    for i = 1:length(geom_str)
        subplot(2,3,i)

        gscatter(wing_geom(:,geomcomb(i,1)),wing_geom(:,geomcomb(i,2)),idx(idx==j))
        isolatedGMModel = getGMModel(j);
        
        hold on
        xlabel(geom_str{geomcomb(i,1)})
        ylabel(geom_str{geomcomb(i,2)})
        mu = isolatedGMModel.mu(geomcomb(i,3));
        tempvec = zeros(length(wing_geom(:,1)),1) + mu;
        pdfvec = [];
        count = 0;
        for k = 1:length(geom_str)
            if k==geomcomb(i,3)
                pdfvec(:,end+1) = tempvec;
            else
                count = count+1;
                pdfvec(:,end+1) = wing_geom(:,geomcomb(i,count));
            end
        end
        probcheck = pdf(isolatedGMModel,pdfvec);
        if i == 1
            get2DIsoProb = @(x,y) arrayfun(@(x0,y0) getNormalizedProb(x0,y0,mu,j,modelStruct),x,y);
%             pdfcheck = pdf(GMModel,[wing_geom(:,1),wing_geom(:,2),tempvec]);
        elseif i == 2
            get2DIsoProb = @(x,y) arrayfun(@(x0,y0) getNormalizedProb(x0,mu,y0,j,modelStruct),x,y);
%             pdfcheck = pdf(GMModel,[wing_geom(:,1),tempvec,wing_geom(:,3)]);
        elseif i ==3 
            get2DIsoProb = @(x,y) arrayfun(@(x0,y0) getNormalizedProb(mu,x0,y0,j,modelStruct),x,y);
%             pdfcheck = pdf(GMModel,[tempvec,wing_geom(:,2),wing_geom(:,3)]);
        end
        g = gca;
        fcontour(get2DIsoProb,[g.XLim g.YLim])
        colorbar
        hold off
        subplot(2,3,i+3)
        fsurf(get2DIsoProb,[g.XLim g.YLim])
        xlabel(geom_str{geomcomb(i,1)})
        ylabel(geom_str{geomcomb(i,2)})
    end
    sgtitle(['Cluster ' num2str(j)]) 
end

% ======== regression tables============
% all 250 flights
get_flinfo = @(desnum,flnum) load([path_flinfo 'flinfo' num2str(desnum) '.mat']).flinfo{flnum};
wing_geom = wing_data(design_mat(:,1),[find(strcmp(header,'sweep')) find(strcmp(header,'back_sweep')) find(strcmp(header,'w/l'))]);
for i = 1:length(design_mat)
    
end


%% function repository
function normProb = getNormalizedProb(x,y,z,cluster_num,modelStruct)
    % changed to hidden function from inline function bc inline function
    % was taking a long time in contour plotting bc the fitgmdist() was
    % getting called several time for every point
    clusterprobsum = zeros(numel(x),1);
    for j = 1:length(modelStruct)
        % Probability that a point belongs to a cluster
        probLowerTail = cdf(modelStruct(j).model,[x,y,z]);
        % two-tailed prob
        nonnormClusterProb = zeros(size(probLowerTail));
        nonnormClusterProb(probLowerTail <= 0.5) = 2*probLowerTail(probLowerTail <= 0.5);
        nonnormClusterProb(probLowerTail > 0.5) = 2*(1-probLowerTail(probLowerTail > 0.5));
        clusterprobsum = clusterprobsum + nonnormClusterProb;
        if j == cluster_num
            cluster_num_prob = nonnormClusterProb;
        end
    end
    
    normProb = cluster_num_prob ./ clusterprobsum;
end

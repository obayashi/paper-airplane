clc, clear, close all;

path = 'C:\Users\balto\switchdrive\PaperPlane\Experiments\0413_50samples\';
fn = 'samples_50wings.mat';

data = load([path fn]).newmat;
header = data(1,:);
wing_data = cell2mat(data(2:end,:));

getinfo = @(index,str) wing_data(index,find(strcmp(header,str)));

matcheck = [];

for i = 1:50
    sweep = getinfo(i,'sweep');
    sweep_back = getinfo(i,'back_sweep');
    wid_len = getinfo(i,'w/l');
    newline = [sweep, sweep_back, wid_len,...
        getinfo(i,'root_chord') getinfo(i,'tip_chord') getinfo(i,'b_half')];
    
    
    sweep = deg2rad(sweep);
    sweep_back = deg2rad(sweep_back);
    Area_half = 10000;
    
    
    syms sw swb wol bhalf Ahalf
    
    %swb is less than or equal to 90
    A = bhalf/tan(pi/2-sw);
    if rad2deg(sweep_back)>90
        B = -1*bhalf*tan(swb-pi/2);
        ct = (bhalf*2)/wol-A;
    else
        B = bhalf*tan(pi/2-swb);
        ct = (bhalf*2)/wol-A-B;
    end
    
    
    A1 = 0.5*A*bhalf;
    A2 = bhalf*ct;
    A3 = 0.5*B*bhalf;
    eqn1 = Ahalf == A1+A2+A3;
    bhalf_solution = solve(eqn1,bhalf);
    %     bhalf_str = char(bhalf_solution);
    A = bhalf_solution/tan(pi/2-sw);
    if rad2deg(sweep_back)>90
        B = -1*bhalf_solution*tan(swb-pi/2);
        ct = (bhalf_solution*2)/wol-A;
    else
        B = bhalf_solution*tan(pi/2-swb);
        ct = (bhalf_solution*2)/wol-A-B;
    end
    
    cr = A+B+ct;
    bhalf_ex1 = double(subs(bhalf_solution,[Ahalf,sw,swb,wol],[Area_half,sweep,sweep_back,wid_len]));
    ct_ex1 = double(subs(ct,[Ahalf,sw,swb,wol],[Area_half,sweep,sweep_back,wid_len]));
    cr_ex1 = double(subs(cr,[Ahalf,sw,swb,wol],[Area_half,sweep,sweep_back,wid_len]));
    
    newline = [newline max(cr_ex1) max(ct_ex1) max(bhalf_ex1)];
    matcheck(end+1,:) = newline;
end
matcheck = [matcheck matcheck(:,4)-matcheck(:,7) matcheck(:,5)-matcheck(:,8) matcheck(:,6)-matcheck(:,9)];


clc, clear, close all;

path = 'C:\Users\balto\switchdrive\PaperPlane\Wing\out_0413\';

mat_name = 'data\0413_random2.mat';
num_design = 52;

outmat = load([path mat_name]).outmat;

%find range of parameters with strongest silouettes, wol, sweep, backsweep

idx_wol = 17;
idx_sweep = 13;
idx_bsweep = 14;

outmat = cell2mat(outmat(2:end,:));
range_wol = [min(outmat(:,idx_wol)) max(outmat(:,idx_wol))];
range_sweep = [min(outmat(:,idx_sweep)) max(outmat(:,idx_sweep))];
range_bsweep = [min(outmat(:,idx_bsweep)) max(outmat(:,idx_bsweep))];

cont = 1;
iter = 1;
while cont
lhs_para = lhsdesign(num_design,3); % p = 3 for 3 parameters
% min+(max-min).*xcol
lhs_para = [min(range_wol)+(max(range_wol)-min(range_wol)).*lhs_para(:,1)...
    min(range_sweep)+(max(range_sweep)-min(range_sweep)).*lhs_para(:,2)...
    min(range_bsweep)+(max(range_bsweep)-min(range_bsweep)).*lhs_para(:,3)];

[norm_wol, mu_wol, sigma_wol] = featureNormalize(outmat(:,idx_wol));
[norm_sweep, mu_sweep, sigma_sweep] = featureNormalize(outmat(:,idx_sweep));
[norm_bsweep, mu_bsweep, sigma_bsweep] = featureNormalize(outmat(:,idx_bsweep));

lhs_para_norm = [(lhs_para(:,1)-mu_wol)./sigma_wol ...
    (lhs_para(:,2)-mu_sweep)./sigma_sweep ...
    (lhs_para(:,3)-mu_bsweep)./sigma_bsweep];

sampled_wings = [];
for i = 1:size(lhs_para_norm,1)
    RMSE_vec = [];
    for j = 1:size(outmat,1)
        V1 = lhs_para_norm(i,:);
        V2 = [norm_wol(j) norm_sweep(j) norm_bsweep(j)];
        RMSE_vec(j) = get_rmse(V1,V2);
    end
    [minval minidx] = min(RMSE_vec);
    sampled_wings(i,:) = [minidx minval];
end

uq_vals = unique(sampled_wings(:,1));
len_uq = length(uq_vals);
if len_uq == 50
    cont = 0;
end
iter = iter + 1;
if mod(iter,100) == 0
    disp(iter)
end
end





%% function repo
function [X_norm, mu, sigma] = featureNormalize(X)
mu = mean(X);
sigma = std(X);
X_norm = (X-mu)./sigma;
end

function [RMSE] = get_rmse(V1,V2)
RMSE = sqrt(mean((V1-V2).^2));
end


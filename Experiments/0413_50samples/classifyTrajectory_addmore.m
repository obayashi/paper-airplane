clc, clear, close all;
addpath(genpath('C:\Users\balto\switchdrive\PaperPlane\matlab'))

path_flinfo = 'C:\Users\balto\switchdrive\PaperPlane\Experiments\0413_50samples\FlightInfo_addmore\';
path_data = 'C:\Users\balto\switchdrive\PaperPlane\Experiments\0413_50samples\';

wing_data = load([path_data 'samples_50wings.mat']).newmat;
header = wing_data(1,:);
% wing_data = cell2mat(wing_data(2:end,:));
wing_data = load([path_data 'wing_data_addmore.mat']).wing_data;
[temp,pallette] = getcolor('b');

num_designs = size(wing_data,1);
num_flights = 5;
km_mat = []; % num of coefficients
design_mat = [];
rng(1)

%===partition training and test===
data = 1:num_designs;
holdout = 0;

if holdout == 0 % no partitioning
    %     cv = cvpartition(length(data),'HoldOut',holdoutkeep);
    %     idx_test = cv.test;
    %     dataTest  = data(idx_test)
    dataTest = 1:num_designs;
    dataTrain = 1:num_designs;
else
    
    if holdout == 0.1
        testidx = [9 17 18 28 47];
    elseif holdout == 0.15
        testidx = [8 9 17 18 28 40 47];
    elseif holdout == 0.2
        testidx = [8 9 16 17 18 28 38 40 47 50];
    elseif holdout == 0.25
        testidx = [2 8 9 16 17 18 28 38 39 40 47 50];
    elseif holdout == 0.3
        testidx = [2 8 9 16 17 18 21 28 34 38 39 40 42 47 50];
    end
    dataTest = testidx;
    temp = ones(1,num_designs);
    temp(dataTest) = 0;
    dataTrain = data(logical(temp));
%     cv = cvpartition(length(data),'HoldOut',holdout);
%     idx_test = cv.test;
%     dataTrain = data(~idx_test);
%     dataTest  = data(idx_test);
end



%===make main data matrix===
for i = 1:length(dataTrain)
    flinfo = load([path_flinfo 'flinfo' num2str(dataTrain(i)) '.mat']).flinfo;
    for j = 1:length(flinfo)
        flight = flinfo{j};
        geom_para = wing_data(dataTrain(i),[find(strcmp(header,'sweep')) find(strcmp(header,'back_sweep')) find(strcmp(header,'w/l'))]);
        dist_para = flight.landing_spot(2);
        centdev_para = flight.landing_spot(1);
        fltime_para = flight.fl_time;
        
        km_mat(end+1,:) = [flight.tstat flight.estimates geom_para dist_para centdev_para fltime_para];
        design_mat(end+1,:) = [dataTrain(i) j];
    end
end
km_header = {'tstat5','tstat4','tstat3','tstat2','tstat1','tstat0',...
    'coeff5','coeff4','coeff3','coeff2','coeff1','coeff0',...
    'sweep','back_sweep','w/l','dist','centdev','fltime'};





% === add distance to columns  ===
choose_cols = [7 8 9 10 16];

%===make matrix for kmeans again===
X = makeKmeansMatrix(km_mat,choose_cols,'all');

%===FInd number of clusters using evalclusters, which supports, the Calinski-Harabasz criterion
clust = zeros(size(X,1),6);
best_totsumd = zeros(6,1);
for i=1:6
    %     [clust(:,i),C,sumd] = kmeans(X,i,'emptyaction','singleton',...
    %         'replicate',10);
    [clust(:,i),C,sumd] = kmeans(X,i,'Distance','cityblock','replicate',10);
    
    best_totsumd(i) = sum(sumd);
end
eva = evalclusters(X,clust,'CalinskiHarabasz');
if  holdout>0 % for partitioned data, force optimal k to be 3
    optimalk = 3;
else
    optimalk = eva.OptimalK;
end

% temp force optimalk to 3====
optimalk = 3;

idx = clust(:,optimalk);
fprintf('Optimal K: %1.0f',optimalk)
figure
plot(1:6,best_totsumd,'o-')
hold on
plot([optimalk optimalk],[min(best_totsumd) max(best_totsumd)],'r--')
xlabel('number of clusters')
ylabel('best total sum of distances of centroid to each point')
grid on

%=====reorder the cluster based on median distance: shortest cluster should
%be cluster 1, etc ====
get_flightdata = @(flidx,headerstr) km_mat(flidx,find(strcmp(km_header,headerstr)));
medianvec = [];
for i = 1:optimalk
    medianvec(end+1) = quantile(get_flightdata(idx==i,'dist'),.5);
end
[temp sortedidx] = sort(medianvec);
for i = 1:optimalk
    idx(idx==sortedidx(i)) = optimalk+i;
end
idx = idx-optimalk;

%=====plot behavior statistics=====
figure
boxchart(idx,get_flightdata(1:length(idx),'dist'))
xlabel('Cluster')
ylabel('Distance')
set(gca, 'XTick', 1:optimalk)
grid on
%
% legendstr = {};
% figure
% for i = 1:optimalk
% histogram(get_flightdata(idx==i,'dist'),'BinWidth',0.08)
% legendstr{end+1} = num2str(i);
% hold on
% end
% legend(legendstr)


%=====plot clustered trajectories=====
% plotClusteredTrajectory(optimalk,80,path_flinfo,idx,design_mat,1:(length(dataTrain)*num_flights),NaN,0)

%=====plot clustered geometries disregarding probability=====
% plotClusteredWings(design_mat,optimalk,idx,wing_data,header,NaN,0,length(dataTrain))

%====find optimal k for each cluster====
get_wing_geom = @(cluster_num,headerstr) wing_data(design_mat(idx==cluster_num,1),[find(strcmp(header,headerstr))]); % gets specific column data based on cluster
subidx = {};
suboptimalk = [];
max_subgroups = 3;
trajincluster = hist(idx,1:max(idx));
[val,clusterwithmany] = max(trajincluster);
for i = 1:optimalk
    X = [get_wing_geom(i,'sweep') get_wing_geom(i,'back_sweep') get_wing_geom(i,'w/l')];
    clust = zeros(size(X,1),max_subgroups);
    %     if i == clusterwithmany
    %         max_subgroups = 3;
    %     end
    for j=1:max_subgroups
        [clust(:,j),C,sumd] = kmeans(X,j,'emptyaction','singleton',...
            'replicate',10);
    end
    if max_subgroups>1
        eva = evalclusters(X,clust,'silhouette');
        %     eva = evalclusters(X,clust,'CalinskiHarabasz');
        %     if i == clusterwithmany
        %         suboptimalk(i) = 4;
        %     else
        
        suboptimalk(i) = eva.OptimalK;
    else
        suboptimalk(i) = 1;
    end
    %     end
    subidx{i} = clust(:,suboptimalk(i));
end
suboptimalk

%===make GMM===
getGMModel = @(cluster_num,subcluster_num) fitgmdist([get_wing_geom(cluster_num,'sweep'),get_wing_geom(cluster_num,'back_sweep'),get_wing_geom(cluster_num,'w/l')],subcluster_num,'Start',subidx{cluster_num}); % gets 3D GMM for cluster
getGMModel_error = @(cluster_num,subcluster_num) fitgmdist([get_wing_geom(cluster_num,'sweep'),get_wing_geom(cluster_num,'back_sweep'),get_wing_geom(cluster_num,'w/l')],subcluster_num,'SharedCovariance',true,'Start',subidx{cluster_num}); % gets 3D GMM for cluster
get_wing_geom_direct = @(des_num,headerstr) wing_data(des_num,[find(strcmp(header,headerstr))]); % gets specific column data based on cluster
% first get GMM model for all clusters
for j = 1:optimalk
    try
        
        
        modelStruct(j).model = getGMModel(j,suboptimalk(j));
    catch
        modelStruct(j).model = getGMModel_error(j,suboptimalk(j));
    end
end

%=====get probabilities=====
getdesnum = @(flnum,cluster_num,subcluster_num) design_mat(flnum(subidx{cluster_num}==subcluster_num),1);
getGMModel_indiv = @(desnum) fitgmdist([get_wing_geom_direct(desnum,'sweep'),...
    get_wing_geom_direct(desnum,'back_sweep'),get_wing_geom_direct(desnum,'w/l')],1);
probmat = zeros(length(dataTest),optimalk);
for i = 1:optimalk
    for j = 1:length(dataTest)
        qp = [get_wing_geom_direct(dataTest(j),'sweep'),...
            get_wing_geom_direct(dataTest(j),'back_sweep'),...
            get_wing_geom_direct(dataTest(j),'w/l')];
        probmat(j,i) = getProbability(qp,modelStruct(i).model);
    end
end

%=====normalize probabilities=====
probsum = sum(probmat,2);
probmat = bsxfun(@rdivide,probmat,probsum(:)); % divide each row by corresponding element in vector
probmat = [dataTest' probmat];

behavior = [0.33 0.33 0.33];

RMSE_vec = [];
for i = 1:num_designs
RMSE_vec(i) = get_rmse(probmat(i,2:end),behavior);

end
[sorted ranking] = sort(RMSE_vec);

function [RMSE] = get_rmse(V1,V2)
RMSE = sqrt(mean((V1-V2).^2));
end


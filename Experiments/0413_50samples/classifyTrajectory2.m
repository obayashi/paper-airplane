clc, clear, close all;
addpath(genpath('C:\Users\balto\switchdrive\PaperPlane\matlab'))

path_flinfo = 'C:\Users\balto\switchdrive\PaperPlane\Experiments\0413_50samples\FlightInfo\';
path_data = 'C:\Users\balto\switchdrive\PaperPlane\Experiments\0413_50samples\';

wing_data = load([path_data 'samples_50wings.mat']).newmat;
header = wing_data(1,:);
wing_data = cell2mat(wing_data(2:end,:));
[temp,pallette] = getcolor('b');

num_designs = 50;
num_flights = 5;
km_mat = []; % num of coefficients
design_mat = [];
rng(1)

%===partition training and test===
data = 1:num_designs;
holdout = 0.0;

if holdout == 0 % no partitioning
    %     cv = cvpartition(length(data),'HoldOut',holdoutkeep);
    %     idx_test = cv.test;
    %     dataTest  = data(idx_test)
    dataTest = 1:num_designs;
    dataTrain = 1:num_designs;
else
    cv = cvpartition(length(data),'HoldOut',holdout);
    idx_test = cv.test;
    dataTrain = data(~idx_test);
    dataTest  = data(idx_test)
end



%===make main data matrix===
for i = 1:length(dataTrain)
    flinfo = load([path_flinfo 'flinfo' num2str(dataTrain(i)) '.mat']).flinfo;
    for j = 1:length(flinfo)
        flight = flinfo{j};
        geom_para = wing_data(dataTrain(i),[find(strcmp(header,'sweep')) find(strcmp(header,'back_sweep')) find(strcmp(header,'w/l'))]);
        dist_para = flight.landing_spot(2);
        centdev_para = flight.landing_spot(1);
        fltime_para = flight.fl_time;
       
        km_mat(end+1,:) = [flight.tstat flight.estimates geom_para dist_para centdev_para fltime_para];
        design_mat(end+1,:) = [dataTrain(i) j];
    end
end
km_header = {'tstat5','tstat4','tstat3','tstat2','tstat1','tstat0',...
    'coeff5','coeff4','coeff3','coeff2','coeff1','coeff0',...
    'sweep','back_sweep','w/l','dist','centdev','fltime'};



%===guess number of cluster of trajectory = 3 from intuition===
knum_guess = 3;

%===from exploration, coefficients visually clusters better than tstats===

%===make all combinations of 3 or more coefficients/tstat===
num_coeffs = 6;
combcell = {};
for i = 3:num_coeffs
    C = nchoosek(1:6,i);
    [rows cols] = size(C);
    for j = 1:rows
        combcell{end+1} = C(j,:);
    end
end

%===decide which combinations are best.run kmeans for all combinations of coefficients and find score for
%each. ===
tstat_idx = [1:6];
coeff_idx = [7:12];
geom_idx = [13:15];
score = [];
choose_cols = coeff_idx;
for i = 1:length(combcell)
    X = makeKmeansMatrix(km_mat,combcell{i}+(choose_cols(1)-1),'all');
    [idx,C,sumd] = kmeans(X,knum_guess,'Distance','cityblock',...
        'Replicates',10);
    silscore = silhouette(X,idx);
    newscore = (silscore + ones(length(silscore),1))./2;
    score(end+1) = sum(newscore)/length(newscore);
end
[val maxidx] = max(score);
fprintf('Best silhouette score: %0.4f\n',val)
fprintf('Column indices from km_mat:')
if holdout>0 %for partitioned data, force combination to be 7:10
    choose_cols = coeff_idx(1:4);
else
    choose_cols = combcell{maxidx}+(choose_cols(1)-1)
end

% === add distance to columns  ===
choose_cols = [choose_cols 16];

%===make matrix for kmeans again===
X = makeKmeansMatrix(km_mat,choose_cols,'all');

%===FInd number of clusters using evalclusters, which supports, the Calinski-Harabasz criterion
clust = zeros(size(X,1),6);
best_totsumd = zeros(6,1);
for i=1:6
    %     [clust(:,i),C,sumd] = kmeans(X,i,'emptyaction','singleton',...
    %         'replicate',10);
    [clust(:,i),C,sumd] = kmeans(X,i,'Distance','cityblock','replicate',10);
    
    best_totsumd(i) = sum(sumd);
end
eva = evalclusters(X,clust,'CalinskiHarabasz');
if  holdout>0 % for partitioned data, force optimal k to be 3
    optimalk = 3;
else
    optimalk = eva.OptimalK;
end

% temp force optimalk to 3====
optimalk = 3;

idx = clust(:,optimalk);
fprintf('Optimal K: %1.0f',optimalk)
figure
plot(1:6,best_totsumd,'o-')
hold on
plot([optimalk optimalk],[min(best_totsumd) max(best_totsumd)],'r--')
xlabel('number of clusters')
ylabel('best total sum of distances of centroid to each point')
grid on

%=====reorder the cluster based on median distance: shortest cluster should
%be cluster 1, etc ====
get_flightdata = @(flidx,headerstr) km_mat(flidx,find(strcmp(km_header,headerstr)));
medianvec = [];
for i = 1:optimalk
medianvec(end+1) = quantile(get_flightdata(idx==i,'dist'),.5);
end
[temp sortedidx] = sort(medianvec);
for i = 1:optimalk
idx(idx==sortedidx(i)) = optimalk+i;
end
idx = idx-optimalk;

%=====plot behavior statistics=====
h=figure;
boxchart(idx,get_flightdata(1:length(idx),'dist'),'BoxWidth',0.8)
xlabel('Cluster')
ylabel('Distance','FontSize',12)
set(gca, 'XTick', 1:optimalk,'YTick',0:6,'FontSize',12,'YAxisLocation','right')
ylim([0 6])
ytickangle(90)
grid on
box on
set(h,'Position',[100 100 200 480])


% legendstr = {};
% figure
% for i = 1:optimalk
% histogram(get_flightdata(idx==i,'dist'),'BinWidth',0.08)
% legendstr{end+1} = num2str(i);
% hold on
% end
% legend(legendstr)


%=====plot clustered trajectories=====
plotClusteredTrajectory(optimalk,80,path_flinfo,idx,design_mat,1:(length(dataTrain)*num_flights),NaN,0)

%=====plot clustered geometries disregarding probability=====
plotClusteredWings(design_mat,optimalk,idx,wing_data,header,NaN,0,length(dataTrain))

%====find optimal k for each cluster====
get_wing_geom = @(cluster_num,headerstr) wing_data(design_mat(idx==cluster_num,1),[find(strcmp(header,headerstr))]); % gets specific column data based on cluster
subidx = {};
suboptimalk = [];
max_subgroups = 3;
trajincluster = hist(idx,1:max(idx));
[val,clusterwithmany] = max(trajincluster);
for i = 1:optimalk
    X = [get_wing_geom(i,'sweep') get_wing_geom(i,'back_sweep') get_wing_geom(i,'w/l')];
    clust = zeros(size(X,1),max_subgroups);
%     if i == clusterwithmany
%         max_subgroups = 3;
%     end
    for j=1:max_subgroups
        [clust(:,j),C,sumd] = kmeans(X,j,'emptyaction','singleton',...
            'replicate',10);
    end
    if max_subgroups>1
        eva = evalclusters(X,clust,'silhouette');
        %     eva = evalclusters(X,clust,'CalinskiHarabasz');
    %     if i == clusterwithmany
    %         suboptimalk(i) = 4;
    %     else

            suboptimalk(i) = eva.OptimalK;
    else 
        suboptimalk(i) = 1;
    end
%     end
    subidx{i} = clust(:,suboptimalk(i));
end
suboptimalk

%===make GMM===
getGMModel = @(cluster_num,subcluster_num) fitgmdist([get_wing_geom(cluster_num,'sweep'),get_wing_geom(cluster_num,'back_sweep'),get_wing_geom(cluster_num,'w/l')],subcluster_num,'Start',subidx{cluster_num}); % gets 3D GMM for cluster
getGMModel_error = @(cluster_num,subcluster_num) fitgmdist([get_wing_geom(cluster_num,'sweep'),get_wing_geom(cluster_num,'back_sweep'),get_wing_geom(cluster_num,'w/l')],subcluster_num,'SharedCovariance',true,'Start',subidx{cluster_num}); % gets 3D GMM for cluster
get_wing_geom_direct = @(des_num,headerstr) wing_data(des_num,[find(strcmp(header,headerstr))]); % gets specific column data based on cluster
% first get GMM model for all clusters
for j = 1:optimalk
    try
%         %temp 1D proof of cdf ok/nok of points for BO
%         GMModel1d = fitgmdist(get_wing_geom(j,'sweep'),1);
%         mu = GMModel1d.mu;
%         std = sqrt(GMModel1d.Sigma);
%         pdfcheck = pdf(GMModel1d,get_wing_geom(j,'sweep'));
%         figure
%         plot(get_wing_geom(j,'sweep'),pdfcheck,'o')
%         hold on
%         plot([mu mu],[0 0.022],'r-')
%         plot([mu+std mu+std],[0 0.022],'b-')
%         plot([mu-std mu-std],[0 0.022],'b-')
%         plot([mu+2*std mu+2*std],[0 0.022],'g-')
%         plot([mu-2*std mu-2*std],[0 0.022],'g-')
%         trypoints = linspace(-20,100)';
%         prob = getProbability(trypoints,GMModel1d);
%         include = 0.9545;
%         plot(trypoints(prob>(1-include)),zeros(size(trypoints(prob>(1-include)))),'bo','MarkerFaceColor','b')
%         plot(trypoints(prob<(1-include)),zeros(size(trypoints(prob<(1-include)))),'ro','MarkerFaceColor','r')
%         %temp endfille
        
        
        modelStruct(j).model = getGMModel(j,suboptimalk(j));
    catch
        modelStruct(j).model = getGMModel_error(j,suboptimalk(j));
    end 
end

%=====get probabilities=====
getdesnum = @(flnum,cluster_num,subcluster_num) design_mat(flnum(subidx{cluster_num}==subcluster_num),1);
getGMModel_indiv = @(desnum) fitgmdist([get_wing_geom_direct(desnum,'sweep'),...
    get_wing_geom_direct(desnum,'back_sweep'),get_wing_geom_direct(desnum,'w/l')],1);
probmat = zeros(length(dataTest),optimalk);
for i = 1:optimalk
    for j = 1:length(dataTest)
        qp = [get_wing_geom_direct(dataTest(j),'sweep'),...
        get_wing_geom_direct(dataTest(j),'back_sweep'),...
        get_wing_geom_direct(dataTest(j),'w/l')];
%         qp = [55.3203 115.6595 0.6852];
%         flnum = find(idx==i);
%         prob = [];
%         for k = 1:suboptimalk(i)
%             desnum = getdesnum(flnum,i,k);
%             model_indiv = getGMModel_indiv(desnum);
            probmat(j,i) = getProbability(qp,modelStruct(i).model);
%         end
%         probmat(j,i) = max(prob);
    end
end

%=====normalize probabilities=====
probsum = sum(probmat,2);
probmat = bsxfun(@rdivide,probmat,probsum(:)); % divide each row by corresponding element in vector
probmat = [dataTest' probmat];

geom_str = {'sweep [deg]','back sweep [deg]','width/length'};
geomcomb = [1 2 3; 1 3 2; 2 3 1]; %last colum is what is excluded
for j = 1:optimalk
    figure
    wing_geom = [get_wing_geom(j,'sweep') get_wing_geom(j,'back_sweep') get_wing_geom(j,'w/l')];
    subplot(2,1,1)
    gscatter3(wing_geom(:,1),wing_geom(:,2),wing_geom(:,3),subidx{j})
    xlabel(geom_str{1})
    ylabel(geom_str{2})
    zlabel(geom_str{3})
    subplot(2,1,2)
    gscatter3(wing_geom(:,1),wing_geom(:,2),wing_geom(:,3),subidx{j})
    g = gca;
    isolatedGMModel = modelStruct(j).model;
    
    hold on
    xlabel(geom_str{1})
    ylabel(geom_str{2})
    zlabel(geom_str{3})
    sgtitle(['Cluster ' num2str(j)])
    sigdims = ndims(isolatedGMModel.Sigma);
    for i = 1:suboptimalk(j)
        if or(sigdims<3,suboptimalk(j)==1)
            sigplot = isolatedGMModel.Sigma;
        else
            sigplot = isolatedGMModel.Sigma(:,:,i);
        end
%         [elv,elf] = ellipsedata3(sigplot,isolatedGMModel.mu(i,:),20,1.5);
        plot_gaussian_ellipsoid(isolatedGMModel.mu(i,:),sigplot,1.8,20,g,pallette{j})
        
    end
    if holdout>0
        plot3(get_wing_geom_direct(dataTest,'sweep'),...
            get_wing_geom_direct(dataTest,'back_sweep'),...
            get_wing_geom_direct(dataTest,'w/l'),'ko','MarkerFaceColor','k')
    end
    
end


%% ======== regression tables============
% all 250 flights
get_flinfo = @(desnum,flnum) load([path_flinfo 'flinfo' num2str(desnum) '.mat']).flinfo{flnum};
wing_geom = wing_data(design_mat(:,1),[find(strcmp(header,'sweep')) find(strcmp(header,'back_sweep')) find(strcmp(header,'w/l'))]);
regall = [wing_geom km_mat(:,16:18) abs(km_mat(:,17))];
regheader = {'sweep','back_sweep','w/l','dist','centdev','fltime','abs(centdev)'};
tableall = array2table(regall);
tableall.Properties.VariableNames(1:end) = regheader;
%cluster1
reg1 = [wing_data(design_mat(idx==1,1),[find(strcmp(header,'sweep')) find(strcmp(header,'back_sweep')) find(strcmp(header,'w/l'))])...
    km_mat(idx==1,16:18) abs(km_mat(idx==1,17))];
table1 = array2table(reg1);
table1.Properties.VariableNames(1:end) = regheader;
%cluster2
reg2 = [wing_data(design_mat(idx==2,1),[find(strcmp(header,'sweep')) find(strcmp(header,'back_sweep')) find(strcmp(header,'w/l'))])...
    km_mat(idx==2,16:18) abs(km_mat(idx==2,17))];
table2 = array2table(reg2);
table2.Properties.VariableNames(1:end) = regheader;
%cluster3
reg3 = [wing_data(design_mat(idx==3,1),[find(strcmp(header,'sweep')) find(strcmp(header,'back_sweep')) find(strcmp(header,'w/l'))])...
    km_mat(idx==3,16:18) abs(km_mat(idx==3,17))];
table3 = array2table(reg3);
table3.Properties.VariableNames(1:end) = regheader;

% function repository
% 
% function normProb = getNormalizedProb(x,y,z,cluster_num,modelStruct)
% % changed to hidden function from inline function bc inline function
% % was taking a long time in contour plotting bc the fitgmdist() was
% % getting called several time for every point
% clusterprobsum = 0;
% for j = 1:length(modelStruct)
%     clusterprobsum = clusterprobsum + pdf(modelStruct(j).model,[x,y,z]);
% end
% normProb = pdf(modelStruct(cluster_num).model,[x,y,z])./ clusterprobsum;
% end



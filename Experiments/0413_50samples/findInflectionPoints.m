clc, clear, close all;

path_traj = 'C:\Users\balto\switchdrive\PaperPlane\Wing\out_0226\trajectory3\';
design_num = 6;
name_traj_file = [path_traj 'BO_0226_' num2str(design_num) '.mat'];

trajectory = load(name_traj_file).trajectory;
height = trajectory.height;
dist = trajectory.dist;
dist = dist(5:end-5);
height = height(5:end-5);

p = polyfit(dist,height,5);
x1 = linspace(dist(1),dist(end));
fitline = polyval(p,x1);

f = @(x)p(1)*x^5 + p(2)*x^4 + p(3)*x^3 + p(4)*x^2 + p(5)*x + p(6);
f2 = @(x)20*p(1)*x^3 + 12*p(2)*x^2 + 6*p(3)*x + 2*p(4);
f4 = @(x)120*p(1) + 24*p(2);

z=AllZeros(f2,dist(1),dist(end),100);
ip_idx = [];
if ~isempty(z)
    for i = 1:length(z)
        zy(i) = f(z(i));
        thy(i) = f4(z(i));
    end
    
    ip_threshold = 6e-6;
    ip_idx = find(abs(thy)>ip_threshold);
end


figure
plot(dist,height,'*')
hold on
plot(x1,fitline)
if isempty(ip_idx)
    ip = ip_idx;
else
    ip = z(ip_idx);
    plot(z(ip_idx),zy(ip_idx),'ro')
end
grid on
axis equal




%% function repo
function z=AllZeros(f,xmin,xmax,N)
% Inputs :
% f : function of one variable
% [xmin - xmax] : range where f is continuous containing zeros
% N : control of the minimum distance (xmax-xmin)/N between two zeros
if (nargin<4)
    N=100;
end
dx=(xmax-xmin)/N;
x2=xmin;
y2=f(x2);
z=[];
for i=1:N
    x1=x2;
    y1=y2;
    x2=xmin+i*dx;
    y2=f(x2);
    if (y1*y2<=0)                              % Rolle's theorem : one zeros (or more) present
        z=[z,fsolve(f,(x2*y1-x1*y2)/(y1-y2))]; % Linear approximation to guess the initial value in the [x1,x2] range.
    end
end
end







clc, clear, close all;

addpath(genpath('C:\Users\balto\switchdrive\PaperPlane\matlab'))

[temp,pallette] = getcolor('b');

GMModel = load('modelStruct16_0517_1259.mat').modelStruct(3).model;
% qp = [28.3030,95.6790,0.2569];
% qp = [22.0776,105.5562,1.3020];

qp = [22.1251,81.6037,1.2518];

probout = getProbability(qp,GMModel);

num_subcluster = size(GMModel.mu,1);

figure
g = gca;
for i = 1:num_subcluster
    mu = GMModel.mu(i,:);
    if and(ndims(GMModel.Sigma)<3,num_subcluster>1)
        Sigma = GMModel.Sigma;
    else
        Sigma = GMModel.Sigma(:,:,i);
    end
    plot_gaussian_ellipsoid(mu,Sigma,1.8,20,g,pallette{1})
end
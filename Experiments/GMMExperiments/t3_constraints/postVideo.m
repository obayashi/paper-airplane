clc, clear, close all;
addpath(genpath('G:\.shortcut-targets-by-id\1wYXDAMCh59hhAOMrpcbdbmdNo1mZvuMP\CREATE Lab\Nana PhD\2022 Paper Airplane\Experiment\matlab'))
path_video = 'G:\.shortcut-targets-by-id\1wYXDAMCh59hhAOMrpcbdbmdNo1mZvuMP\CREATE Lab\Nana PhD\2022 Paper Airplane\Experiment\Video\';

video_folder = '0521_t5_noGMM'; % change

wing_mat = load(findYoungestFile([pwd '\WingData'],'mat')).wing_data;
wing_data = cell2mat(wing_mat(2:end,:));

mat_idx = @(whichmat,headerstr) find(strcmp(headerstr,whichmat(1,:)));

design_num = wing_data(end,mat_idx(wing_mat,'idx_orig'));

% process video, get raw trajectory
getTrajectory(path_video,design_num,video_folder,pwd)
disp('getTrajectory DONE')

% get flight data
% !!!make sure to do camera calibration and check minimum distance
% beforehand!!!
getFlightData(design_num,pwd)
disp('getFlightData DONE')

%make flight datasheet
makeFlightInfoPlot(design_num,pwd)
disp('makeFlightInfoPlot DONE')

% add to the existing km_mat/design_mat and save as new
[km_mat,design_mat] = make_kmmat(design_num,pwd,1);

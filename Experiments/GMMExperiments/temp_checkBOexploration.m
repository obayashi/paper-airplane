addpath(genpath('G:\.shortcut-targets-by-id\1wYXDAMCh59hhAOMrpcbdbmdNo1mZvuMP\CREATE Lab\Nana PhD\2022 Paper Airplane\Experiment\matlab'))

wing_mat = load(findYoungestFile([pwd '\WingData'],'mat')).wing_data;
wing_data = cell2mat(wing_mat(2:end,:));
km_mat = load(findYoungestFile([pwd '\ClusterData'],'mat')).km_mat;
km_data = cell2mat(km_mat(2:end,:));

target = 5;
num_seed = 7;
mat_idx = @(whichmat,headerstr) find(strcmp(headerstr,whichmat(1,:)));

dist = km_data(:,mat_idx(km_mat,'dist'));
dist = reshape(dist,5,size(wing_data,1));
avg_dist = mean(dist,1);
[maxdist, maxidx] = sort(abs(avg_dist-target));


figure 
subplot(4,1,1)
iter = 1:size(wing_data,1);
sw_idx = mat_idx(wing_mat,'sweep');
swb_idx = mat_idx(wing_mat,'back_sweep');
wol_idx = mat_idx(wing_mat,'w/l');
plot(iter,wing_data(:,sw_idx),'o-')
ylabel('sweep')
hold on 
plot([num_seed num_seed],[0 60],'r-')
plot([1 max(iter)],[wing_data(maxidx(1),sw_idx) wing_data(maxidx(1),sw_idx)],'k-')
plot([maxidx(1) maxidx(1)],[0 60],'k:')
plot([maxidx(2) maxidx(2)],[0 60],'b:')
plot([1 max(iter)],[wing_data(maxidx(2),sw_idx) wing_data(maxidx(2),sw_idx)],'b-')
subplot(4,1,2)
plot(iter,wing_data(:,swb_idx),'o-')
ylabel('back sweep')
hold on 
plot([num_seed num_seed],[0 120],'r-')
plot([maxidx(1) maxidx(1)],[0 120],'k:')
plot([maxidx(2) maxidx(2)],[0 120],'b:')
plot([1 max(iter)],[wing_data(maxidx(1),swb_idx) wing_data(maxidx(1),swb_idx)],'k-')
plot([1 max(iter)],[wing_data(maxidx(2),swb_idx) wing_data(maxidx(2),swb_idx)],'b-')
subplot(4,1,3)
plot(iter,wing_data(:,wol_idx),'o-')
ylabel('width/length')
hold on 
plot([num_seed num_seed],[0 1.8],'r-')
plot([maxidx(1) maxidx(1)],[0 1.8],'k:')
plot([maxidx(2) maxidx(2)],[0 1.8],'b:')
plot([1 max(iter)],[wing_data(maxidx(1),wol_idx) wing_data(maxidx(1),wol_idx)],'k-')
plot([1 max(iter)],[wing_data(maxidx(2),wol_idx) wing_data(maxidx(2),wol_idx)],'b-')
subplot(4,1,4)
plot(iter,dist)
hold on
plot(iter,avg_dist,'k-','LineWidth',2)
plot([num_seed num_seed],[0 5],'r-')
plot([maxidx(1) maxidx(1)],[0 5],'k:')
plot([maxidx(2) maxidx(2)],[0 5],'b:')
plot([1 max(iter)],[target target],'m-')
ylabel('distance [m]')



% TEMP plot box chart
km_mat = load(findYoungestFile([pwd '\ClusterData'],'mat')).km_mat;
km_data = cell2mat(km_mat(2:end,:));
idx = load(findYoungestFile([pwd '\GMModels'],'mat')).idx;

mat_idx = @(whichmat,headerstr) find(strcmp(headerstr,whichmat(1,:)));

figure
boxchart(idx,km_data(:,mat_idx(km_mat,'dist')));
xlabel('Cluster')
ylabel('Distance')
set(gca, 'XTick', 1:3)
grid on
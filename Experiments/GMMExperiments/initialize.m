% Create the initial seed

clc, clear, close all;

addpath(genpath('G:\.shortcut-targets-by-id\1wYXDAMCh59hhAOMrpcbdbmdNo1mZvuMP\CREATE Lab\Nana PhD\2022 Paper Airplane\Experiment\matlab'))
% paths old
path_data = 'G:\.shortcut-targets-by-id\1wYXDAMCh59hhAOMrpcbdbmdNo1mZvuMP\CREATE Lab\Nana PhD\2022 Paper Airplane\Experiment\0413_50samples\';
current = pwd;


% user inputs
%target3
% num_seed = 10; % arbitrary
% rng(1)
%target5
num_seed = 7; % 7 seems to be minimum number of seeds that is able to create model
rng(12)

% make matrix
wing_data = load([path_data 'samples_50wings.mat']).newmat;
header = wing_data(1,:);
wing_data = cell2mat(wing_data(2:end,:));
wing_data(:,find(strcmp(header,'idx_orig'))) = 1:50;
seedidx = sort(randperm(size(wing_data,1),num_seed));
wing_data = wing_data(seedidx,:);
wing_data = [header; num2cell(wing_data)];


%make necessary folders
folderlist = {'ClusterData','GMModels','WingData'};
for i = 1:length(folderlist)
   folder = [current '\' folderlist{i}];
    if not(isfolder(folder))
        mkdir(folder)
    end 
end


% save wing data
time = datestr(now,'mmdd_HHMM');
filename = sprintf('wing_data_%s.mat',time);
prompt = "Do you want to save wing_data? y/n:";
txt = input(prompt,"s");
if strcmp(txt,'y')
    save([pwd '\WingData\' filename],'wing_data')
end

% make folders if don't exist and copy flight data
folderlist = {'FlightInfo','Datasheet','Dxf','RawTrajectory'};
fn_before = {'flinfo','wing','','flinfo'};
fn_after = {'.mat','.png','.dxf','.mat'};
for i = 1:length(folderlist)
    folder = [current '\' folderlist{i}];
    if not(isfolder(folder))
        mkdir(folder)
    end
    
    for j = 1:num_seed
        if strcmp(folderlist{i},'Dxf')
            
            source = [path_data 'dxf_50samples\' fn_before{i} num2str(seedidx(j)) fn_after{i}];
        else
            source = [path_data folderlist{i} '\' fn_before{i} num2str(seedidx(j)) fn_after{i}];
        end
        copyfile(source,folder)
    end
end

[km_mat,design_mat] = make_kmmat(seedidx,pwd,0);







clc, clear, close all;
addpath(genpath('C:\Users\balto\switchdrive\PaperPlane\matlab'))
path_video = 'C:\Users\balto\switchdrive\PaperPlane\Experiments\0524_VennDiagram\0524_venndiagram_Videos\';

video_folder = ''; % change
current = pwd;
% vennwings = [28 48 51 52 54 71 73];

wing_mat = load('WingData/wing_data_venn.mat').wing_data;
wing_header = wing_mat(1,:);
wing_data = cell2mat(wing_mat(2:end,:));

mat_idx = @(whichmat,headerstr) find(strcmp(headerstr,whichmat(1,:)));

design_num = wing_data(:,mat_idx(wing_mat,'idx_orig'));

for i = 1:length(design_num)

% process video, get raw trajectory
getTrajectory(path_video,design_num(i),video_folder,pwd)

% get flight data
% !!!make sure to do camera calibration and check minimum distance
% beforehand!!!
getFlightData(design_num(i),current)

%make flight datasheet
makeFlightInfoPlot_venn(design_num(i),current)
end

% add to the existing km_mat/design_mat and save as new
% [km_mat,design_mat] = make_kmmat(design_num,current,1);

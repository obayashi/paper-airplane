% get GMM model and probability for each design with equal number of
% flights between behaviors

clc, clear, close all;
addpath(genpath('C:\Users\balto\switchdrive\PaperPlane\matlab'))

path_flinfo = 'C:\Users\balto\switchdrive\PaperPlane\Experiments\0413_50samples\FlightInfo_addmore\';
path_data = 'C:\Users\balto\switchdrive\PaperPlane\Experiments\0413_50samples\';
venn_path = 'C:\Users\balto\switchdrive\PaperPlane\Experiments\0524_VennDiagram\';

wing_data = load([path_data 'samples_50wings.mat']).newmat;
header = wing_data(1,:);
% wing_data = cell2mat(wing_data(2:end,:));
wing_data = load([path_data 'wing_data_addmore.mat']).wing_data;
[temp,pallette] = getcolor('b');
wing_mat_venn = load([venn_path 'WingData/wing_data_venn.mat']).wing_data;
wing_data_venn = cell2mat(wing_mat_venn(2:end,:));

num_designs = size(wing_data,1);
num_flights = 5;
km_mat = []; % num of coefficients
design_mat = [];
rng(1)

%===partition training and test===
data = 1:num_designs;
holdout = 0.0;

if holdout == 0 % no partitioning
    %     cv = cvpartition(length(data),'HoldOut',holdoutkeep);
    %     idx_test = cv.test;
    %     dataTest  = data(idx_test)
    dataTest = 1:num_designs;
    dataTrain = 1:num_designs;
else
    if holdout == 0.1
        testidx = [9 17 18 28 47];
    elseif holdout == 0.15
        testidx = [8 9 17 18 28 40 47];
    elseif holdout == 0.2
        testidx = [8 9 16 17 18 28 38 40 47 50];
    elseif holdout == 0.25
        testidx = [2 8 9 16 17 18 28 38 39 40 47 50];
    elseif holdout == 0.3
        testidx = [2 8 9 16 17 18 21 28 34 38 39 40 42 47 50];
    end
    dataTest = testidx;
    temp = ones(1,num_designs);
    temp(dataTest) = 0;
    dataTrain = data(logical(temp));
    %     cv = cvpartition(length(data),'HoldOut',holdout);
    %     idx_test = cv.test;
    %     dataTrain = data(~idx_test);
    %     dataTest  = data(idx_test);
end



%===make main data matrix===
for i = 1:length(dataTrain)
    flinfo = load([path_flinfo 'flinfo' num2str(dataTrain(i)) '.mat']).flinfo;
    for j = 1:length(flinfo)
        flight = flinfo{j};
        geom_para = wing_data(dataTrain(i),[find(strcmp(header,'sweep')) find(strcmp(header,'back_sweep')) find(strcmp(header,'w/l'))]);
        dist_para = flight.landing_spot(2);
        centdev_para = flight.landing_spot(1);
        fltime_para = flight.fl_time;
        
        km_mat(end+1,:) = [flight.tstat flight.estimates geom_para dist_para centdev_para fltime_para];
        design_mat(end+1,:) = [dataTrain(i) j];
    end
end
km_header = {'tstat5','tstat4','tstat3','tstat2','tstat1','tstat0',...
    'coeff5','coeff4','coeff3','coeff2','coeff1','coeff0',...
    'sweep','back_sweep','w/l','dist','centdev','fltime'};





% === add distance to columns  ===
choose_cols = [7 8 9 10 16];

%===make matrix for kmeans again===
X = makeKmeansMatrix(km_mat,choose_cols,'all');

%===FInd number of clusters using evalclusters, which supports, the Calinski-Harabasz criterion
% clust = zeros(size(X,1),6);
% best_totsumd = zeros(6,1);
% for i=1:6
%     %     [clust(:,i),C,sumd] = kmeans(X,i,'emptyaction','singleton',...
%     %         'replicate',10);
optimalk = 3;

[idx,C,sumd] = kmeans(X,optimalk,'Distance','cityblock','replicate',10);
%
%     best_totsumd(i) = sum(sumd);
% end
% eva = evalclusters(X,clust,'CalinskiHarabasz');
% if  holdout>0 % for partitioned data, force optimal k to be 3
%     optimalk = 3;
% else
%     optimalk = eva.OptimalK;
% end
%
% % temp force optimalk to 3====
numincl = [sum(idx==1) sum(idx==2) sum(idx==3)];
[mini minind] = min(numincl);

allindex = [];
for i = 1:optimalk
    if i~=minind
        rando = randperm(numincl(i));
        rando = rando(1:mini);
        clusterindex = find(idx==i);
        filteredindex = clusterindex(rando);
        allindex = [allindex;filteredindex];
    else
        allindex = [allindex;find(idx==i)];
    end
end
allindex = sort(allindex);

idx = idx(allindex);
km_mat = km_mat(allindex,:);
design_mat = design_mat(allindex,:);

venn_designs = wing_data_venn(:,1);
venn_idx_mat = [];

%====add one line at a time to km_mat for the venn evaluation=====
for k = 1:length(venn_designs)
    new_km_mat = km_mat;
    flinfo = load([venn_path 'FlightInfo\flinfo' num2str(venn_designs(k)) '.mat']).flinfo;
    for j = 1:length(flinfo)
        flight = flinfo{j};
        geom_para = wing_data(dataTrain(i),[find(strcmp(header,'sweep')) find(strcmp(header,'back_sweep')) find(strcmp(header,'w/l'))]);
        dist_para = flight.landing_spot(2);
        centdev_para = flight.landing_spot(1);
        fltime_para = flight.fl_time;
        
        new_km_mat(end+1,:) = [flight.tstat flight.estimates geom_para dist_para centdev_para fltime_para];
        %         design_mat(end+1,:) = [dataTrain(i) j];
    end
    
    
    % === add distance to columns  ===
    choose_cols = [7 8 9 10 16];
    
    %===make matrix for kmeans again===
    X = makeKmeansMatrix(new_km_mat,choose_cols,'all');

    optimalk = 3;
    [idx,C,sumd] = kmeans(X,optimalk,'Distance','cityblock','replicate',10);
    
 
    
    %=====reorder the cluster based on median distance: shortest cluster should
    %be cluster 1, etc ====
    get_flightdata = @(flidx,headerstr) new_km_mat(flidx,find(strcmp(km_header,headerstr)));
    medianvec = [];
    for i = 1:optimalk
        medianvec(end+1) = quantile(get_flightdata(idx==i,'dist'),.5);
    end
    [temp sortedidx] = sort(medianvec);
    for i = 1:optimalk
        idx(idx==sortedidx(i)) = optimalk+i;
    end
    idx = idx-optimalk;
    
    venn_idx_mat(end+1,:) = idx(end-9:end)';
    
    
    
    
end

%======get prediction ratios======
probmat = load([venn_path 'gtprob.mat']).probmat;
pred_ratio = probmat(venn_designs,2:end);
exp_ratio = [sum(venn_idx_mat==1,2) sum(venn_idx_mat==2,2) sum(venn_idx_mat==3,2)]./10;

%=====make scientology=====


figure
P = [0 0; 1 0; 0.5 sqrt(3)/2];
T = [1 2 3];
TR = triangulation(T,P);
triplot(TR)
grid on
hold on
ID = ones(length(venn_designs),1);
B = pred_ratio;
C = barycentricToCartesian(TR,ID,B);
plot(C(:,1),C(:,2),'ko','MarkerSize',10,'MarkerFaceColor','k')
BB = exp_ratio;
CC = barycentricToCartesian(TR,ID,BB);
plot(CC(:,1),CC(:,2),'mo','MarkerSize',7,'MarkerFaceColor','m')
legend('triangle','prediction','experiment')

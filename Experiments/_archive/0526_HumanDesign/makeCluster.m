clc, clear, close all;
addpath(genpath('C:\Users\balto\switchdrive\PaperPlane\matlab'))

km_mat = load(findYoungestFile([pwd '\ClusterData'],'mat')).km_mat;
design_mat = load(findYoungestFile([pwd '\ClusterData'],'mat')).design_mat;
wing_mat = load(findYoungestFile([pwd '\WingData'],'mat')).wing_data;
km_data = cell2mat(km_mat(2:end,:));
wing_data = cell2mat(wing_mat(2:end,:));

% inline functions
mat_idx = @(whichmat,headerstr) find(strcmp(headerstr,whichmat(1,:)));
get_objlist = @(flidx,headerstr) km_data(flidx,mat_idx(km_mat,headerstr));
get_wing_geom = @(index,headerstr) wing_data(find(wing_data(:,1)==index),[find(strcmp(wing_mat(1,:),headerstr))]); % gets specific column data based on cluster


% inputs
objectivestr = 'dist';
rng(1)

% make trajectory cluster with kmeans
optimalk = 3;
choose_cols = [mat_idx(km_mat,'coeff5') mat_idx(km_mat,'coeff4')...
    mat_idx(km_mat,'coeff3') mat_idx(km_mat,'coeff2') mat_idx(km_mat,'dist')];
X = makeKmeansMatrix(km_data,choose_cols,'all');
[idx,C,sumd] = kmeans(X,optimalk,'Distance','cityblock','Replicates',10);

% find median values for the objective
medianvec = [];
for i = 1:optimalk
    medianvec(end+1) = quantile(get_objlist(idx==i,objectivestr),.5);
end
[medianvec sortedidx] = sort(medianvec);
for i = 1:optimalk
idx(idx==sortedidx(i)) = optimalk+i;
end
idx = idx-optimalk;

%=====plot clustered trajectories=====
path_flinfo = 'C:\Users\balto\switchdrive\PaperPlane\Experiments\0413_50samples\FlightInfo\';
plotClusteredTrajectory(optimalk,50,path_flinfo,idx,design_mat,1:size(km_data,1),NaN,0)

%=====plot clustered geometries disregarding probability=====
plotClusteredWings2(design_mat,optimalk,idx,wing_data,wing_mat(1,:),NaN,0,size(km_mat,1))

%====find optimal k for each cluster====
subidx = {};
suboptimalk = [];
geom_mat = {};
for i = 1:optimalk
    wing_idx = design_mat(idx==i,1);
    X = [];
    for j = 1:length(wing_idx)
        X(end+1,:) = [get_wing_geom(wing_idx(j),'sweep') get_wing_geom(wing_idx(j),'back_sweep') get_wing_geom(wing_idx(j),'w/l')];
    end
    geom_mat{i} = X;
    num_designs = length(unique(wing_idx));
    if num_designs<=5
        max_subgroups = 1;
    elseif and(num_designs>5,num_designs<=10)
        max_subgroups = 2;
    else
        max_subgroups = 3;
    end
    clust = zeros(size(X,1),max_subgroups);
    for j=1:max_subgroups
        [clust(:,j),C,sumd] = kmeans(X,j,'emptyaction','singleton',...
            'replicate',10);
    end
    if max_subgroups>1
        eva = evalclusters(X,clust,'silhouette');
        suboptimalk(i) = eva.OptimalK;
    else
        suboptimalk(i) = 1;
    end
    %     end
    subidx{i} = clust(:,suboptimalk(i));
end

%===make GMM===
getGMModel = @(geom_mat,cluster_num,subcluster_num) fitgmdist(geom_mat,subcluster_num,'Start',subidx{cluster_num}); % gets 3D GMM for cluster
getGMModel_error = @(geom_mat,cluster_num,subcluster_num) fitgmdist(geom_mat,subcluster_num,'SharedCovariance',true,'Start',subidx{cluster_num}); % gets 3D GMM for cluster

for j = 1:optimalk
    try
        modelStruct(j).model = getGMModel(geom_mat{j},j,suboptimalk(j));
    catch
        modelStruct(j).model = getGMModel_error(geom_mat{j},j,suboptimalk(j));
    end
end

% save gmmodel
time = datestr(now,'mmdd_HHMM');
filename = sprintf(['modelStruct_%s.mat'],time);
prompt = "Do you want to save gmmodel? y/n:";
txt = input(prompt,"s");
if strcmp(txt,'y')
    save([pwd '\GMModels\' filename],'modelStruct','idx','medianvec','subidx')
end

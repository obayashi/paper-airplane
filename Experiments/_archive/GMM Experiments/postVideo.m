clc, clear, close all;
addpath(genpath('C:\Users\balto\switchdrive\PaperPlane\matlab'))
path_video = 'C:\Users\balto\Videos\';

video_folder = '0514_trial'; % change

wing_mat = load(findYoungestFile([pwd '\WingData'],'mat')).wing_data;
wing_data = cell2mat(wing_mat(2:end,:));

mat_idx = @(whichmat,headerstr) find(strcmp(headerstr,whichmat(1,:)));

design_num = wing_data(end,mat_idx(wing_mat,'idx_orig'));

% process video, get raw trajectory
getTrajectory(path_video,design_num,video_folder,pwd)

% get flight data
% !!!make sure to do camera calibration and check minimum distance
% beforehand!!!
getFlightData(design_num,pwd)

%make flight datasheet
makeFlightInfoPlot(design_num,pwd)

% add to the existing km_mat/design_mat and save as new
[km_mat,design_mat] = make_kmmat(design_num,pwd,1);

clc, clear, close all;
addpath(genpath('C:\Users\balto\switchdrive\PaperPlane\matlab'))

wing_mat = load(findYoungestFile([pwd '\WingData'],'mat')).wing_data;
wing_data = cell2mat(wing_mat(2:end,:));

prompt = "Enter design numer:";
txt = input(prompt,"s");
new_idx = str2num(txt);
prompt = "Enter sweep angle (deg):";
txt = input(prompt,"s");
new_sw = str2double(txt);
prompt = "Enter back sweep angle (deg):";
txt = input(prompt,"s");
new_swb = str2double(txt);
prompt = "Enter width-length ratio:";
txt = input(prompt,"s");
new_wol = str2double(txt);

wing = cluster2origpara(new_sw,new_swb,new_wol);

paper.size = [210 297]; %a4
paper.margin = 5; %mm
holder.length = 60;


%hardcode not good
ac_fwd = 30; % percentage
holder_front_offset = 32; %mm

wing_pts = get_points(wing,paper);

wing.bsweep = new_swb;
wing.width = wing.b_half*2;
wing.length = max(wing_pts.x)-min(wing_pts.x);
wing.wol = new_wol;

aero = get_mac(wing_pts,wing,paper);
ac_x_fwd = aero.ac_x-((ac_fwd/10)*(aero.ac_x/10));
holder_front_x = ac_x_fwd-holder_front_offset;
holder_back_x = holder_front_x+holder.length;

outvec =  [new_idx wing_pts.x(1:4) wing_pts.y(1:4) wing_pts.area...
    wing.c_root wing.c_tip wing.b_half wing.sw_1 wing.bsweep...
    wing.width wing.length wing.wol aero.mac...
    aero.ac_x holder_front_x holder_back_x];

wing_data = [wing_data; outvec];
wing_data = [wing_mat(1,:); num2cell(wing_data)];

% save wing data
time = datestr(now,'mmdd_HHMM');
filename = sprintf('wing_data_%s.mat',time);
prompt = "Do you want to save wing_data? y/n:";
txt = input(prompt,"s");
if strcmp(txt,'y')
    save([pwd '\WingData\' filename],'wing_data')
end

fprintf('\n')
disp(['Make dxf of wing ' num2str(new_idx)])
fprintf('x2: %.4f\n',outvec(find(strcmp(wing_data(1,:),'x2'))))
fprintf('y2: %.4f\n',outvec(find(strcmp(wing_data(1,:),'y2'))))
fprintf('x3: %.4f\n',outvec(find(strcmp(wing_data(1,:),'x3'))))
fprintf('x4: %.4f\n',outvec(find(strcmp(wing_data(1,:),'x4'))))
fprintf('holderfront: %.4f\n',outvec(find(strcmp(wing_data(1,:),'holderfront_x'))))



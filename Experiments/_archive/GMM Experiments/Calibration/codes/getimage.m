clc, clear, close all;

%% 1) change here
path = 'C:\Users\balto\Videos\0418_50samples\0420_redo\';
vid_name = 'reddots.mp4';

vidObj =VideoReader(strcat(path,vid_name),'CurrentTime',0);
l= vidObj.NumFrames;
frames = read(vidObj,[1 Inf]);
frame_width = size(frames,2);
frame_height = size(frames,1);
t1 = frames(:,:,:,1);

% work on top view camera
I1 = imcrop(t1,[0 0 frame_width/2 frame_height]); % left of screen
figure
imshow(I1)
imwrite(I1,['red_floor.jpg'])
pause(3);
close all;

%% open color thresholder app and save createMask function

%% work with createMask function and check if okay
num_dots = 10; % may need to change
[BW,maskedRGBImage] = createMask(I1);
largestRegion = bwareafilt(BW, num_dots);       
figure
imshow(largestRegion)
hold on
s = regionprops(largestRegion,'centroid'); %Calculate centroids for connected components in the image using regionprops. The regionprops function returns the centroids in a structure array.
centroids_top = cat(1,s.Centroid); %Store the x- and y-coordinates of the centroids into a two-column matrix.
plot(centroids_top(:,1),centroids_top(:,2),'r*')
hold off

%% save pixel points of red dots
% centroids_top([1,3],:) = []; % if there are ones i want to exclude
campts.x = centroids_top(:,1);
campts.y = centroids_top(:,2);
save('campts.mat','campts');

%% check that campts and wldpts are in the same order and run transform.m


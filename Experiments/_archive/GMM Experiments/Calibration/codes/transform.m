clc, clear, close all;

imgpts = load('campts.mat').campts;
wldpts = load('wldpts.mat').wldpts;

movingPoints = [imgpts.x imgpts.y];
fixedPoints = [wldpts.x wldpts.y];
tform = fitgeotrans(movingPoints,fixedPoints,'projective');
time = datestr(now,'mmdd_HHMM');
filename = sprintf('../tform_%s.mat',time);
save(filename,'tform');

%test -- seems correct
[worldPoints_X, worldPoints_Y] = transformPointsForward(tform, 960,540);



addpath(genpath('C:\Users\balto\switchdrive\PaperPlane\matlab'))

wing_mat = load(findYoungestFile([pwd '\WingData'],'mat')).wing_data;
wing_data = cell2mat(wing_mat(2:end,:));

mat_idx = @(whichmat,headerstr) find(strcmp(headerstr,whichmat(1,:)));



figure 
subplot(3,1,1)
iter = 1:size(wing_data,1);
sw_idx = mat_idx(wing_mat,'sweep');
swb_idx = mat_idx(wing_mat,'back_sweep');
wol_idx = mat_idx(wing_mat,'w/l');
plot(iter,wing_data(:,sw_idx))
ylabel('sweep')
hold on 
plot([10 10],[0 60],'r-')
subplot(3,1,2)
plot(iter,wing_data(:,swb_idx))
ylabel('back sweep')
hold on 
plot([10 10],[0 120],'r-')
subplot(3,1,3)
plot(iter,wing_data(:,wol_idx))
ylabel('width/length')
hold on 
plot([10 10],[0 1.8],'r-')

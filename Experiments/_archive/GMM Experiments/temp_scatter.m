clc, clear, close all;
addpath(genpath('C:\Users\balto\switchdrive\PaperPlane\matlab'))

km_mat = load(findYoungestFile([pwd '\ClusterData'],'mat')).km_mat;
design_mat = load(findYoungestFile([pwd '\ClusterData'],'mat')).design_mat;
wing_mat = load(findYoungestFile([pwd '\WingData'],'mat')).wing_data;
idx = load(findYoungestFile([pwd '\GMModels'],'mat')).idx;
km_data = cell2mat(km_mat(2:end,:));
wing_data = cell2mat(wing_mat(2:end,:));

mat_idx = @(whichmat,headerstr) find(strcmp(headerstr,whichmat(1,:)));

X = [];
for i = 1:size(design_mat,1)
    tempidx = find(wing_data(:,1)==design_mat(i,1));
    X(end+1,:) = [wing_data(tempidx,mat_idx(wing_mat,'sweep')),...
        wing_data(tempidx,mat_idx(wing_mat,'back_sweep')),...
        wing_data(tempidx,mat_idx(wing_mat,'w/l'))];
end

X = [X; 17.6 78 0.726];
idx = [idx;4];
gscatter3(X(:,1),X(:,2),X(:,3),idx)


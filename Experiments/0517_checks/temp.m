clc, clear, close all;

addpath(genpath('C:\Users\balto\switchdrive\PaperPlane\matlab'))

[num txt raw] = xlsread('Book2.xlsx','Sheet2');
idx = num(:,1);
sw = num(:,2);
swb = num(:,3);
wol = num(:,4);

original = 1:50;
idxo = idx(original);
swo = sw(original);
swbo = swb(original);
wolo = wol(original);

new = 51:size(idx,1);
idxn = idx(new);
swn = sw(new);
swbn = swb(new);
woln = wol(new);

neww = 71:size(idx,1);
swnn = sw(neww);
swbnn = swb(neww);
wolnn = wol(neww);

X = table2array(load('X.mat').X);
tf = load('tf.mat').tf;
tf_des = load('tf_des.mat').tf_des;


figure
plot3(swo(idxo==1),swbo(idxo==1),wolo(idxo==1),'r.','MarkerSize',12)
hold on
plot3(swo(idxo==2),swbo(idxo==2),wolo(idxo==2),'g.','MarkerSize',12)
plot3(swo(idxo==3),swbo(idxo==3),wolo(idxo==3),'b.','MarkerSize',12)

grid on
plot3(swn,swbn,woln,'k.','MarkerSize',12)
plot3(swnn,swbnn,wolnn,'k.','MarkerSize',25)
plot3(X(tf,1),X(tf,2),X(tf,3),'m.','MarkerSize',12)
% plot3(X(tf_des,1),X(tf_des,2),X(tf_des,3),'m.','MarkerSize',12)
% plot3(X(:,1),X(:,2),X(:,3),'m.','MarkerSize',12)


legend('Cl1','Cl2','Cl3','New points','fake long distances','OK points from BO')
xlabel('sweep')
ylabel('back sweep')
zlabel('width/length')

[t,pallette] = getcolor('b');

GMModel = load('modelStruct_0518_0907.mat').modelStruct(3).model;
% qp = [28.3030,95.6790,0.2569];
% qp = [22.0776,105.5562,1.3020];
% qp = [22.1251,81.6037,1.2518];

% probout = getProbability(qp,GMModel);

num_subcluster = size(GMModel.mu,1);

g = gca;
for i = 1:num_subcluster
    mu = GMModel.mu(i,:);
    if and(ndims(GMModel.Sigma)<3,num_subcluster>1)
        Sigma = GMModel.Sigma;
    else
        Sigma = GMModel.Sigma(:,:,i);
    end
    if Sigma(3,3) < 0.04
        Sigma(3,3) = 0.04;
    end
    plot_gaussian_ellipsoid(mu,Sigma,2,20,g,pallette{1})
end


[num txt raw] = xlsread('Book2.xlsx','Sheet1');
figure 
subplot(3,1,1)
iter = 1:17;
sw_idx = 14;
swb_idx = 15;
wol_idx = 18;
plot(iter,num(:,sw_idx))
ylabel('sweep')
hold on 
plot([10 10],[0 60],'r-')
subplot(3,1,2)
plot(iter,num(:,swb_idx))
ylabel('back sweep')
hold on 
plot([10 10],[0 120],'r-')
subplot(3,1,3)
plot(iter,num(:,wol_idx))
ylabel('width/length')
hold on 
plot([10 10],[0 1.8],'r-')





